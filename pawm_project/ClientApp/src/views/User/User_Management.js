﻿import React, { Component } from 'react';
import {
    Button,
    Row,
    Col,
    Card,
    CardHeader,
    CardBody,
    Table,
    CardFooter,
    Input
} from 'reactstrap';
import { Link } from 'react-router-dom'
import * as helper from '../../modules/Helper'
class User extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = { collapse: true };
    }

    toggle() {
        this.setState({ collapse: !this.state.collapse });
    }
    componentDidMount() {
    }
    render() {
        return (
            <div className="form-user" >
                <div className="form-management">
                    <div className="animated fadeIn">
                        <Card>
                            <CardHeader>
                                <h1> Danh sách nhân viên </h1>
                            </CardHeader>
                            <CardBody>
                                <Row>
                                    <Col sm="3" xs="12">
                                        <label>Cửa hàng</label>
                                        <Input type="select" name="select" id="select" required>
                                            <option value="" defaultselected="true">Chọn cửa hàng</option>
                                            <option value="1">Cửa hàng 1</option>
                                            <option value="2">Cửa hàng 2</option>
                                            <option value="3">Cửa hàng 3</option>
                                            <option value="4">Cửa hàng 4</option>
                                        </Input>
                                    </Col>
                                    <Col sm="3" xs="12">
                                        <label>Mã nhân viên</label>
                                        <Input type="text" placeholder="Mã nhân viên"  />
                                    </Col>
                                    <Col sm="3" xs="12">
                                        <label>Tên nhân viên</label>
                                        <Input type="text" placeholder="Tên nhân viên"  />
                                    </Col>
                                    <Col sm="3" xs="12">
                                        <label>Số điện thoại</label>
                                        <Input type="text" placeholder="Số điện thoại" />
                                    </Col>
                                </Row>
                            </CardBody>
                            <CardFooter>
                                <div className="button-right">
                                    <Link to="/them-nhan-vien" className="btn btn-primary" >
                                        <span className="icon-plus"> </span> Thêm mới
                                    </Link>
                                    <Button className="primary button_focus">
                                        <span className="icon-magnifier"> </span>Tìm kiếm
                                </Button>
                                    
                                </div>
                            </CardFooter>
                            <CardBody>
                                <Table responsive striped>
                                    <thead>
                                        <tr>
                                            <th width="100px"></th>
                                            <th width="80px">Mã NV</th>
                                            <th width="150px">Tên nhân viên</th>
                                            <th width="105px">Số điện thoại</th>
                                            <th width="100px">Cửa hàng</th>
                                            <th width="150px">Email</th>
                                            <th width="100px">Ngày sinh</th>
                                            <th width="80px">Giới tính</th>
                                            <th>Địa chỉ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td id="control">
                                                <Link to={helper.getPathHost('staff', 'detail-staff', 'url')} className="btn btn-primary btn-sm" >
                                                    <span title="Chi tiết" class="fa fa-pencil"> </span>
                                                </Link>
                                                <Button id="btnDel" color="danger" size="sm" >
                                                    <i title="Xóa" className="fa fa-trash"></i>
                                                </Button>
                                            </td>
                                            <td>NV0001</td>
                                            <td>Nguyễn Văn A</td>
                                            <td>0931548754</td>
                                            <td>Cầm đồ 1</td>
                                            <td>anv@gmail.com</td>
                                            <td>11/11/1996</td>
                                            <td>Nam</td>
                                            <td>123 BTX</td>
                                        </tr>
                                        <tr>
                                            <td id="control">
                                                <Link to={helper.getPathHost('staff', 'detail-staff', 'url')} className="btn btn-primary btn-sm" >
                                                    <span title="Chi tiết" class="fa fa-pencil"> </span>
                                                </Link>
                                                <Button id="btnDel" color="danger" size="sm" >
                                                    <i title="Xóa" className="fa fa-trash"></i>
                                                </Button>
                                            </td>
                                            <td>NV0002</td>
                                            <td>Nguyễn Thị B</td>
                                            <td>0931548756</td>
                                            <td>Cầm đồ 1</td>
                                            <td>bnt@gmail.com</td>
                                            <td>11/11/1996</td>
                                            <td>Nữ</td>
                                            <td>123 BTBC</td>
                                        </tr>
                                        <tr>
                                            <td id="control">
                                                <Link to={helper.getPathHost('staff', 'detail-staff', 'url')} className="btn btn-primary btn-sm" >
                                                    <span title="Chi tiết" class="fa fa-pencil"> </span>
                                                </Link>
                                                <Button id="btnDel" color="danger" size="sm" >
                                                    <i title="Xóa" className="fa fa-trash"></i>
                                                </Button>
                                            </td>
                                            <td>NV0003</td>
                                            <td>Phan Văn C</td>
                                            <td>0931548775</td>
                                            <td>Cầm đồ 2</td>
                                            <td>cpv@gmail.com</td>
                                            <td>11/11/1996</td>
                                            <td>Nam</td>
                                            <td>123 BCLA</td>
                                        </tr>
                                        <tr>
                                            <td id="control">
                                                <Link to={helper.getPathHost('staff', 'detail-staff', 'url')} className="btn btn-primary btn-sm" >
                                                    <span title="Chi tiết" class="fa fa-pencil"> </span>
                                                </Link>
                                                <Button id="btnDel" color="danger" size="sm" >
                                                    <i title="Xóa" className="fa fa-trash"></i>
                                                </Button>
                                            </td>
                                            <td>NV0004</td>
                                            <td>Nguyễn Văn D</td>
                                            <td>0931548758</td>
                                            <td>Cầm đồ 3</td>
                                            <td>anv@gmail.com</td>
                                            <td>11/11/1996</td>
                                            <td>Nam</td>
                                            <td>123 BTX</td>
                                        </tr>
                                        <tr>
                                            <td id="control">
                                                <Link to={helper.getPathHost('staff', 'detail-staff', 'url')} className="btn btn-primary btn-sm" >
                                                    <span title="Chi tiết" class="fa fa-pencil"> </span>
                                                </Link>
                                                <Button id="btnDel" color="danger" size="sm" >
                                                    <i title="Xóa" className="fa fa-trash"></i>
                                                </Button>
                                            </td>
                                            <td>NV0005</td>
                                            <td>Nguyễn Văn E</td>
                                            <td>0931548751</td>
                                            <td>Cầm đồ 2</td>
                                            <td>anv@gmail.com</td>
                                            <td>11/11/1996</td>
                                            <td>Nam</td>
                                            <td>123 BTX</td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </CardBody>
                        </Card>
                    </div>
                </div>
            </div>
        )
    }
}
export default User;