﻿import React, { Component } from 'react';
import {
    Row,
    Col,
    Card,
    CardHeader,
    CardBody,
    CardFooter,
    Button,
    FormGroup,
    Label,
    Input,
    TabContent,
    TabPane,
    Nav,
    NavItem,
    NavLink,
    Table
} from 'reactstrap';
import { Link } from 'react-router-dom'
import { TextMask, InputAdapter } from 'react-text-mask-hoc';
import $ from 'jquery'
import classnames from 'classnames';

class UpdateExchange extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            activeTab: '1'
        };
    }

    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }
    componentDidMount() {
        (function () {
            var forms = document.getElementsByClassName('form-exchange-update');
            // Loop over them and prevent submission
            Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        })();

        $('.img-box #file-upload').change(function () {
            //var i = $(this).prev('label').clone();
            //var fileName = $('#file-upload')[0].files[0].name;
            //$(this).prev('label').text(fileName);
            var file = $('.img-box #file-upload')[0].files[0];
            $(".img-box img").attr('src', URL.createObjectURL(file));
        });

        $(document).on("keypress", "#CardId , #HoldingAmount , #InterestArising ,#AmountPaid ,#InterestRate", function (evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57)) {
                return false;
            }
            return true;
        })

    }
    render() {
        return (
            <div className="update-exchange-page">
                <div id="UpdateGD">
                    <Card >
                        <CardHeader>
                            <h1>Chi tiết giao dịch</h1>
                        </CardHeader>
                        <CardBody>
                            <Row>
                                <Col sm="6" xs="12">
                                    <FormGroup>
                                        <Label >Mã khách hàng</Label>
                                        <Input type="text" placeholder="Mã khách hàng" readOnly defaultValue="CH10001" />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label >Tên khách hàng</Label>
                                        <Input type="text" placeholder="Tên khách hàng" defaultValue="Nguyễn Văn A" readOnly />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Số điện thoại</Label>
                                        <Input type="text" placeholder="Số điện thoại" defaultValue="0123456789" readOnly />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>CMND</Label>
                                        <Input type="text" placeholder="Chứng minh nhân dân" defaultValue="987654321" readOnly />
                                    </FormGroup>
                                </Col>
                                <Col sm="6" xs="12">
                                    <FormGroup>
                                        <Label>Tên tài sản</Label>
                                        <Input type="text" placeholder="Tên tài sản" defaultValue="Iphone 7" readOnly />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Số tiền cầm</Label>
                                        <Input type="text" placeholder="Số tiền cầm" readOnly defaultValue="11 000 000" />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Lãi</Label>
                                        <Input type="text" placeholder="Phát sinh khi chọn lãi" defaultValue="1%" readOnly />
                                    </FormGroup>
                                    <Row>
                                        <Col sm="6">
                                            <FormGroup>
                                                <Label>Từ ngày</Label>
                                                <Input type="text" placeholder="Từ ngày" defaultValue="12-12-2018" readOnly />
                                            </FormGroup>
                                        </Col>
                                        <Col sm="6">
                                            <FormGroup>
                                                <Label>Đến ngày</Label>
                                                <Input type="text" placeholder="Đến ngày" defaultValue="30-12-2018" readOnly />
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </CardBody>
                    </Card>
                    <Nav tabs>
                        <NavItem>
                            <NavLink
                                className={classnames({ active: this.state.activeTab === '1' })}
                                onClick={() => { this.toggle('1'); }}
                            >
                                Gia hạn
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                                className={classnames({ active: this.state.activeTab === '2' })}
                                onClick={() => { this.toggle('2'); }}
                            >
                                Đóng lãi
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                                className={classnames({ active: this.state.activeTab === '3' })}
                                onClick={() => { this.toggle('3'); }}
                            >
                                Trả bớt gốc
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                                className={classnames({ active: this.state.activeTab === '4' })}
                                onClick={() => { this.toggle('4'); }}
                            >
                                Chuộc đồ
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                                className={classnames({ active: this.state.activeTab === '5' })}
                                onClick={() => { this.toggle('5'); }}
                            >
                                Lịch sử
                            </NavLink>
                        </NavItem>
                    </Nav>
                    <TabContent activeTab={this.state.activeTab}>
                        <TabPane tabId="1">
                            <Card >
                                <CardBody>
                                    <Row>
                                        <Col sm="6" xs="12">
                                            <FormGroup>
                                                <Label>Từ ngày</Label>
                                                <TextMask
                                                    mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
                                                    Component={InputAdapter}
                                                    className="form-control"
                                                    placeholder="Từ ngày"
                                                    type="text"
                                                    required
                                                />
                                                <div className="invalid-feedback">
                                                    Vui lòng nhập ngày.
                                                </div>
                                            </FormGroup>
                                        </Col>
                                        <Col sm="6" xs="12">
                                            <FormGroup>
                                                <Label>Đến ngày</Label>
                                                <TextMask
                                                    mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
                                                    Component={InputAdapter}
                                                    className="form-control"
                                                    placeholder="Đến ngày"
                                                    type="text"
                                                    required
                                                />
                                                <div className="invalid-feedback">
                                                    Vui lòng nhập ngày.
                                                </div>
                                            </FormGroup>
                                        </Col>
                                        <Col sm="4" xs="8">
                                            <FormGroup>
                                                <Label>Lãi</Label>
                                                <Input type="select" required>
                                                    <option value="1">Theo ngày</option>
                                                    <option value="2">Theo tháng</option>
                                                </Input>
                                                <div className="invalid-feedback">
                                                    Vui lòng chọn lãi suất.
                                                </div>
                                            </FormGroup>
                                        </Col>
                                        <Col sm="2" xs="4">
                                            <FormGroup>
                                                <Label style={{ marginTop: "17px" }}></Label>
                                                <Input type="text" placeholder="Phát sinh khi chọn lãi" required id="InterestRate" />
                                                <div className="invalid-feedback">
                                                    Không được bỏ trống.
                                                </div>
                                            </FormGroup>
                                        </Col>
                                        <Col sm="6" xs="12">
                                            <FormGroup>
                                                <Label>Tiền lãi phát sinh</Label>
                                                <Input type="text" placeholder="Tiền lãi phát sinh" id="InterestArising" />
                                            </FormGroup>
                                        </Col>
                                        <Col sm="12" xs="12" className="mb-3">
                                            <FormGroup>
                                                <Label>Ghi chú</Label>
                                                <Input type="textarea" name="textarea-input" id="textarea-input" rows="2"
                                                    placeholder="Ghi chú" />
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                </CardBody>
                                <CardFooter>
                                    <div className="button-right">
                                        <Link to="/danh-sach-khach-hang/thong-tin-khach-hang" className="btn btn-primary" >
                                            <span className="icon-list"> </span>Danh sách
                                        </Link>
                                        <Button color="primary" type="submit">
                                            <span className="fa fa-save"> </span> Gia hạn
                                        </Button>
                                    </div>
                                </CardFooter>
                            </Card>
                        </TabPane>
                        <TabPane tabId="2">
                            <Card >
                                <CardBody>
                                    <Row>
                                        <Col sm="5" xs="12">
                                            <FormGroup>
                                                <Label>Từ ngày</Label>
                                                <TextMask
                                                    mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
                                                    Component={InputAdapter}
                                                    className="form-control"
                                                    placeholder="Từ ngày"
                                                    type="text"
                                                    required
                                                />
                                                <div className="invalid-feedback">
                                                    Vui lòng nhập ngày.
                                                </div>
                                            </FormGroup>
                                        </Col>
                                        <Col sm="5" xs="12">
                                            <FormGroup>
                                                <Label>Đến ngày</Label>
                                                <TextMask
                                                    mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
                                                    Component={InputAdapter}
                                                    className="form-control"
                                                    placeholder="Đến ngày"
                                                    type="text"
                                                    required
                                                />
                                                <div className="invalid-feedback">
                                                    Vui lòng nhập ngày.
                                                </div>
                                            </FormGroup>
                                        </Col>
                                        <Col sm="2" xs="4">
                                            <FormGroup>
                                                <Label>Số ngày</Label>
                                                <Input type="text" defaultValue="3" readOnly />
                                            </FormGroup>
                                        </Col>
                                        <Col sm="6" xs="8">
                                            <FormGroup>
                                                <Label>Tiền lãi</Label>
                                                <Input type="text" placeholder="Tiền lãi" defaultValue="10000000" readOnly />
                                            </FormGroup>
                                        </Col>
                                        <Col sm="6" xs="4">
                                            <FormGroup>
                                                <Label>Số tiền khác</Label>
                                                <Input type="text" placeholder="Số tiền khác" />
                                            </FormGroup>
                                        </Col>
                                        <Col sm="6" xs="12">
                                            <FormGroup>
                                                <Label>Tổng tiền lãi</Label>
                                                <Input type="text" placeholder="Tổng tiền lãi" readOnly />
                                            </FormGroup>
                                        </Col>
                                        <Col sm="6" xs="12">
                                            <FormGroup>
                                                <Label>Ghi chú</Label>
                                                <Input type="textarea" name="textarea-input" id="textarea-input" rows="1"
                                                    placeholder="Ghi chú" />
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                </CardBody>
                                <CardFooter>
                                    <div className="button-right">
                                        <Link to="/danh-sach-khach-hang/thong-tin-khach-hang" className="btn btn-primary" >
                                            <span className="icon-list"> </span>Danh sách
                                        </Link>
                                        <Button color="primary" type="submit">
                                            <span className="fa fa-save"> </span> Đóng lãi
                                        </Button>
                                    </div>
                                </CardFooter>
                            </Card>
                        </TabPane>
                        <TabPane tabId="3">
                            <Card >
                                <CardBody>
                                    <Row>
                                        <Col sm="6" xs="12">
                                            <FormGroup>
                                                <Label>Ngày trả trước gốc</Label>
                                                <TextMask
                                                    mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
                                                    Component={InputAdapter}
                                                    className="form-control"
                                                    placeholder="Ngày trả trước gốc"
                                                    type="text"
                                                    required
                                                />
                                                <div className="invalid-feedback">
                                                    Vui lòng nhập ngày.
                                                </div>
                                            </FormGroup>
                                        </Col>
                                        <Col sm="6" xs="12">
                                            <FormGroup>
                                                <Label>Số tiền trả trước</Label>
                                                <Input type="text" placeholder="Số tiền trả trước" />
                                            </FormGroup>
                                        </Col>
                                        <Col sm="12" xs="12">
                                            <FormGroup>
                                                <Label>Ghi chú</Label>
                                                <Input type="textarea" name="textarea-input" id="textarea-input" rows="2"
                                                    placeholder="Ghi chú" />
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                </CardBody>
                                <CardFooter>
                                    <div className="button-right">
                                        <Link to="/danh-sach-khach-hang/thong-tin-khach-hang" className="btn btn-primary" >
                                            <span className="icon-list"> </span>Danh sách
                                        </Link>
                                        <Button color="primary" type="submit">
                                            <span className="fa fa-save"> </span> Trả gốc
                                        </Button>
                                    </div>
                                </CardFooter>
                            </Card>
                        </TabPane>
                        <TabPane tabId="4">
                            <Card >
                                <CardBody>
                                    <Row>
                                        <Col sm="6" xs="12">
                                            <FormGroup>
                                                <Label>Ngày chuộc đồ</Label>
                                                <TextMask
                                                    mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
                                                    Component={InputAdapter}
                                                    className="form-control"
                                                    placeholder="Ngày chuộc đồ"
                                                    type="text"
                                                    required
                                                />
                                                <div className="invalid-feedback">
                                                    Vui lòng nhập ngày.
                                                </div>
                                            </FormGroup>
                                        </Col>
                                        <Col sm="6" xs="12">
                                            <FormGroup>
                                                <Label>Số tiền cầm</Label>
                                                <Input type="text" placeholder="Số tiền cầm" defaultValue="10000000" />
                                            </FormGroup>
                                        </Col>
                                        <Col sm="6" xs="12">
                                            <FormGroup>
                                                <Label>Tiền khác</Label>
                                                <Input type="text" placeholder="Tiền khác" />
                                            </FormGroup>
                                        </Col>
                                        <Col sm="6" xs="12">
                                            <FormGroup>
                                                <Label>Số tiền thanh toán</Label>
                                                <Input type="text" placeholder="Số tiền thanh toán" defaultValue="10000000" readOnly />
                                            </FormGroup>
                                        </Col>
                                        <Col sm="12" xs="12">
                                            <FormGroup>
                                                <Label>Ghi chú</Label>
                                                <Input type="textarea" name="textarea-input" id="textarea-input" rows="2"
                                                    placeholder="Ghi chú" />
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                </CardBody>
                                <CardFooter>
                                    <div className="button-right">
                                        <Link to="/danh-sach-khach-hang/thong-tin-khach-hang" className="btn btn-primary" >
                                            <span className="icon-list"> </span>Danh sách
                                        </Link>
                                        <Button color="primary" type="submit">
                                            <span className="fa fa-save"> </span> Thanh toán
                                        </Button>
                                    </div>
                                </CardFooter>
                            </Card>
                        </TabPane>
                        <TabPane tabId="5">
                            <Card >
                                <CardHeader>
                                    <span>Lịch sử giao dịch</span>
                                </CardHeader>
                                <CardBody>
                                    <Table responsive className="table-striped">
                                        <thead>
                                            <tr>
                                                <th id="th-first">STT</th>
                                                <th>Ngày</th>
                                                <th>Nhân viên giao dịch</th>
                                                <th>Nội dung</th>
                                                <th>Ghi chú</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>12/12/2018</td>
                                                <td>Nguyễn Văn A</td>
                                                <td>Gia hạn lãi ngày ... với số tiền...</td>
                                                <td>Nhớ đóng lãi trước ngày...</td>
                                            </tr>
                                        </tbody>
                                    </Table>
                                </CardBody>
                            </Card>
                        </TabPane>
                    </TabContent>
                </div>
            </div>
        )
    }
}
export default UpdateExchange;
