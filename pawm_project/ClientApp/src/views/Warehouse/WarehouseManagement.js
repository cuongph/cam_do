﻿import React, { Component } from 'react';
import {
    Row,
    Col,
    Card,
    CardHeader,
    CardBody,
    CardFooter,
    Button,
    Input,
    Table,
    FormGroup,
    Label
} from 'reactstrap';
import { Link } from 'react-router-dom'
import { TextMask, InputAdapter } from 'react-text-mask-hoc';
import classnames from 'classnames';
class WarehouseManagement extends Component {
    render() {
        return (
            <div className="animated fadeIn">
                <div className="customer-page">
                    <Card>
                        <CardHeader>
                            <h1>Quản lí kho</h1>
                        </CardHeader>
                        <CardBody>
                            <Row>
                                <Col sm="3" xs="12">
                                    <FormGroup style={{ marginBottom: "0" }}>
                                        <Label>Cửa hàng</Label>
                                        <Col>
                                            <Input type="select">
                                                <option value="1">Cửa hàng 1</option>
                                                <option value="2">Cửa hàng 2</option>
                                                <option value="3">Cửa hàng 3</option>
                                                <option value="4">Cửa hàng 4</option>
                                            </Input>
                                        </Col>
                                    </FormGroup>
                                </Col>
                                <Col sm="3" xs="12">
                                    <FormGroup style={{ marginBottom: "0" }}>
                                        <Label>Loại tài sản</Label>
                                        <Col>
                                            <Input type="select">
                                                <option value="1">Điện thoại</option>
                                                <option value="2">Máy tính</option>
                                                <option value="3">Xe máy</option>
                                                <option value="4">Xe hơi</option>
                                            </Input>
                                        </Col>
                                    </FormGroup>
                                </Col>
                                <Col sm="3" xs="12">
                                    <FormGroup>
                                        <Label>Từ ngày</Label>
                                        <TextMask
                                            mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
                                            Component={InputAdapter}
                                            className="form-control"
                                            placeholder="Từ ngày"
                                            type="text"
                                            required
                                        />
                                        <div className="invalid-feedback">
                                            Vui lòng nhập ngày.
                                                            </div>
                                    </FormGroup>
                                </Col>
                                <Col sm="3" xs="12">
                                    <FormGroup>
                                        <Label>Đến ngày</Label>
                                        <TextMask
                                            mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
                                            Component={InputAdapter}
                                            className="form-control"
                                            placeholder="Đến ngày"
                                            type="text"
                                            required
                                        />
                                        <div className="invalid-feedback">
                                            Vui lòng nhập ngày.
                                                            </div>
                                    </FormGroup>
                                </Col>
                            </Row>
                        </CardBody>
                        <CardFooter>
                            <div className="button-right">
                                <Button color="primary button_focus">
                                    <span className="icon-magnifier"> </span>Tìm kiếm
                                </Button>
                            </div>
                        </CardFooter>
                    </Card>
                    <Card>
                        
                        <CardBody>
                            <Table responsive className="table-striped">
                                <thead>
                                    <tr>
                                        <th>Cửa hàng</th>
                                        <th>Loại TS</th>
                                        <th>Tên TS</th>
                                        <th>Giá cầm</th>
                                        <th>Giá thanh lý</th>
                                        <th>Người tạo</th>
                                        <th>Ngày tạo</th>
                                        <th>Số lượng</th>
                                        <th style={{ textAlign: "center" }}>Trạng thái</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Cửa hàng 1</td>
                                        <td>Điện thoại</td>
                                        <td>Iphone X</td>
                                        <td>12,000,000</td>
                                        <td>18,000,000</td>
                                        <td>khohangtong</td>
                                        <td>14/7/2018</td>
                                        <td>100</td>
                                        <td className="stt_icon" align="center"><i className="icon-check icons"></i></td>
                                    </tr>
                                    <tr>
                                        <td>Cửa hàng 1</td>
                                        <td>Điện thoại</td>
                                        <td>Iphone X</td>
                                        <td>12,000,000</td>
                                        <td>18,000,000</td>
                                        <td>khohangtong</td>
                                        <td>14/7/2018</td>
                                        <td>100</td>
                                        <td className="stt_icon" align="center"><i className="icon-close icons"></i></td>
                                    </tr>
                                    <tr>
                                        <td>Cửa hàng 1</td>
                                        <td>Điện thoại</td>
                                        <td>Iphone X</td>
                                        <td>12,000,000</td>
                                        <td>18,000,000</td>
                                        <td>khohangtong</td>
                                        <td>14/7/2018</td>
                                        <td>100</td>
                                        <td className="stt_icon" align="center"><i className="icon-check icons"></i></td>
                                    </tr>
                                    <tr>
                                        <td>Cửa hàng 1</td>
                                        <td>Điện thoại</td>
                                        <td>Iphone X</td>
                                        <td>12,000,000</td>
                                        <td>18,000,000</td>
                                        <td>khohangtong</td>
                                        <td>14/7/2018</td>
                                        <td>100</td>
                                        <td className="stt_icon" align="center"><i className="icon-check icons"></i></td>
                                    </tr>
                                </tbody>
                            </Table>
                        </CardBody>
                    </Card>
                </div>
            </div>
        )
    }
}
export default WarehouseManagement;
