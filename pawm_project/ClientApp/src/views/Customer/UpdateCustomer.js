﻿import React, { Component } from 'react';
import {
    Form,
    Row,
    Col,
    Card,
    CardHeader,
    CardBody,
    CardFooter,
    Button,
    FormGroup,
    Label,
    Input
} from 'reactstrap';
import { Link } from 'react-router-dom'
import { TextMask, InputAdapter } from 'react-text-mask-hoc';
import $ from 'jquery'

class UpdateCustomer extends Component {
    componentDidMount() {
        (function () {
            var forms = document.getElementsByClassName('form-customer-update');
            // Loop over them and prevent submission
            Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        })();

        $(document).on("keypress", "#CardId", function (evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57)) {
                return false;
            }
            return true;
        })

    }
    render() {
        return (
            <div className="update-customer-page">
                <Card>
                    <Form className="form-customer-update" noValidate>
                        <CardBody>
                            <Row>
                                <Col sm="4" xs="12">
                                    <FormGroup>
                                        <Label >Mã khách hàng</Label>
                                        <Input type="text" placeholder="Mã khách hàng" readOnly defaultValue="CH10001" required />
                                        <div className="invalid-feedback">
                                            Vui lòng nhập mã khách hàng.
                                    </div>
                                    </FormGroup>
                                </Col>
                                <Col sm="4" xs="12">
                                    <FormGroup>
                                        <Label>Cửa hàng</Label>
                                        <Input type="select" required>
                                            <option value="1">Cửa hàng 1</option>
                                            <option value="2">Cửa hàng 2</option>
                                            <option value="3">Cửa hàng 3</option>
                                            <option value="4">Cửa hàng 4</option>
                                        </Input>
                                        <div className="invalid-feedback">
                                            Vui lòng chọn cửa hàng.
                                </div>
                                    </FormGroup>
                                </Col>
                                <Col sm="4" xs="12">
                                    <FormGroup>
                                        <Label >Tên khách hàng</Label>
                                        <Input type="text" placeholder="Tên khách hàng" defaultValue="Nguyễn Văn A" required />
                                        <div className="invalid-feedback">
                                            Vui lòng nhập tên khách hàng.
                                </div>
                                    </FormGroup>
                                </Col>
                                <Col sm="4" xs="12">
                                    <FormGroup>
                                        <Label>Số điện thoại</Label>
                                        <TextMask
                                            mask={[/[0]/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/]}
                                            Component={InputAdapter}
                                            className="form-control"
                                            placeholder="Số điện thoại"
                                            type="text"
                                            value="123456789"
                                            required
                                        />
                                        <div className="invalid-feedback">
                                            Vui lòng nhập số điện thoại.
                                    </div>
                                    </FormGroup>
                                </Col>
                                <Col sm="4" xs="12">
                                    <FormGroup>
                                        <Label>Ngày sinh</Label>
                                        <TextMask
                                            mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
                                            Component={InputAdapter}
                                            className="form-control"
                                            placeholder="Ngày sinh"
                                            type="text"
                                            value="12-12-1995"
                                        />
                                    </FormGroup>
                                </Col>
                                <Col sm="4" xs="12">
                                    <FormGroup>
                                        <Label>Giới tính</Label> <br />
                                        <FormGroup check inline>
                                            <Input className="form-check-input" type="radio" id="inline-radio1" name="inline-radios" value="option1" defaultChecked />
                                            <Label className="form-check-label" check htmlFor="inline-radio1">Nam</Label>
                                        </FormGroup>
                                        <FormGroup check inline style={{ marginLeft: '50px' }}>
                                            <Input className="form-check-input" type="radio" id="inline-radio2" name="inline-radios" value="option2" />
                                            <Label className="form-check-label" check htmlFor="inline-radio2">Nữ</Label>
                                        </FormGroup>
                                    </FormGroup>
                                </Col>
                                <Col sm="4" xs="12">
                                    <FormGroup>
                                        <Label>Email</Label>
                                        <Input type="text" placeholder="Email" defaultValue="angv@gmail.com" />
                                    </FormGroup>
                                </Col>
                                <Col sm="4" xs="12">
                                    <FormGroup>
                                        <Label>CMND</Label>
                                        <Input type="text" placeholder="Chứng minh nhân dân" defaultValue="987654321" id="CardId"/>
                                    </FormGroup>
                                </Col>
                                
                                <Col sm="4" xs="12">
                                    <FormGroup>
                                        <Label>Địa chỉ</Label>
                                        <Input type="text" placeholder="Địa chỉ" defaultValue="Cộng Hòa Tân Bình" id="CardId" />
                                    </FormGroup>
                                </Col>
                                <Col xs="12">
                                    <FormGroup>
                                        <Label>Ghi chú</Label>
                                        <Input type="textarea" name="textarea-input" id="textarea-input" rows="2"
                                            placeholder="Ghi chú" defaultValue="Khách hàng nhiều lần trể hạn" />
                                    </FormGroup>
                                </Col>
                            </Row>
                        </CardBody>
                        <CardFooter>
                            <div className="button-right">
                                <Link to="/danh-sach-khach-hang" className="btn btn-primary" >
                                    <span className="icon-list"> </span>Danh sách
                            </Link>
                                <Button className="button_focus" color="primary" type="submit">
                                    <span className="fa fa-save"> </span> Lưu
                            </Button>
                            </div>
                        </CardFooter>
                    </Form>
                </Card>
            </div>
        )
    }
}
export default UpdateCustomer;
