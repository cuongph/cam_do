﻿import React, { Component } from 'react';
import {
    Row,
    Col,
    Card,
    CardHeader,
    CardBody,
    CardFooter,
    Button,
    Input,
    Table,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Form,
    FormGroup,
    Label,
    TabContent,
    TabPane,
    Nav,
    NavItem,
    NavLink
} from 'reactstrap';
import { Link } from 'react-router-dom'
import { TextMask, InputAdapter } from 'react-text-mask-hoc';
import classnames from 'classnames';
class InvoiceManagement extends Component {
    constructor(props) {
        super(props);
        this.state = {
            primary: false,
            activeTab: '1'
        };
        this.togglePrimary = this.togglePrimary.bind(this);
    }
    togglePrimary() {
        this.setState({
            primary: !this.state.primary
        });
    }
    render() {
        return (
            <div className="animated fadeIn">
                <div className="asset-page">
                    <Card>
                        <CardHeader>
                            <h1>Hóa đơn</h1>
                        </CardHeader>
                        <CardBody>
                            <Row>
                                <Col sm="4" xs="12">
                                    <label>Cửa hàng</label>
                                    <Input type="select" className="mb-3">
                                        <option value="1">Cửa hàng 1</option>
                                        <option value="2">Cửa hàng 2</option>
                                        <option value="3">Cửa hàng 3</option>
                                        <option value="4">Cửa hàng 4</option>
                                    </Input>
                                </Col>
                                <Col sm="4" xs="12">
                                    <label>Mã khách hàng</label>
                                    <Input type="text" placeholder="Mã khách hàng" className="mb-3" />
                                </Col>
                                <Col sm="4" xs="12">
                                    <label>Tên khách hàng</label>
                                    <Input type="text" placeholder="Tên khách hàng" />
                                </Col>
                                <Col sm="4" xs="12">
                                    <label>Mã hóa đơn</label>
                                    <Input type="text" placeholder="Mã hóa đơn" />
                                </Col>
                                <Col sm="4" xs="12">
                                    <label>Ngày lập</label>
                                    <Input type="text" placeholder="Ngày lập" className="mb-3" />
                                </Col>
                                <Col sm="4" xs="12">
                                    <label >Nhân viên</label>
                                    <Input type="text" placeholder="Nhân viên" className="mb-3" />
                                </Col>
                            </Row>
                        </CardBody>
                        <CardFooter>
                            <div className="button-right">
                                <Button color="primary button_focus">
                                    <span className="icon-magnifier"> </span>Tìm kiếm
                                </Button>
                            </div>
                        </CardFooter>
                    </Card>
                    <Card>
                        
                        <CardBody>
                            <Button color="primary export_excel">
                                <span className="icon-cloud-download"> </span>Xuất excel
                                </Button>
                            <Table responsive className="table-striped">
                                <thead>
                                    <tr>
                                        <th id="th-first">

                                        </th>
                                        <th>Mã HĐ</th>
                                        <th>Ngày</th>
                                        <th>Tên KH</th>
                                        <th>Nhân viên</th>
                                        <th>Tên TS</th>
                                        <th>Tổng tiền</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <Button className="btn-primary btn-sm" onClick={this.togglePrimary}>
                                                <span title="Chi tiết" className="fa fa-pencil"> </span>
                                            </Button>
                                        </td>
                                        <td>10001</td>
                                        <td>12/04/2018</td>
                                        <td>Nguyễn Văn A</td>
                                        <td>Nguyễn Kim Huyền</td>
                                        <td>Iphone 7plus</td>
                                        <td>7,000,000</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <Button className="btn-primary btn-sm" onClick={this.togglePrimary}>
                                                <span title="Chi tiết" className="fa fa-pencil"> </span>
                                            </Button>
                                        </td>
                                        <td>10001</td>
                                        <td>12/04/2018</td>
                                        <td>Nguyễn Văn A</td>
                                        <td>Nguyễn Kim Huyền</td>
                                        <td>Iphone 7plus</td>
                                        <td>7,000,000</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <Button className="btn-primary btn-sm" onClick={this.togglePrimary}>
                                                <span title="Chi tiết" className="fa fa-pencil"> </span>
                                            </Button>
                                        </td>
                                        <td>10001</td>
                                        <td>12/04/2018</td>
                                        <td>Nguyễn Văn A</td>
                                        <td>Nguyễn Kim Huyền</td>
                                        <td>Iphone 7plus</td>
                                        <td>7,000,000</td>
                                    </tr>
                                </tbody>
                            </Table>
                        </CardBody>
                    </Card>
                </div>
                {/*popup thanh toan*/}
                <Modal isOpen={this.state.primary} toggle={this.togglePrimary}
                    className={'modal-primary modal-lg ' + this.props.className}>
                    <ModalHeader toggle={this.togglePrimary}>Chi tiết hóa đơn</ModalHeader>
                    <ModalBody id="popup_detail">
                        <div className="form_popup">
                            <Form className="form-exchange-create" noValidate>
                                <Card>
                                    <CardBody>
                                        <Row>
                                            <Col sm="4" xs="12">
                                                <FormGroup>
                                                    <Label>Mã hóa đơn</Label>
                                                    <Input type="text" placeholder="1011010" required readOnly />
                                                    <div className="invalid-feedback">
                                                        Vui lòng nhập tên tài sản.
                                                    </div>
                                                </FormGroup>
                                            </Col>
                                            <Col sm="4" xs="12">
                                                <FormGroup>
                                                    <Label>Ngày</Label>
                                                    <TextMask
                                                        mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
                                                        Component={InputAdapter}
                                                        className="form-control"
                                                        placeholder="Ngày"
                                                        type="text"
                                                        required
                                                    />
                                                    <div className="invalid-feedback">
                                                        Vui lòng nhập ngày.
                        </div>
                                                </FormGroup>
                                            </Col>
                                            <Col sm="4" xs="12">
                                                <FormGroup>
                                                    <Label>Mã KH</Label>
                                                    <Input type="text" placeholder="Mã KH" required readOnly />
                                                    <div className="invalid-feedback">
                                                        Vui lòng nhập tên tài sản.
                                                    </div>
                                                </FormGroup>
                                            </Col>
                                            <Col sm="4" xs="12">
                                                <FormGroup>
                                                    <Label>Tên KH</Label>
                                                    <Input type="text" placeholder="Trịnh Thị Ngóc Miếng" required readOnly />
                                                    <div className="invalid-feedback">
                                                        Vui lòng nhập tên tài sản.
                                                    </div>
                                                </FormGroup>
                                            </Col>
                                            <Col sm="4" xs="12">
                                                <FormGroup>
                                                    <Label>Nhân viên</Label>
                                                    <Input type="text" placeholder="Trịnh Ngọc Quỳnh" required readOnly />
                                                    <div className="invalid-feedback">
                                                        Vui lòng nhập tên tài sản.
                                                    </div>
                                                </FormGroup>
                                            </Col>
                                        </Row>
                                        <Table responsive className="table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Ngày</th>
                                                    <th>Hình thức</th>
                                                    <th>Tên TS</th>
                                                    <th>Số tiền</th>
                                                    <th>Ghi chú</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>05/7/2018</td>
                                                    <td>Cầm đồ</td>
                                                    <td>Iphone 7Plus</td>
                                                    <td>10,000,000</td>
                                                    <td>Nhớ đóng lãi trước ngày...</td>
                                                </tr>
                                                <tr>
                                                    <td>15/8/2018</td>
                                                    <td>Đóng lãi</td>
                                                    <td></td>
                                                    <td>1,200,000</td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>17/11/2018</td>
                                                    <td>Gia hạn</td>
                                                    <td></td>
                                                    <td>500,000</td>
                                                    <td></td>
                                                </tr>
                                            </tbody>
                                        </Table>
                                    </CardBody>
                                    {/*<CardFooter>
                                        <div className="button-right">
                                            <Button className="button_focus" color="primary" type="submit">
                                                <span className="fa fa-save"> </span> Lưu
                                            </Button>
                                        </div>
                                    </CardFooter>*/}
                                </Card>
                            </Form>
                        </div>
                    </ModalBody>
                </Modal>
                {/*end popup thanh toan*/}
            </div>
        )
    }
}
export default InvoiceManagement;
