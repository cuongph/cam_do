﻿module.exports = {
    'login': require('./Login/Login').default,

    'dashboard': require('./Dashboard/Dashboard').default,

    'detail-asset-type': require('./Asset/AssetDetail').default,
    'list-asset-type': require('./Asset/AssetManagement').default,
    'create-asset-type': require('./Asset/CreateAsset').default,

    'detail-customer': require('./Customer/CustomerDetail').default,
    'list-customer': require('./Customer/CustomerManagement').default,
    'create-customer': require('./Customer/CreateCustomer').default,

    'list-invoice': require('./Invoice/InVoiceManagement').default,

    'create-liquidate': require('./Liquidate/CreateLiquidationCard').default,
    'exprire-liquidate': require('./Liquidate/ProductExpireManagement').default,

    'profile-settings': require('./Profile/ProfileUser').default,

    'detail-purchaser': require('./Purchaser/Purchaser_Detail').default,
    'list-purchaser': require('./Purchaser/Purchaser_Management').default,
    'create-purchaser': require('./Purchaser/Purchaser_Create').default,
    'view-purchaser': require('./Purchaser/Purchaser_View').default,
    'list-purchaser-new-asset': require('./Purchaser/Purchaser_ListAsset_New').default,
    'list-purchaser-update-asset': require('./Purchaser/Purchaser_ListAsset_NewUpdate').default,

    'detail-store': require('./Stores/Stores_Detail').default,
    'list-store': require('./Stores/Stores_Management').default,
    'create-store': require('./Stores/Stores_Create').default,

    'detail-staff': require('./User/User_Detail').default,
    'list-staff': require('./User/User_Management').default,
    'create-staff': require('./User/User_Create').default,

    'warehouse-management': require('./Warehouse/WarehouseManagement').default,
}
