﻿const routes = {
    //Home
    '/': 'Trang chủ',

    //Cá nhân
    '/thong-tin-ca-nhan': 'Thông tin cá nhân',

    //Dashboard
    '/thong-ke': 'Thống kê',

    //Hóa đơn
    '/danh-sach-hoa-don': 'Danh sách hóa đơn',

    //Quản lý kho
    '/quan-ly-kho': 'Quản lý kho',

    //Thanh lý
    '/giao-dich-het-han': 'Danh sách giao dịch hết hạn',
    '/giao-dich-het-han/them-phieu-thanh-ly': 'Thêm phiếu thanh lý',

    //CH
    '/danh-sach-cua-hang': 'Danh sách cửa hàng',
    '/them-cua-hang': 'Thêm cửa hàng',
    '/danh-sach-cua-hang/thong-tin-cua-hang': 'Thông tin cửa hàng',

    //KH
    '/danh-sach-khach-hang': 'Danh sách khách hàng',
    '/them-khach-hang': 'Thêm khách hàng',
    '/danh-sach-khach-hang/thong-tin-khach-hang': 'Thông tin khách hàng',
    '/danh-sach-khach-hang/them-giao-dich': 'Thêm giao dịch',
    '/danh-sach-khach-hang/thong-tin-giao-dich': 'Thông tin giao dịch',

    //Tài Sản
    '/danh-sach-loai-tai-san': 'Danh sách loại tài sản',
    '/them-loai-tai-san': 'Thêm loại tài sản',
    '/danh-sach-loai-tai-san/thong-tin-loai-tai-san': 'Thông tin loại tài sản',

    //NV
    '/danh-sach-nhan-vien': 'Danh sách nhân viên',
    '/them-nhan-vien': 'Thêm nhân viên',
    '/danh-sach-nhan-vien/thong-tin-nhan-vien': 'Thông tin nhân viên',

    //Thợ
    '/danh-sach-tho': 'Danh sách thợ',
    '/them-tho': 'Thêm thợ',
    '/danh-sach-tho/thong-tin-tho': 'Thông tin thợ',


    //Template
    '/components': 'Components',
    '/charts': 'Charts',
    '/components/buttons': 'Buttons',
    '/components/social-buttons': 'Social Buttons',
    '/components/cards': 'Cards',
    '/components/forms': 'Forms',
    '/components/modals': 'Modals',
    '/components/switches': 'Switches',
    '/components/tables': 'Tables',
    '/components/tabs': 'Tabs',
    '/forms': 'Forms',
    '/forms/basic-forms': 'Basic Forms',
    '/forms/advanced-forms': 'Advanced Forms',
    '/editors': 'Editors',
    '/editors/text-editors': 'Text Editors',
    '/icons': 'Icons',
    '/icons/font-awesome': 'Font Awesome',
    '/icons/simple-line-icons': 'Simple Line Icons',
    '/plugins': 'Plugins',
    '/plugins/datatable': 'Data Table',
    '/plugins/loading-buttons': 'Loading Buttons',
    '/plugins/notifications': 'Notifications',
    '/plugins/spinners': 'Spinners',
    '/widgets': 'Widgets',
    '/google-maps': 'Google Maps',
    '/ui-kits': 'UI Kits',
    '/ui-kits/invoicing': 'Invoicing',
    '/ui-kits/invoicing/invoice': 'Invoice',
    '/ui-kits/email/compose': 'Compose',
    '/ui-kits/email/inbox': 'Inbox',
    '/ui-kits/email/message': 'Message'
};
export default routes;
