﻿import React, { Component } from 'react';
import {
    Button,
    Row,
    Col,
    Label,
    Card,
    CardHeader,
    CardBody,
    Table,
    CardFooter,
    Input
} from 'reactstrap';
import { Link } from 'react-router-dom'
import * as helper from '../../modules/Helper'
class Purchaser_Management extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = { collapse: true };
    }

    toggle() {
        this.setState({ collapse: !this.state.collapse });
    }
    componentDidMount() {
    }
    render() {
        return (
            <div className="form-user" >
                <div className="form-management">
                    <div className="animated fadeIn">
                        <Card>
                            <CardHeader>
                                <h1> Danh sách thợ </h1>
                            </CardHeader>
                            <CardBody>
                                <Row>
                                    <Col sm="3" xs="12">
                                        <Label htmlFor="text-input">Mã thợ</Label>
                                        <Input type="text" placeholder="Mã thợ" />
                                    </Col>
                                    <Col sm="3" xs="12">
                                        <Label htmlFor="text-input">Tên thợ</Label>
                                        <Input type="text" placeholder="Tên thợ" />
                                    </Col>
                                    <Col sm="3" xs="12">
                                        <Label htmlFor="text-input">Số điện thoại</Label>
                                        <Input type="text" placeholder="Số điện thoại" />
                                    </Col>
                                    <Col sm="3" xs="12">
                                        <Label htmlFor="text-input">Địa chỉ</Label>
                                        <Input type="text" placeholder="Địa chỉ" />
                                    </Col>
                                </Row>
                            </CardBody>
                            <CardFooter>
                                <div className="button-right">

                                    <Link to={helper.getPathHost('purchaser', 'create-purchaser', 'url')} className="btn btn-primary" >
                                        <span className="icon-plus"> </span> Thêm mới
                                    </Link>
                                    <Button color="primary button_focus">
                                        <span className="icon-magnifier"> </span>Tìm kiếm
                                </Button>
                                </div>
                            </CardFooter>
                            <CardBody>
                                <Table responsive striped>
                                    <thead>
                                        <tr>
                                            <th width="50px" id="th-first"></th>
                                            <th width="90px">Mã thợ</th>
                                            <th>Tên thợ</th>
                                            <th>Số điện thoại</th>
                                            <th>Email</th>
                                            <th>Địa chỉ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td id="control">
                                                <Link to={helper.getPathHost('purchaser', 'detail-purchaser', 'url')} className="btn btn-primary btn-sm" >
                                                    <span title="Chi tiết" class="fa fa-pencil"> </span>
                                                </Link>
                                                <Button id="btnDel" color="danger" size="sm" >
                                                    <i title="Xóa" className="fa fa-trash"></i>
                                                </Button>
                                            </td>
                                            <td>THO0001</td>
                                            <td>Nguyễn Văn A</td>
                                            <td>0931548754</td>
                                            <td>anv@gmail.com</td>
                                            <td>123 BTX</td>
                                        </tr>
                                        <tr>
                                            <td id="control">
                                                <Link to={helper.getPathHost('purchaser', 'detail-purchaser', 'url')} className="btn btn-primary btn-sm" >
                                                    <span title="Chi tiết" class="fa fa-pencil"> </span>
                                                </Link>
                                                <Button id="btnDel" color="danger" size="sm" >
                                                    <i title="Xóa" className="fa fa-trash"></i>
                                                </Button>
                                            </td>
                                            <td>THO0002</td>
                                            <td>Nguyễn Thị B</td>
                                            <td>0931548756</td>
                                            <td>bnt@gmail.com</td>
                                            <td>123 BTBC</td>
                                        </tr>
                                        <tr>
                                            <td id="control">
                                                <Link to={helper.getPathHost('purchaser', 'detail-purchaser', 'url')} className="btn btn-primary btn-sm" >
                                                    <span title="Chi tiết" class="fa fa-pencil"> </span>
                                                </Link>
                                                <Button id="btnDel" color="danger" size="sm" >
                                                    <i title="Xóa" className="fa fa-trash"></i>
                                                </Button>
                                            </td>
                                            <td>THO0003</td>
                                            <td>Phan Văn C</td>
                                            <td>0931548775</td>
                                            <td>cpv@gmail.com</td>
                                            <td>123 BCLA</td>
                                        </tr>
                                        
                                    </tbody>
                                </Table>
                            </CardBody>
                        </Card>
                        {/* <Card>
                            <CardHeader>
                                <h1> Danh sách loại tài sản được yêu cầu giao dịch </h1>
                            </CardHeader>
                            <CardBody>
                                <Table responsive striped>
                                    <thead>
                                        <tr>
                                            <th width="50px" id="th-first"></th>
                                            <th width="50px">Mã thợ</th>
                                            <th width="190px">Tên thợ</th>
                                            <th width="100px">Số điện thoại</th>
                                            <th width="120px">Tên loại tài sản</th>
                                            <th width="120px">Giá Tiền</th>
                                            <th width="100px">Ngày bắt đầu</th>
                                            <th width="100px">Ngày kết thúc</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td id="control">
                                                <Button id="btnDuyet" color="primary" size="sm" >
                                                    <i className="fa fa-check-circle"></i>
                                                </Button>
                                            </td>
                                            <td>THO0001</td>
                                            <td>Nguyễn Văn A</td>
                                            <td>0963251548</td>
                                            <td>Xe máy</td>
                                            <td>10000000</td>
                                            <td>12/12/2018</td>
                                            <td>12/6/2019</td>
                                        </tr>
                                        <tr>
                                            <td id="control">
                                                <Button id="btnDuyet" color="primary" size="sm" >
                                                    <i className="fa fa-check-circle"></i>
                                                </Button>
                                            </td>
                                            <td>THO0001</td>
                                            <td>Nguyễn Văn A</td>
                                            <td>0963251548</td>
                                            <td>Xe máy</td>
                                            <td>10000000</td>
                                            <td>12/12/2018</td>
                                            <td>12/6/2019</td>
                                        </tr>
                                        <tr>
                                            <td id="control">
                                                <Button id="btnDuyet" color="primary" size="sm" >
                                                    <i className="fa fa-check-circle"></i>
                                                </Button>
                                            </td>
                                            <td>THO0001</td>
                                            <td>Nguyễn Văn A</td>
                                            <td>0963251548</td>
                                            <td>Xe máy</td>
                                            <td>10000000</td>
                                            <td>12/12/2018</td>
                                            <td>12/6/2019</td>
                                        </tr>

                                    </tbody>
                                </Table>
                            </CardBody>
                        </Card> */}
                    </div>
                </div>
            </div>
        )
    }
}
export default Purchaser_Management;