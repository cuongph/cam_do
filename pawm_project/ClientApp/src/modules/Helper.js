﻿import Config from '../../config/config'
import moment from 'moment';

export const getConfig = (key) => {
    return Config[key]
}

export function getPathHost(group, child, prefix) {
    return child ? (prefix ? _.find(Config.urlPath[group].child, { name: child })[prefix] :
        _.find(Config.urlPath[group].child, { name: child }).url) :
        prefix ? Config.urlPath[group][prefix] : Config.urlPath[group]
}

//var digits = function (num) {
//    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
//}
//function isNumber(evt) {
//    var iKeyCode = (evt.which) ? evt.which : evt.keyCode
//    if (iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
//        return false;

//    return true;
//}

export function getDate(code, input) {
    var date = moment(input).format('DD-MM-YYYY_HH-mm-ss')
    if (code !== undefined && code === 1)
        date = moment(input).format('YYYY-MM-DD HH:mm:ss')
    if (code !== undefined && code === 2)
        date = moment(input).format('YYYY-MM-DD')
    if (code !== undefined && code === 3)
        date = moment(input).format('YYYY-MM-DD_HH')
    if (code !== undefined && code === 4)
        date = moment(input).format('YYYY-MM')
    if (code !== undefined && code === 5)
        date = moment(input).format('hh:mm A')
    if (code !== undefined && code === 6)
        date = moment(input).add(20, 'years').format('YYYY-MM-DD')
    // logger.debug(date)
    return date
}
