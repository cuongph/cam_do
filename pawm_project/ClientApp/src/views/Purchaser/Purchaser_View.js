﻿import React, { Component } from 'react';
import {
    Row,
    Col,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Card,
    CardHeader,
    CardBody,
    CardFooter,
    Button,
    FormGroup,
    Label,
    Input,
    Table,
    Form,
    TabContent,
    TabPane,
    Nav,
    NavItem,
    NavLink
} from 'reactstrap';
import { Link } from 'react-router-dom';
import { TextMask, InputAdapter } from 'react-text-mask-hoc';
import classnames from 'classnames';
import * as helper from '../../modules/Helper'
//React select
import Select from 'react-select';
import '../../../public/asset/scss/vendors/react-select/react-select.scss';
// React DateRangePicker
import 'react-dates/initialize';
import { DateRangePicker } from 'react-dates';
import 'react-dates/lib/css/_datepicker.css';

const options = [
    { value: 'AL', label: 'Alabama', disabled: true },
    { value: 'AK', label: 'Alaska' },
    { value: 'AS', label: 'American Samoa' },
    { value: 'AZ', label: 'Arizona' },
    { value: 'AR', label: 'Arkansas' },
    { value: 'CA', label: 'California' },
];
const data = [
    {
        'loai': 'L001',
        'tenloai': 'Xe máy',
        'tentaisan':"Xe SH",
        'gia': '10000000',
        'ngaybd': '01/10/2018',
        'ngaykt': '01/12/2018',
    },
    {
        'loai': 'L001',
        'tenloai': 'Xe máy',
        'tentaisan': "Xe Airblade",
        'gia': '10000000',
        'ngaybd': '01/10/2018',
        'ngaykt': '01/12/2018',
    },
    {
        'loai': 'L001',
        'tenloai': 'Xe máy',
        'tentaisan': "Xe Wave",
        'gia': '10000000',
        'ngaybd': '01/10/2018',
        'ngaykt': '01/12/2018',
    },
];
class ViewPurchaser extends Component {
    constructor(props) {
        super(props);
        this.saveChanges = this.saveChanges.bind(this);
        this.updateDimensions = this.updateDimensions.bind(this);

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
        this.cellButton = this.cellButton.bind(this);
        this.cellButton_LoaiTSThuMua = this.cellButton_LoaiTSThuMua.bind(this);
        this.cellButton_Them = this.cellButton_Them.bind(this);
        this.togglePrimary = this.togglePrimary.bind(this);
        this.delItem = this.delItem.bind(this);
        this.toggle = this.toggle.bind(this);

        this.state = {
            //value: '',
            windowWidth: window.innerWidth,
            orientation: 'vertical',
            openDirection: 'down',
            data:data,
            tenloai: '',
            loai: '',
            gia: '',
            startDate: null,
            endDate: null,
            startDate1: null,
            endDate1: null,
            items: [],
            activeTab: '1',
        };
        this.options = {
            sortIndicator: true,
            hideSizePerPage: true,
            //sizePerPage: 25,
            paginationSize: 3,
            hidePageListOnlyOnePage: false,
            clearSearch: false,
            alwaysShowAllBtns: true,
            withFirstAndLast: false,
            prePage: '<<',
            nextPage: '>>',
            paginationShowsTotal: true,
            noDataText: 'Không có dữ liệu'
        }
    };
    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }
    togglePrimary() {
        this.setState({
            primary: !this.state.primary
        });
    }
    handleFormSubmit(e) {
        e.preventDefault();
        if (this.state.loai) {
            let items = [...this.state.items];

            items.push({ loai: this.state.loai, tenloai: this.state.tenloai, gia: this.state.gia, startDate: this.state.startDate ? helper.getDate(2, this.state.startDate) : '', endDate: this.state.endDate ? helper.getDate(2, this.state.endDate):'' });
            //console.log("items", items)
            this.setState({
                items,
                loai: '',
                gia: '',
                startDate: null,
                endDate: null,
            });
        }
    };
    delItem(e) {
        //e.preventDefault();
        console.log("asss", e.target)
        let items = [...this.state.items];
        delete items["1"];
        console.log("items", items)
    };
    handleInputChange(e) {
        let input = e.target;
        let name = e.target.name;
        //console.log("name", name)
        let value = input.value;
        console.log("value", name, value)
        this.setState({
            [name]: value
        })
    };
    
    componentDidMount() {
        this.updateDimensions();
        window.addEventListener('resize', this.updateDimensions);

        (function () {
            var forms = document.getElementsByClassName('form-create-purchaser');
            // Loop over them and prevent submission
            Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        })();
    }
    cellButton_LoaiTSThuMua(cell, row, enumObject, rowIndex) {
        return (
            <ButtonGroup>
                <Button id="btnUpdate" color="danger" size="sm" >
                    <span title="Chi tiết" class="fa fa-pencil"> </span>
                </Button>
                <Button id="btnDel" color="danger" size="sm" >
                    <i title="Xóa" class="fa fa-trash"></i>
                </Button>
            </ButtonGroup>
        )
    }
    //onClick = { this.delItem }
    cellButton_Them(cell, row, enumObject, rowIndex) {
        return (
            <Button id="btnDel" color="danger" size="sm"  >
                    <i className="icon-close no-mr"></i>
                </Button>
        )
    }
    cellButton(cell, row, enumObject, rowIndex) {
        return (
            <ButtonGroup>
                <Button id="btnUpdate" color="danger" size="sm" >
                    <i className="icon-pencil no-mr"></i>
                </Button>
                <Button id="btnDel" color="danger" size="sm" >
                    <i className="icon-close no-mr"></i>
                </Button>
            </ButtonGroup>
        )
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateDimensions);
    }

    saveChanges(value) {
        console.log(value);
        this.setState({ loai: value.value, tenloai: value.label });
    }

    updateDimensions() {
        const windowWidth = window.innerWidth;
        this.setState((state) => {
            return ({
                windowWidth: windowWidth,
                orientation: windowWidth < 620 ? 'vertical' : 'horizontal',
                openDirection: windowWidth < 620 ? 'up' : 'down'
            });
        });
    }
    //componentDidMount() {
    //    (function () {
    //        var forms = document.getElementsByClassName('form-create-purchaser');
    //        // Loop over them and prevent submission
    //        Array.prototype.filter.call(forms, function (form) {
    //            form.addEventListener('submit', function (event) {
    //                if (form.checkValidity() === false) {
    //                    event.preventDefault();
    //                    event.stopPropagation();
    //                }
    //                form.classList.add('was-validated');
    //            }, false);
    //        });
    //    })();
    //}
    render() {
        //console.log(new Date(this.state.startDate) + "/" + this.state.focusedInput)
        return (
            <div className="create-purchaser-page">
                <Form className="form-create-purchaser" noValidate>
                    <Card>
                        <div className="tab_customer tab_tho">
                            <Nav tabs>
                                <NavItem>
                                    <NavLink
                                        className={classnames({ active: this.state.activeTab === '1' })}
                                        onClick={() => { this.toggle('1'); }}
                                    >
                                        Thông tin cá nhân
                            </NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink
                                        className={classnames({ active: this.state.activeTab === '2' })}
                                        onClick={() => { this.toggle('2'); }}
                                    >
                                        Thêm thu mua
                            </NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink
                                        className={classnames({ active: this.state.activeTab === '3' })}
                                        onClick={() => { this.toggle('3'); }}
                                    >
                                        Thêm loại tài sản
                            </NavLink>
                                </NavItem>
                            </Nav>
                            <TabContent activeTab={this.state.activeTab}>
                                <TabPane tabId="1">
                                    <Card >
                                        <CardBody>
                                            <Row>
                                                <Col sm="3" xs="12">
                                                    <FormGroup>
                                                        <Label htmlFor="text-input">Họ tên</Label>
                                                        <Input type="text" id="text-input" name="text-input" placeholder="Họ tên" required />
                                                        <div className="invalid-feedback">
                                                            Vui lòng nhập họ tên.
                                </div>
                                                    </FormGroup>
                                                </Col>
                                                <Col sm="3" xs="12">
                                                    <FormGroup>
                                                        <Label htmlFor="text-input">Email</Label>
                                                        <Input type="text" id="text-input" name="text-input" placeholder="Email" />
                                                    </FormGroup>
                                                </Col>
                                                <Col sm="3" xs="12">
                                                    <FormGroup>
                                                        <Label htmlFor="text-input">Số điện thoại</Label>
                                                        <TextMask
                                                            mask={[/[0]/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/]}
                                                            Component={InputAdapter}
                                                            className="form-control"
                                                            placeholder="Số điện thoại"
                                                            type="text" id="text-input" name="text-input"
                                                            required
                                                        />
                                                        <div className="invalid-feedback">
                                                            Vui lòng nhập số điện thoại.
                                    </div>
                                                    </FormGroup>
                                                </Col>
                                                <Col sm="3" xs="12">
                                                    <FormGroup>
                                                        <Label>Giới tính</Label> <br />
                                                        <FormGroup check inline>
                                                            <Input className="form-check-input" type="radio" id="inline-radio1" name="inline-radios" value="option1" />
                                                            <Label className="form-check-label" check htmlFor="inline-radio1">Nam</Label>
                                                        </FormGroup>
                                                        <FormGroup check inline style={{ marginLeft: '50px' }}>
                                                            <Input className="form-check-input" type="radio" id="inline-radio2" name="inline-radios" value="option2" />
                                                            <Label className="form-check-label" check htmlFor="inline-radio2">Nữ</Label>
                                                        </FormGroup>
                                                    </FormGroup>
                                                </Col>
                                                <Col sm="3" xs="12">
                                                    <FormGroup>
                                                        <Label htmlFor="text-input">Ngày sinh</Label>
                                                        <TextMask
                                                            mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
                                                            Component={InputAdapter}
                                                            className="form-control"
                                                            placeholder="Ngày sinh"
                                                            type="text" id="text-input" name="text-input"
                                                        />
                                                    </FormGroup>
                                                </Col>
                                                <Col sm="6" xs="12">
                                                    <FormGroup>
                                                        <Label htmlFor="text-input">Địa chỉ</Label>
                                                        <Input type="text" name="textarea-input" id="textarea-input" rows="1" placeholder="Địa chỉ" />
                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                            <CardFooter>
                                                <div className="button-right">
                                                    <Button className="button_focus" color="primary" type="submit">
                                                        <span className="fa fa-save"> </span> Lưu
                                            </Button>
                                                </div>
                                            </CardFooter>
                                        </CardBody>
                                        
                                    </Card>
                                </TabPane>
                                <TabPane tabId="2">
                                    <Card >
                                        <CardBody>
                                            <Card>
                                                <Row>
                                                    <Col sm="4" xs="12">
                                                        <FormGroup>
                                                            <Label htmlFor="text-input">Loại tài sản</Label>
                                                            <Select
                                                                //name="form-field-name2"
                                                                name="loai"
                                                                value={this.state.loai}
                                                                options={options}
                                                                onChange={this.saveChanges}
                                                            />
                                                        </FormGroup>
                                                    </Col>
                                                    <Col sm="4" xs="12">
                                                        <FormGroup>
                                                            <Label htmlFor="text-input">Tên tài sản</Label>
                                                            <Input type="text" id="tentaisan" name="tentaisan" value={this.state.tentaisan} onChange={this.handleInputChange} placeholder="Tên tài sản" />

                                                        </FormGroup>
                                                    </Col>
                                                    <Col sm="4" xs="12">
                                                        <FormGroup>
                                                            <Label htmlFor="text-input">Giá tiền</Label>
                                                            <Input type="text" id="gia" name="gia" value={this.state.gia} onChange={this.handleInputChange} placeholder="Giá tiền" />

                                                        </FormGroup>
                                                    </Col>
                                                    <Col sm="4" xs="12">
                                                        <FormGroup>
                                                            <Label>Từ ngày</Label>
                                                            <TextMask
                                                                mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
                                                                Component={InputAdapter}
                                                                className="form-control"
                                                                placeholder="Từ ngày"
                                                                type="text"
                                                            />
                                                        </FormGroup>
                                                    </Col>
                                                    <Col sm="4" xs="12">
                                                        <FormGroup>
                                                            <Label>Đến ngày</Label>
                                                            <TextMask
                                                                mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
                                                                Component={InputAdapter}
                                                                className="form-control"
                                                                placeholder="Đến ngày"
                                                                type="text"
                                                            />
                                                        </FormGroup>
                                                    </Col>
                                                </Row>
                                                <CardFooter>
                                                    <div className="button-right">
                                                        <Button className="button_focus" color="primary" type="button">
                                                            <span className="icon-plus no-mr"> </span> Thêm
                                    </Button>

                                                    </div>
                                                </CardFooter>
                                            </Card>
                                            <BootstrapTable data={this.state.data} version="5" bordered={false} striped hover options={this.options}>
                                                <TableHeaderColumn dataField='button' dataFormat={this.cellButton_LoaiTSThuMua} width='100px' dataAlign='center' />
                                                <TableHeaderColumn isKey dataField="tenloai" dataSort>Tên loại tài sản</TableHeaderColumn>
                                                <TableHeaderColumn dataField="tentaisan" dataSort>Tên tài sản</TableHeaderColumn>
                                                <TableHeaderColumn dataField="gia" dataSort>Giá Tiền</TableHeaderColumn>
                                                <TableHeaderColumn dataField="ngaybd" dataSort>Ngày Bắt Đầu</TableHeaderColumn>
                                                <TableHeaderColumn dataField="ngaykt" dataSort>Ngày Kết Thúc</TableHeaderColumn>
                                            </BootstrapTable>
                                        </CardBody>
                                    </Card>
                                </TabPane>
                                <TabPane tabId="3">
                                    <Card >
                                        <CardBody>
                                            <div className="App">
                                                <form onSubmit={this.handleFormSubmit}>
                                                    <Card>
                                                        <Row>
                                                            <Col sm="4" xs="12">
                                                                <FormGroup>
                                                                    <Label htmlFor="text-input">Loại tài sản</Label>
                                                                    <Input type="text" id="loaiTS" name="loaiTS" value={this.state.loaiTS} onChange={this.handleInputChange} placeholder="Loại tài sản" />

                                                                </FormGroup>
                                                            </Col>
                                                            <Col sm="4" xs="12">
                                                                <FormGroup>
                                                                    <Label htmlFor="text-input">Tên tài sản</Label>
                                                                    <Input type="text" id="tentaisan" name="tentaisan" value={this.state.tentaisan} onChange={this.handleInputChange} placeholder="Tên tài sản" />

                                                                </FormGroup>
                                                            </Col>
                                                            <Col sm="4" xs="12">
                                                                <FormGroup>
                                                                    <Label htmlFor="text-input">Giá tiền</Label>
                                                                    <Input type="text" id="giaTS" name="giaTS" value={this.state.giaTS} onChange={this.handleInputChange} placeholder="Giá tiền" />

                                                                </FormGroup>
                                                            </Col>
                                                            <Col sm="4" xs="12">
                                                                <FormGroup>
                                                                    <Label>Từ ngày</Label>
                                                                    <TextMask
                                                                        mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
                                                                        Component={InputAdapter}
                                                                        className="form-control"
                                                                        placeholder="Từ ngày"
                                                                        type="text"
                                                                    />
                                                                </FormGroup>
                                                            </Col>
                                                            <Col sm="4" xs="12">
                                                                <FormGroup>
                                                                    <Label>Đến ngày</Label>
                                                                    <TextMask
                                                                        mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
                                                                        Component={InputAdapter}
                                                                        className="form-control"
                                                                        placeholder="Đến ngày"
                                                                        type="text"
                                                                    />
                                                                </FormGroup>
                                                            </Col>
                                                        </Row>
                                                        <CardFooter>
                                                            <div className="button-right">
                                                                <Button className="button_focus" color="primary" type="button">
                                                                    <span className="icon-plus no-mr"> </span> Thêm
                                    </Button>

                                                            </div>
                                                        </CardFooter>
                                                        <BootstrapTable data={this.state.data} version="5" bordered={false} striped hover options={this.options}>
                                                            <TableHeaderColumn dataField='button' dataFormat={this.cellButton} width='100px' dataAlign='center' />
                                                            <TableHeaderColumn isKey dataField="tenloai" dataSort>Tên loại tài sản</TableHeaderColumn>
                                                            <TableHeaderColumn dataField="tentaisan" dataSort>Tên tài sản</TableHeaderColumn>
                                                            <TableHeaderColumn dataField="gia" dataSort>Giá Tiền</TableHeaderColumn>
                                                            <TableHeaderColumn dataField="ngaybd" dataSort>Ngày Bắt Đầu</TableHeaderColumn>
                                                            <TableHeaderColumn dataField="ngaykt" dataSort>Ngày Kết Thúc</TableHeaderColumn>
                                                        </BootstrapTable>
                                                    </Card>
                                                </form>
                                            </div>
                                        </CardBody>
                                    </Card>
                                </TabPane>
                            </TabContent>
                        </div>







                        
                    </Card>
                </Form>
            </div>
        )
    }
}
//class Table extends Component {
//    render() {
//        const items = this.props.items;
//        return (
//            <div id="Table">
//                <BootstrapTable data={items} version="5" bordered={false} striped hover options={this.props.options}>
//                    <TableHeaderColumn dataField='button' dataFormat={this.props.cellButton_Them} width='60px' dataAlign='center' />
//                    <TableHeaderColumn isKey dataField="loai" dataSort>Mã loại tài sản</TableHeaderColumn>
//                    <TableHeaderColumn dataField="tenloai" dataSort>Tên loại tài sản</TableHeaderColumn>
//                    <TableHeaderColumn dataField="gia" dataSort>Giá Tiền</TableHeaderColumn>
//                    <TableHeaderColumn dataField="startDate" dataSort>Ngày Bắt Đầu</TableHeaderColumn>
//                    <TableHeaderColumn dataField="endDate" dataSort>Ngày Kết Thúc</TableHeaderColumn>
//                </BootstrapTable>
//            </div>
//        );
//    }
//}
export default ViewPurchaser;