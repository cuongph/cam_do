﻿import React, { Component } from 'react';
import {
    Row,
    Col,
    Button,
    Card,
    CardHeader,
    CardFooter,
    CardBody,
    Form,
    FormGroup,
    Label,
    Input
} from 'reactstrap';
import { Link } from 'react-router-dom'
import { TextMask, InputAdapter } from 'react-text-mask-hoc';

class User_Detail extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = { collapse: true };
    }

    toggle() {
        this.setState({ collapse: !this.state.collapse });
    }
    componentDidMount() {
        (function () {
            var forms = document.getElementsByClassName('form-user-update');
            // Loop over them and prevent submission
            Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        })();
    }
    render() {
        return (

            <Card>
                <Form className="form-user-update" noValidate>
                    <CardHeader>
                        <h1>Thông tin nhân viên</h1>
                    </CardHeader>
                    <CardBody>
                        <Row>
                            <Col sm="3" xs="12">
                                <FormGroup>
                                    <Label htmlFor="text-input">Mã nhân viên</Label>
                                    <Input type="text" id="text-input" name="text-input" placeholder="Mã nhân viên..." defaultValue="NV0001" required readOnly />
                                </FormGroup>
                            </Col>
                            <Col sm="3" xs="12">
                                <FormGroup>
                                    <Label>Cửa hàng</Label>
                                    <Input type="select" name="select" id="select" required>
                                        <option value="1" >Cửa hàng 1</option>
                                        <option value="2" >Cửa hàng 2</option>
                                        <option value="3">Cửa hàng 3</option>
                                        <option value="4">Cửa hàng 4</option>
                                    </Input>
                                    <div className="invalid-feedback">
                                        Vui lòng chọn cửa hàng.
                                </div>
                                </FormGroup>
                            </Col>
                            <Col sm="3" xs="12">
                                <FormGroup>
                                    <Label htmlFor="text-input">Họ tên</Label>
                                    <Input type="text" id="text-input" name="text-input" placeholder="Họ tên..." defaultValue="Nguyễn Văn A" required />
                                    <div className="invalid-feedback">
                                        Vui lòng nhập họ tên.
                                </div>
                                </FormGroup>
                            </Col>
                            <Col sm="3" xs="12">
                                <FormGroup>
                                    <Label htmlFor="text-input">Email</Label>
                                    <Input type="text" id="text-input" name="text-input" placeholder="Email..." defaultValue="anv@gmail.com" />
                                </FormGroup>
                            </Col>
                            <Col sm="3" xs="12">
                                <FormGroup>
                                    <Label htmlFor="text-input">Số điện thoại</Label>
                                    <TextMask
                                        mask={[/[0]/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/]}
                                        Component={InputAdapter}
                                        className="form-control"
                                        placeholder="Số điện thoại..."
                                        type="text" id="text-input" name="text-input"
                                        value="0931548754"
                                        required
                                    />
                                    <div className="invalid-feedback">
                                        Vui lòng nhập số điện thoại.
                                </div>
                                </FormGroup>
                            </Col>
                            <Col sm="3" xs="12">
                                <FormGroup>
                                    <Label htmlFor="text-input">Ngày sinh</Label>
                                    <TextMask
                                        mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
                                        Component={InputAdapter}
                                        className="form-control"
                                        placeholder="Ngày sinh..."
                                        type="text" id="text-input" name="text-input"
                                        value="11/11/1996"
                                    />
                                </FormGroup>
                            </Col>
                            <Col sm="3" xs="12">
                                <FormGroup>
                                    <Label>Giới tính</Label> <br />
                                    <FormGroup check inline>
                                        <Input className="form-check-input" type="radio" id="inline-radio1" name="inline-radios" value="option1" defaultChecked="true" />
                                        <Label className="form-check-label" check htmlFor="inline-radio1">Nam</Label>
                                    </FormGroup>
                                    <FormGroup check inline style={{ marginLeft: '50px' }}>
                                        <Input className="form-check-input" type="radio" id="inline-radio2" name="inline-radios" value="option2" />
                                        <Label className="form-check-label" check htmlFor="inline-radio2">Nữ</Label>
                                    </FormGroup>
                                </FormGroup>
                            </Col>
                            <Col sm="3" xs="12">
                                <FormGroup>
                                    <Label>Khóa tài khoản</Label>
                                    <Input type="select" name="select" id="select">
                                        <option value="2" >Mở tài khoản</option>
                                        <option value="1" >Khóa tài khoản</option>
                                    </Input>
                                </FormGroup>
                            </Col>
                            <Col sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="text-input">Địa chỉ</Label>
                                    <Input type="text" rows="2" id="text-input" name="text-input" placeholder="Địa chỉ..." defaultValue="123 BTX" />
                                </FormGroup>
                            </Col>
                            <Col sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="text-input">Ghi chú</Label>
                                    <Input type="text" rows="2" id="text-input" name="text-input" placeholder="Địa chỉ..." defaultValue="123 BTX" />
                                </FormGroup>
                            </Col>
                        </Row>
                    </CardBody>
                    <CardFooter>
                        <div className="button-right">
                            <Link to="/danh-sach-nhan-vien" className="btn btn-primary" >
                                <span className="icon-list"> </span>Danh sách
                            </Link>
                            <Button color="primary" type="submit" className="button_focus">
                                <span className="fa fa-save"> </span> Lưu
                            </Button>
                        </div>
                    </CardFooter>
                </Form>
            </Card>
        )
    }
}
export default User_Detail;