﻿import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    DropdownMenu,
    DropdownToggle,
    Dropdown,
} from 'reactstrap';
import { Link } from 'react-router-dom'
import * as helper from '../../modules/Helper'

const propTypes = {
    accnt: PropTypes.bool
};
const defaultProps = {
    accnt: false
};

class HeaderDropdown extends Component {

    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            dropdownOpen: false,
        };
    }

    toggle() {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen
        });
    }

    dropAccnt() {
        return (
            <Dropdown nav isOpen={this.state.dropdownOpen} toggle={this.toggle}>
                <DropdownToggle nav>
                    <div className="user-profile">
                        <i className="icon-user"></i> <span>admin@admin.com</span>
                    </div>
                </DropdownToggle>
                <DropdownMenu right style={{ top: "18px" }}>
                    <Link to={helper.getPathHost('public', 'profile-settings', null)} className="btn dropdown-item" >
                        <i className="icon-user"> </i> Cá nhân
                    </Link>
                    <Link to={helper.getPathHost('public', 'login', null)} className="btn dropdown-item" >
                        <i className="icon-logout"> </i> Đăng xuất
                    </Link>
                </DropdownMenu>
            </Dropdown>
        );
    }

    render() {
        const { accnt } = this.props;
        return (
            accnt ? this.dropAccnt() : null
        );
    }
}

HeaderDropdown.propTypes = propTypes;
HeaderDropdown.defaultProps = defaultProps;

export default HeaderDropdown;
