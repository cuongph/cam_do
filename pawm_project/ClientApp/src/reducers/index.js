﻿import { combineReducers } from 'redux'
import Example from './rExample'
import { routerReducer } from 'react-router-redux';

export default combineReducers({
    Example,
    routing: routerReducer
})