﻿import React, { Component } from 'react';
import {
    Form,
    Row,
    Col,
    Card,
    CardHeader,
    CardBody,
    CardFooter,
    Button,
    FormGroup,
    Label,
    Input
} from 'reactstrap';
import { Link } from 'react-router-dom'
import { TextMask, InputAdapter } from 'react-text-mask-hoc';
import $ from 'jquery';

class CreateExchange extends Component {
    componentDidMount() {
        (function () {
            var forms = document.getElementsByClassName('form-exchange-create');
            // Loop over them and prevent submission
            Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        })();

        $('.img-box #file-upload').change(function () {
            //var i = $(this).prev('label').clone();
            //var fileName = $('#file-upload')[0].files[0].name;
            //$(this).prev('label').text(fileName);
            var file = $('.img-box #file-upload')[0].files[0];
            $(".img-box img").attr('src', URL.createObjectURL(file));
        });

        $(document).on("keypress", "#CardId , #HoldingAmount , #InterestArising ,#AmountPaid ,#InterestRate", function (evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57)) {
                return false;
            }
            return true;
        })

    }
    render() {
        return (
            <div className="create-exchange-page">
                <div id="CreateGD">
                    <Form className="form-exchange-create" noValidate>
                        <div className="img-box">
                            <img src="/asset/img/image-default.png" alt="Images" />
                            <label htmlFor="file-upload" className="custom-file-upload">
                                <i className="icon-cloud-upload" style={{ marginRight: '2px' }}></i> Chọn ảnh
                        </label>
                            <input id="file-upload" name='upload_cont_img' type="file" style={{ display: "none" }} multiple />
                        </div>
                        <Card >
                            <CardHeader>
                                <span>Tạo giao dịch</span>
                            </CardHeader>
                            <CardBody>
                                <Row>
                                    <Col sm="6" xs="8">
                                        <FormGroup>
                                            <Label >Mã khách hàng</Label>
                                            <Input type="text" placeholder="Mã khách hàng" readOnly defaultValue="CH10001" required />
                                            <div className="invalid-feedback">
                                                Vui lòng nhập mã khách hàng.
                                </div>
                                        </FormGroup>
                                    </Col>
                                    <Col sm="4" xs="12">
                                        <FormGroup>
                                            <Label>Từ ngày</Label>
                                            <TextMask
                                                mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
                                                Component={InputAdapter}
                                                className="form-control"
                                                placeholder="Từ ngày"
                                                type="text"
                                                required
                                            />
                                            <div className="invalid-feedback">
                                                Vui lòng nhập ngày.
                                            </div>
                                        </FormGroup>
                                    </Col>
                                    <Col sm="6" xs="12">
                                        <FormGroup>
                                            <Label >Tên khách hàng</Label>
                                            <Input type="text" placeholder="Tên khách hàng" defaultValue="Nguyễn Văn A" required readOnly />
                                            <div className="invalid-feedback">
                                                Vui lòng nhập tên khách hàng.
                                            </div>
                                        </FormGroup>
                                    </Col>
                                    <Col sm="4" xs="12">
                                        <FormGroup>
                                            <Label>Đến ngày</Label>
                                            <TextMask
                                                mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
                                                Component={InputAdapter}
                                                className="form-control"
                                                placeholder="Đến ngày"
                                                type="text"
                                            />
                                        </FormGroup>
                                    </Col>
                                    <Col sm="6" xs="12">
                                        <FormGroup>
                                            <Label>Số điện thoại</Label>
                                            <TextMask
                                                mask={[/[0]/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/]}
                                                Component={InputAdapter}
                                                className="form-control"
                                                placeholder="Số điện thoại"
                                                type="text" id="text-input" name="text-input"
                                                value="023456789"
                                                required
                                                readOnly
                                            />
                                            <div className="invalid-feedback">
                                                Vui lòng nhập số điện thoại.
                                            </div>
                                        </FormGroup>
                                    </Col>
                                    <Col sm="6" xs="12" className="mb-3">
                                        <FormGroup>
                                            <Label>Loại tài sản</Label>
                                            <Input type="select" required className="mb-2">
                                                <option value="1">Xe hơi</option>
                                                <option value="2">Xe máy</option>
                                                <option value="3">Điện thoại</option>
                                                <option value="4">Máy tính</option>
                                            </Input>
                                            <div className="invalid-feedback">
                                                Vui lòng chọn loại tài sản.
                                            </div>
                                            <FormGroup check inline>
                                                <Input className="form-check-input" type="checkbox" id="inline-radio1" name="inline-radios" value="option1" />
                                                <Label className="form-check-label" check htmlFor="inline-radio1">Giấy tờ xe</Label>
                                            </FormGroup>
                                            <FormGroup check inline>
                                                <Input className="form-check-input" type="checkbox" id="inline-radio2" name="inline-radios" value="option2" />
                                                <Label className="form-check-label" check htmlFor="inline-radio2">Chứng minh nhân dân</Label>
                                            </FormGroup>
                                            <FormGroup check inline>
                                                <Input className="form-check-input" type="checkbox" id="inline-radio3" name="inline-radios" value="option3" />
                                                <Label className="form-check-label" check htmlFor="inline-radio3">Bằng lái</Label>
                                            </FormGroup>
                                        </FormGroup>
                                    </Col>
                                    <Col sm="6" xs="12">
                                        <FormGroup>
                                            <Label>CMND</Label>
                                            <Input type="text" placeholder="Chứng minh nhân dân" defaultValue="987654321" id="CardId" readOnly />
                                        </FormGroup>
                                    </Col>
                                    <Col sm="4" xs="8">
                                        <FormGroup>
                                            <Label>Lãi</Label>
                                            <Input type="select" required>
                                                <option value="1">Theo ngày</option>
                                                <option value="2">Theo tháng</option>
                                            </Input>
                                            <div className="invalid-feedback">
                                                Vui lòng chọn lãi suất.
                                            </div>
                                        </FormGroup>
                                    </Col>
                                    <Col sm="2" xs="4">
                                        <FormGroup>
                                            <Label style={{ marginTop: "17px" }}></Label>
                                            <Input type="text" placeholder="Phát sinh khi chọn lãi" required id="InterestRate" />
                                            <div className="invalid-feedback">
                                                Không được bỏ trống.
                                    </div>
                                        </FormGroup>
                                    </Col>
                                    <Col sm="6" xs="12">
                                        <FormGroup>
                                            <Label>Tên tài sản</Label>
                                            <Input type="text" placeholder="Tên tài sản" required />
                                            <div className="invalid-feedback">
                                                Vui lòng nhập tên tài sản.
                                    </div>
                                        </FormGroup>
                                    </Col>
                                    <Col sm="6" xs="12">
                                        <FormGroup>
                                            <Label>Số tiền thanh toán</Label>
                                            <Input type="text" placeholder="Phát sinh tự động" readOnly required />
                                            <div className="invalid-feedback">
                                                Vui lòng nhập số tiền thanh toán.
                                    </div>
                                        </FormGroup>
                                    </Col>
                                    <Col sm="6" xs="12">
                                        <FormGroup>
                                            <Label>Số tiền cầm</Label>
                                            <Input type="text" placeholder="Số tiền cầm" required id="HoldingAmount" />
                                            <div className="invalid-feedback">
                                                Vui lòng nhập số tiền cầm.
                                    </div>
                                        </FormGroup>
                                    </Col>
                                    <Col sm="6" xs="12">
                                        <FormGroup>
                                            <Label>Số tiền thanh lý</Label>
                                            <Input type="text" placeholder="Số tiền thanh lý" required />
                                            <div className="invalid-feedback">
                                                Vui lòng nhập số tiền thanh lý.
                                    </div>
                                        </FormGroup>
                                    </Col>
                                    <Col xs="12" className="mb-3">
                                        <FormGroup >
                                            <Label>Ghi chú</Label>
                                            <Input type="textarea" name="textarea-input" id="textarea-input" rows="2"
                                                placeholder="Ghi chú" />
                                        </FormGroup>
                                    </Col>
                                </Row>
                            </CardBody>
                            <CardFooter>
                                <div className="button-right">
                                    <Link to="/danh-sach-khach-hang" className="btn btn-primary" >
                                        <span className="icon-list"> </span>Danh sách
                                </Link>
                                    <Button className="button_focus" color="primary" type="submit">
                                        <span className="fa fa-save"> </span> Lưu
                                </Button>
                                    <Button color="primary" type="button">
                                        <span className="fa fa-print"> </span> In phiếu
                                </Button>
                                </div>
                            </CardFooter>
                        </Card>
                    </Form>
                </div>
            </div>
        )
    }
}
export default CreateExchange;
