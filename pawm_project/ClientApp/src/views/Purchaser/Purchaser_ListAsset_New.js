﻿import React, { Component } from 'react';
import {
    Button,
    Row,
    Col,
    Card,
    CardHeader,
    CardBody,
    FormGroup,
    Label,
    Table,
    CardFooter,
    Input
} from 'reactstrap';
import { Link } from 'react-router-dom'
import { TextMask, InputAdapter } from 'react-text-mask-hoc';
import * as helper from '../../modules/Helper'
// React DateRangePicker
import 'react-dates/initialize';
import { DateRangePicker } from 'react-dates';
import 'react-dates/lib/css/_datepicker.css';

class Purchaser_AssetNew extends Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.updateDimensions = this.updateDimensions.bind(this);
        this.state = {
            collapse: true,
            orientation: 'vertical',
            openDirection: 'down',
            //startDate: null,
            //endDate: null,
        };
    }
    toggle() {
        this.setState({ collapse: !this.state.collapse });
    }
    updateDimensions() {
        const windowWidth = window.innerWidth;
        this.setState((state) => {
            return ({
                windowWidth: windowWidth,
                orientation: windowWidth < 620 ? 'vertical' : 'horizontal',
                openDirection: windowWidth < 620 ? 'up' : 'down'
            });
        });
    }
    componentWillUnmount() {
        window.removeEventListener('resize', this.updateDimensions);
    }
    componentDidMount() {
        this.updateDimensions();
        window.addEventListener('resize', this.updateDimensions);

        (function () {
            var forms = document.getElementsByClassName('form-create-purchaser');
            // Loop over them and prevent submission
            Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        })();
    }
    render() {
        return (
            <div className="form-user" >
                <div className="form-management">
                    <div className="animated fadeIn">
                        <Card>
                            <CardHeader>
                                <h1> Thêm loại tài sản </h1>
                            </CardHeader>
                            <CardBody>
                                <Row>
                                    <Col sm="3" xs="12">
                                        <Label>Mã thợ</Label>
                                        <Input type="text" placeholder="Mã thợ" />
                                    </Col>
                                    <Col sm="3" xs="12">
                                        <Label>Tên thợ</Label>
                                        <Input type="text" placeholder="Tên thợ" />
                                    </Col>
                                    <Col sm="3" xs="12">
                                        <Label>Số điện thoại</Label>
                                        <Input type="text" placeholder="Số điện thoại" />
                                    </Col>
                                    <Col sm="3" xs="12">
                                        <Label>Địa chỉ</Label>
                                        <Input type="text" placeholder="Địa chỉ" />
                                    </Col>
                                </Row>
                            </CardBody>
                            <CardFooter>
                                <div className="button-right">
                                    <Button color="primary button_focus">
                                        <span className="icon-magnifier"> </span>Tìm kiếm
                                </Button>
                                </div>
                            </CardFooter>
                            <CardBody>
                                <Table responsive striped>
                                    <thead>
                                        <tr>
                                            <th id="th-first"></th>
                                            <th>Mã thợ</th>
                                            <th>Tên thợ</th>
                                            <th>Số điện thoại</th>
                                            <th>Tên loại tài sản</th>
                                            <th>Tên tài sản</th>
                                            <th>Giá Tiền</th>
                                            <th>Ngày bắt đầu</th>
                                            <th>Ngày kết thúc</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td id="control">
                                                <Button id="btnDuyet" color="primary" size="sm" >
                                                    <i title="Đồng ý" className="fa fa-check-circle"></i>
                                                </Button>
                                                <button class="btn btn-danger btn-sm"><i title="Xóa" class="fa fa-trash"></i></button>
                                            </td>
                                            <td>THO0001</td>
                                            <td>Nguyễn Văn A</td>
                                            <td>0963251548</td>
                                            <td>Xe máy</td>
                                            <td>Wave</td>
                                            <td>10000000</td>
                                            <td>12/12/2018</td>
                                            <td>12/6/2019</td>
                                        </tr>
                                        <tr>
                                        <td id="control">
                                            <Button id="btnDuyet" color="primary" size="sm" >
                                                    <i title="Đồng ý" className="fa fa-check-circle"></i>
                                            </Button>
                                            <button class="btn btn-danger btn-sm"><i title="Xóa" class="fa fa-trash"></i></button>
                                        </td>
                                            <td>THO0001</td>
                                            <td>Nguyễn Văn A</td>
                                            <td>0963251548</td>
                                            <td>Xe máy</td>
                                            <td>Sirius</td>
                                            <td>10000000</td>
                                            <td>12/12/2018</td>
                                            <td>12/6/2019</td>
                                        </tr>
                                        <tr>
                                            <td id="control">
                                                <Button id="btnDuyet" color="primary" size="sm" >
                                                    <i title="Đồng ý" className="fa fa-check-circle"></i>
                                                </Button>
                                                <button class="btn btn-danger btn-sm"><i title="Xóa" class="fa fa-trash"></i></button>
                                            </td>
                                            <td>THO0001</td>
                                            <td>Nguyễn Văn A</td>
                                            <td>0963251548</td>
                                            <td>Xe máy</td>
                                            <td>Xe SH</td>
                                            <td>10000000</td>
                                            <td>12/12/2018</td>
                                            <td>12/6/2019</td>
                                        </tr>

                                    </tbody>
                                </Table>
                            </CardBody>
                        </Card>
                    </div>
                </div>
            </div>
        )
    }
}
export default Purchaser_AssetNew;