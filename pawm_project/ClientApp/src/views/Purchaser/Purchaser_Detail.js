﻿import React, { Component } from 'react';
import {
    Row,
    Col,
    Card,
    CardHeader,
    CardBody,
    CardFooter,
    Button,
    FormGroup,
    Label,
    Input,
    Form,
    TabContent,
    TabPane,
    Nav,
    NavItem,
    NavLink
} from 'reactstrap';
import { Link } from 'react-router-dom';
import { TextMask, InputAdapter } from 'react-text-mask-hoc';
import * as helper from '../../modules/Helper'
//React select
import Select from 'react-select';
import '../../../public/asset/scss/vendors/react-select/react-select.scss';
// React DateRangePicker
import 'react-dates/initialize';
import { DateRangePicker } from 'react-dates';
import 'react-dates/lib/css/_datepicker.css';

const options = [
    { value: 'AL', label: 'Alabama', disabled: true },
    { value: 'AK', label: 'Alaska' },
    { value: 'AS', label: 'American Samoa' },
    { value: 'AZ', label: 'Arizona' },
    { value: 'AR', label: 'Arkansas' },
    { value: 'CA', label: 'California' },
];
const data = [
    {
        'loai': 'L001',
        'tenloai': 'Xe máy',
        'tentaisan': "Xe SH",
        'gia': '10000000',
        'ngaybd': '01/10/2018',
        'ngaykt': '01/12/2018',
    },
    {
        'loai': 'L001',
        'tenloai': 'Xe máy',
        'tentaisan': "Xe Airblade",
        'gia': '10000000',
        'ngaybd': '01/10/2018',
        'ngaykt': '01/12/2018',
    },
    {
        'loai': 'L001',
        'tenloai': 'Xe máy',
        'tentaisan': "Xe Wave",
        'gia': '10000000',
        'ngaybd': '01/10/2018',
        'ngaykt': '01/12/2018',
    },
];
class DetailPurchaser extends Component {
    constructor(props) {
        super(props);
        //this.saveChanges = this.saveChanges.bind(this);
        //this.updateDimensions = this.updateDimensions.bind(this);

        //this.handleInputChange = this.handleInputChange.bind(this);
        //this.handleFormSubmit = this.handleFormSubmit.bind(this);
        this.cellButton = this.cellButton.bind(this)

        this.state = {
            //value: '',
            windowWidth: window.innerWidth,
            orientation: 'vertical',
            openDirection: 'down',

            tenloai: '',
            loai: '',
            gia: '',
            ngaybd: null,
            ngaykt: null,
            items: data
        };
        this.options = {
            sortIndicator: true,
            hideSizePerPage: true,
            //sizePerPage: 25,
            paginationSize: 3,
            hidePageListOnlyOnePage: false,
            clearSearch: false,
            alwaysShowAllBtns: true,
            withFirstAndLast: false,
            prePage: '<<',
            nextPage: '>>',
            paginationShowsTotal: true,
            noDataText: 'Không có dữ liệu'
        }
    };

    //handleFormSubmit(e) {
    //    console.log("e", e)
    //    e.preventDefault();

    //    let items = [...this.state.items];

    //    items.push({ loai: this.state.loai, tenloai: this.state.tenloai, gia: this.state.gia, startDate: helper.getDate(2, this.state.startDate), endDate: helper.getDate(2, this.state.endDate) });
    //    console.log("items", items)
    //    this.setState({
    //        items,
    //        loai: '',
    //        gia: '',
    //        startDate: null,
    //        endDate: null,
    //    });
    //};
    //handleInputChange(e) {
    //    let input = e.target;
    //    let name = e.target.name;
    //    //console.log("name", name)
    //    let value = input.value;
    //    console.log("value", name, value)
    //    this.setState({
    //        [name]: value
    //    })
    //};

    //tempChange(startDate, endDate) {
    //    this.setState({ startDate, endDate });
    //}
    componentDidMount() {
        this.updateDimensions();
        window.addEventListener('resize', this.updateDimensions);

        (function () {
            var forms = document.getElementsByClassName('form-create-purchaser');
            // Loop over them and prevent submission
            Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        })();
    }
    cellButton(cell, row, enumObject, rowIndex) {
        return (
            //<button
            //    type="button"
            //    onClick={() =>
            //        this.onClickProductSelected(cell, row, rowIndex)}
            //>
            //    Click me {rowIndex}
            //</button>

            //<Link to={"/"} className="btn btn-primary btn-sm" >
            //    <span className="icon-eye no-mr"> </span>
            //</Link>
            <Button id="btnDel" color="danger" size="sm" >
                <i className="icon-close no-mr"></i>
            </Button>
        )
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateDimensions);
    }

    //saveChanges(value) {
    //    this.setState({ loai: value.value, tenloai: value.label });
    //}

    updateDimensions() {
        const windowWidth = window.innerWidth;
        this.setState((state) => {
            return ({
                windowWidth: windowWidth,
                orientation: windowWidth < 620 ? 'vertical' : 'horizontal',
                openDirection: windowWidth < 620 ? 'up' : 'down'
            });
        });
    }
    //componentDidMount() {
    //    (function () {
    //        var forms = document.getElementsByClassName('form-create-purchaser');
    //        // Loop over them and prevent submission
    //        Array.prototype.filter.call(forms, function (form) {
    //            form.addEventListener('submit', function (event) {
    //                if (form.checkValidity() === false) {
    //                    event.preventDefault();
    //                    event.stopPropagation();
    //                }
    //                form.classList.add('was-validated');
    //            }, false);
    //        });
    //    })();
    //}
    render() {
        return (
            <div className="create-purchaser-page">
                <Form className="form-create-purchaser" noValidate>
                    <Card>

                        <CardHeader>
                            <h1>Thông tin thợ</h1>
                        </CardHeader>
                        <CardBody>
                            <Row>
                                <Col sm="3" xs="12">
                                    <FormGroup>
                                        <Label htmlFor="text-input">Mã thợ</Label>
                                        <Input type="text" id="text-input" name="text-input" placeholder="T0012" required readOnly />
                                        <div className="invalid-feedback">
                                            Vui lòng nhập họ tên.
                                </div>
                                    </FormGroup>
                                </Col>
                                <Col sm="3" xs="12">
                                    <FormGroup>
                                        <Label htmlFor="text-input">Họ tên</Label>
                                        <Input type="text" id="text-input" name="text-input" placeholder="Họ tên" required />
                                        <div className="invalid-feedback">
                                            Vui lòng nhập họ tên.
                                </div>
                                    </FormGroup>
                                </Col>
                                <Col sm="3" xs="12">
                                    <FormGroup>
                                        <Label htmlFor="text-input">Số điện thoại</Label>
                                        <TextMask
                                            mask={[/[0]/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/]}
                                            Component={InputAdapter}
                                            className="form-control"
                                            placeholder="Số điện thoại"
                                            type="text" id="text-input" name="text-input"
                                            required
                                        />
                                        <div className="invalid-feedback">
                                            Vui lòng nhập số điện thoại.
                                    </div>
                                    </FormGroup>
                                </Col>
                                <Col sm="3" xs="12">
                                    <FormGroup>
                                        <Label htmlFor="text-input">Email</Label>
                                        <Input type="text" id="text-input" name="text-input" placeholder="Email" />
                                    </FormGroup>
                                </Col>
                                <Col sm="3" xs="12">
                                    <FormGroup>
                                        <Label htmlFor="text-input">Ngày sinh</Label>
                                        <TextMask
                                            mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
                                            Component={InputAdapter}
                                            className="form-control"
                                            placeholder="Ngày sinh"
                                            type="text" id="text-input" name="text-input"
                                        />
                                    </FormGroup>
                                </Col>
                                <Col sm="3" xs="12">
                                    <FormGroup>
                                        <Label htmlFor="text-input">Địa chỉ</Label>
                                        <Input type="text" name="textarea-input" id="textarea-input" rows="1" placeholder="Địa chỉ" />
                                    </FormGroup>
                                </Col>
                                <Col sm="3" xs="12">
                                    <FormGroup>
                                        <Label>Giới tính</Label> <br />
                                        <FormGroup check inline>
                                            <Input className="form-check-input" type="radio" id="inline-radio1" name="inline-radios" value="option1" />
                                            <Label className="form-check-label" check htmlFor="inline-radio1">Nam</Label>
                                        </FormGroup>
                                        <FormGroup check inline style={{ marginLeft: '50px' }}>
                                            <Input className="form-check-input" type="radio" id="inline-radio2" name="inline-radios" value="option2" />
                                            <Label className="form-check-label" check htmlFor="inline-radio2">Nữ</Label>
                                        </FormGroup>
                                    </FormGroup>
                                </Col>
                                <Col sm="12" xs="12">
                                    <FormGroup>
                                        <Label htmlFor="text-input">Ghi chú</Label>
                                        <Input type="textarea" name="textarea-input" id="textarea-input" rows="1" placeholder="Thợ tốt" />
                                    </FormGroup>
                                </Col>
                            </Row>
                        </CardBody>
                        <CardFooter>
                            <div className="button-right">
                                <Link to="/danh-sach-tho" className="btn btn-primary" >
                                    <span className="icon-list"> </span>Danh sách
                            </Link>
                                <Button className="button_focus" color="primary" type="submit">
                                    <span className="fa fa-save"> </span> Lưu
                            </Button>
                            </div>
                        </CardFooter>
                        <CardBody>
                            <div className="App">
                                <Table items={this.state.items} options={this.options} cellButton={this.cellButton} />
                            </div>
                        </CardBody>
                    </Card>
                </Form>
            </div>
        )
    }
}
class Table extends Component {
    render() {
        const items = this.props.items;
        return (
            <div id="Table">
                <BootstrapTable data={items} version="5" bordered={false} striped hover options={this.props.options}>
                    <TableHeaderColumn isKey dataField="loai" dataSort>Mã loại tài sản</TableHeaderColumn>
                    <TableHeaderColumn dataField="tenloai" dataSort>Tên loại tài sản</TableHeaderColumn>
                    <TableHeaderColumn dataField="tentaisan" dataSort>Tên tài sản</TableHeaderColumn>
                    <TableHeaderColumn dataField="gia" dataSort>Giá Tiền</TableHeaderColumn>
                    <TableHeaderColumn dataField="ngaybd" dataSort>Ngày Bắt Đầu</TableHeaderColumn>
                    <TableHeaderColumn dataField="ngaykt" dataSort>Ngày Kết Thúc</TableHeaderColumn>
                </BootstrapTable>
            </div>
        );
    }
}
export default DetailPurchaser;