import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import { createBrowserHistory } from 'history';
import configureStore from './store/configureStore';
import { HashRouter, Route, Switch } from 'react-router-dom';

// Styles
//import './default_validate';

// Import Flag Icons Set
import 'flag-icon-css/css/flag-icon.min.css';
// Import Font Awesome Icons Set
import 'font-awesome/css/font-awesome.min.css';
// Import Simple Line Icons Set
import 'simple-line-icons/css/simple-line-icons.css';
import '../public/asset/scss/style.scss'
// Temp fix for reactstrap
import '../public/asset/scss/core/_dropdown-menu-right.scss'

// RouterFull
import RouterFull from './routers'
// Views
import iRouter from './views'
import * as helper from './modules/Helper'

// Create browser history to use in the Redux store
//const baseUrl = document.getElementsByTagName('base')[0].getAttribute('href');
const baseUrl = '/'
const history = createBrowserHistory({ basename: baseUrl });

// Get the application-wide store instance, prepopulating with state from the server where available.
const initialState = window.initialReduxState;
const store = configureStore(history, initialState);
const rootElement = document.getElementById('root');

ReactDOM.render(
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <HashRouter>
                <Switch>
                    <Route exact path={helper.getPathHost('public', 'login', null)} name="Login Page" component={iRouter['login']} />
                    <Route path="/" name="Home" component={RouterFull} />
                </Switch>
            </HashRouter>
        </ConnectedRouter>
    </Provider>,
    rootElement);
