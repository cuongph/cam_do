﻿import React, { Component } from 'react';
import {
    Form,
    Row,
    Col,
    Card,
    CardHeader,
    CardBody,
    CardFooter,
    Button,
    FormGroup,
    Label,
    Input
} from 'reactstrap';
import { Link } from 'react-router-dom'
import { TextMask, InputAdapter } from 'react-text-mask-hoc';
import $ from 'jquery'

class CreateLiquidationCard extends Component {
    componentDidMount() {
        (function () {
            var forms = document.getElementsByClassName('form-liquidation-create');
            // Loop over them and prevent submission
            Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        })();

        $('.img-box #file-upload').change(function () {
            //var i = $(this).prev('label').clone();
            //var fileName = $('#file-upload')[0].files[0].name;
            //$(this).prev('label').text(fileName);
            var file = $('.img-box #file-upload')[0].files[0];
            $(".img-box img").attr('src', URL.createObjectURL(file));
        });

        $(document).on("keypress", "#Money", function (evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57)) {
                return false;
            }
            return true;
        })
    }
    render() {
        return (
            <div className="create-liquidation-card-page">
                <Form className="form-liquidation-create" noValidate>
                    <div className="img-box">
                        <img src="/asset/img/image-default.png" alt="Images" />
                        <label htmlFor="file-upload" className="custom-file-upload">
                            <i className="icon-cloud-upload" style={{ marginRight: '2px' }}></i> Chọn ảnh
                        </label>
                        <input id="file-upload" name='upload_cont_img' type="file" style={{ display: "none" }} />
                    </div>
                    <Card>
                        <CardHeader>
                            <span>Thêm phiếu thanh lý</span>
                        </CardHeader>
                        <CardBody>
                            <Row>
                                <Col sm="6" xs="8">
                                    <FormGroup>
                                        <Label >Giá tiền</Label>
                                        <Input type="text" placeholder="Giá tiền" required id="Money" />
                                        <div className="invalid-feedback">
                                            Vui lòng giá tiền
                                </div>
                                    </FormGroup>
                                </Col>
                                <Col sm="4" xs="12">
                                    <FormGroup>
                                        <Label >Tên hàng hóa</Label>
                                        <Input type="text" placeholder="Tên hàng hóa" readOnly defaultValue="Iphone 7" required />
                                        <div className="invalid-feedback">
                                            Vui lòng nhập tên hàng hóa.
                                </div>
                                    </FormGroup>
                                </Col>
                                <Col sm="6" xs="12">
                                    <FormGroup>
                                        <Label >Thời gian quá hạn</Label>
                                        <Input type="text" placeholder="Thời gian quá hạn" required />
                                        <div className="invalid-feedback">
                                            Vui lòng nhập thời gian quá hạn.
                                </div>
                                    </FormGroup>
                                </Col>
                                <Col sm="4" xs="12">
                                    <FormGroup>
                                        <Label >Ngày thanh lý</Label>
                                        <TextMask
                                            mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
                                            Component={InputAdapter}
                                            className="form-control"
                                            placeholder="Ngày hiện tại"
                                            type="text"
                                            readOnly
                                            required
                                        />
                                    </FormGroup>
                                </Col>
                                <Col xs="12">
                                    <FormGroup>
                                        <Label>Ghi chú</Label>
                                        <Input type="textarea" rows="2" placeholder="Ghi chú" />
                                    </FormGroup>
                                </Col>
                            </Row>
                        </CardBody>
                        <CardFooter>
                            <div className="button-right">
                                <Link to="/giao-dich-het-han" className="btn btn-primary" >
                                    <span className="icon-list"> </span>Danh sách
                            </Link>
                                <Button className="button_focus" color="primary" type="submit">
                                    <span className="fa fa-save"> </span> Lưu
                            </Button>
                            </div>
                        </CardFooter>
                    </Card>
                </Form>
            </div>
        )
    }
}
export default CreateLiquidationCard;
