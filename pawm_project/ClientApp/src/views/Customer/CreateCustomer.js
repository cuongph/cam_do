﻿import React, { Component } from 'react';
import {
    Row,
    Col,
    Card,
    CardHeader,
    CardBody,
    CardFooter,
    Button,
    FormGroup,
    Label,
    Input,
    Form,
    Table,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
} from 'reactstrap';
import { Link } from 'react-router-dom'
import { TextMask, InputAdapter } from 'react-text-mask-hoc';
import $ from 'jquery'
//import ModalExist from '../Modals/mCustomerExist'

class CreateCustomer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            primary: false
        };

        this.togglePrimary = this.togglePrimary.bind(this);
    }

    togglePrimary() {
        this.setState({
            primary: !this.state.primary
        });
    }

    componentDidMount() {
        (function () {
            var forms = document.getElementsByClassName('form-customer-create');
            // Loop over them and prevent submission
            Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        })();

        $(document).on("keypress", "#CardId", function (evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57)) {
                return false;
            }
            return true;
        })

    }
    render() {
        return (
            <div className="create-customer-page addnew_customer">
                <Card>
                    <Form className="form-customer-create" noValidate>
                        <CardHeader>
                            <h1>Thêm khách hàng</h1>
                        </CardHeader>
                        <CardBody>
                            <Row>
                                <Col sm="4" xs="12">
                                    <FormGroup>
                                        <Label >Mã khách hàng</Label>
                                        <Input type="text" placeholder="Mã khách hàng" readOnly />

                                    </FormGroup>
                                </Col>
                                <Col sm="4" xs="12">
                                    <FormGroup>
                                        <Label>Cửa hàng</Label>
                                        <Input type="select" required>
                                            <option value="1">Cửa hàng 1</option>
                                            <option value="2">Cửa hàng 2</option>
                                            <option value="3">Cửa hàng 3</option>
                                            <option value="4">Cửa hàng 4</option>
                                        </Input>
                                        <div className="invalid-feedback">
                                            Vui lòng chọn cửa hàng.
                                    </div>
                                    </FormGroup>
                                </Col>
                                <Col sm="4" xs="12">
                                    <FormGroup>
                                        <Label >Tên khách hàng</Label>
                                        <Input type="text" placeholder="Tên khách hàng" required />
                                        <div className="invalid-feedback">
                                            Vui lòng nhập tên khách hàng.
                                    </div>
                                    </FormGroup>
                                </Col>
                                <Col sm="4" xs="12">
                                    <FormGroup>
                                        <Label>Số điện thoại</Label>
                                        <TextMask
                                            mask={[/[0]/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/]}
                                            Component={InputAdapter}
                                            className="form-control"
                                            placeholder="Số điện thoại"
                                            type="text" id="text-input" name="text-input"
                                            required
                                        />
                                        <div className="invalid-feedback">
                                            Vui lòng nhập số điện thoại.
                                    </div>
                                    </FormGroup>
                                </Col>
                                <Col sm="4" xs="12">
                                    <FormGroup>
                                        <Label>Ngày sinh</Label>
                                        <TextMask
                                            mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
                                            Component={InputAdapter}
                                            className="form-control"
                                            placeholder="Ngày sinh"
                                            type="text"
                                        />
                                    </FormGroup>
                                </Col>
                                <Col sm="4" xs="12">
                                    <FormGroup>
                                        <Label>Giới tính</Label> <br />
                                        <FormGroup check inline>
                                            <Input className="form-check-input" type="radio" id="inline-radio1" name="inline-radios" value="option1" />
                                            <Label className="form-check-label" check htmlFor="inline-radio1">Nam</Label>
                                        </FormGroup>
                                        <FormGroup check inline style={{ marginLeft: '10px' }}>
                                            <Input className="form-check-input" type="radio" id="inline-radio2" name="inline-radios" value="option2" />
                                            <Label className="form-check-label" check htmlFor="inline-radio2">Nữ</Label>
                                        </FormGroup>
                                    </FormGroup>
                                </Col>
                                <Col sm="4" xs="12">
                                    <FormGroup>
                                        <Label>Email</Label>
                                        <Input type="text" placeholder="Email" />
                                    </FormGroup>
                                </Col>
                                <Col sm="4" xs="12">
                                    <FormGroup>
                                        <Label>CMND</Label>
                                        <Input type="text" placeholder="Chứng minh nhân dân" id="CardId" />
                                    </FormGroup>
                                </Col>
                                <Col sm="4" xs="12">
                                    <FormGroup>
                                        <Label>Địa chỉ</Label>
                                        <Input type="text" name="textarea-input" id="textarea-input" rows="2"
                                            placeholder="Địa chỉ" />
                                    </FormGroup>
                                </Col>
                                <Col xs="12">
                                    <FormGroup>
                                        <Label>Ghi chú</Label>
                                        <Input type="textarea" name="textarea-input" id="textarea-input" rows="2"
                                            placeholder="Ghi chú" defaultValue="Khách hàng nhiều lần trể hạn" />
                                    </FormGroup>
                                </Col>
                            </Row>
                        </CardBody>
                        <CardFooter>
                            <div className="button-right">
                                <Link to="/danh-sach-khach-hang" className="btn btn-primary" >
                                    <span className="icon-list"> </span>Danh sách
                            </Link>
                                <Button className="button_focus" color="primary" type="submit" onClick={this.togglePrimary}>
                                    <span className="fa fa-save"> </span> Lưu
                            </Button>
                            </div>
                        </CardFooter>
                    </Form>
                </Card>
                {/* <Button color="primary" onClick={this.togglePrimary}>Primary modal</Button>
                <ModalExist
                    togglePrimary={this.togglePrimary}
                    primary={this.state.primary}
                />*/}
                <Modal isOpen={this.state.primary} toggle={this.togglePrimary}
                    className={'modal-primary modal-lg ' + this.props.className}>
                    <ModalHeader toggle={this.togglePrimary}>Danh sách khách hàng trùng</ModalHeader>
                    <ModalBody>
                        <Table responsive>
                            <thead>
                                <tr>
                                    <th>Mã KH</th>
                                    <th>Tên KH</th>
                                    <th>Số điện thoại</th>
                                    <th>Ngày sinh</th>
                                    <th>Cửa hàng</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>CH10001</td>
                                    <td>Nguyễn Văn A</td>
                                    <td>123456789</td>
                                    <td>30/12/1995</td>
                                    <td>Cầm đồ 01</td>
                                </tr>
                            </tbody>
                        </Table>
                    </ModalBody>
                </Modal>
            </div>
        )
    }
}
export default CreateCustomer;
