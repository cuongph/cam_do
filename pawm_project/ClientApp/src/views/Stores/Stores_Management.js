﻿import React, { Component } from 'react';
import {
    Button,
    Row,
    Col,
    FormGroup,
    Label,
    Input,
    Card,
    CardHeader,
    CardBody,
    CardFooter,
    Table
} from 'reactstrap';
import { Link } from 'react-router-dom'

import * as helper from '../../modules/Helper'

class Stores extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = { collapse: true };
    }

    toggle() {
        this.setState({ collapse: !this.state.collapse });
    }
    render() {
        return (
            <div className="form-stores" >
                <div className="form-management">
                    <div className="animated fadeIn">
                        <Row>
                            <Col xs="12" lg="12">
                                <Card>
                                    <CardHeader>
                                        <h1>Danh sách cửa hàng</h1>
                                    </CardHeader>
                                    <CardFooter>
                                        <div className="button-right button_creat_store">
                                            <Link to="/them-cua-hang" className="btn btn-primary button_focus" >
                                                <span className="icon-plus"> </span> Thêm mới
                                    </Link>
                                        </div>
                                    </CardFooter>
                                    {/*<div className="button_creat_store button-right">
                                        <Link to="/them-cua-hang" className="btn btn-primary button_focus" >
                                            <span className="icon-plus"> </span> Thêm cửa hàng
                                    </Link>
                                    </div>*/}
                                    <CardBody className="tablet_store">
                                        <Table responsive striped>
                                            <thead>
                                                <tr>
                                                    <th width="100px"></th>
                                                    <th width="200px">Mã cửa hàng</th>
                                                    <th width="220px">Tên cửa hàng</th>
                                                    <th width="200px">Số điện thoại</th>
                                                    <th>Địa chỉ</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td id="control">
                                                        <Link to={helper.getPathHost('store', 'detail-store', 'url')} className="btn btn-primary btn-sm" >
                                                            <span title="Chi tiết" class="fa fa-pencil"> </span>
                                                        </Link>
                                                        <Button id="btnDel" color="danger" size="sm" >
                                                            <i title="Xóa" className="fa fa-trash"></i>
                                                        </Button>
                                                    </td>
                                                    <td>CH0001</td>
                                                    <td>Yiorgos Avraamu</td>
                                                    <td>063154875</td>
                                                    <td>123 BTX</td>
                                                </tr>
                                                <tr>
                                                    <td id="control">
                                                        <Link to={helper.getPathHost('store', 'detail-store', 'url')} className="btn btn-primary btn-sm" >
                                                            <span title="Chi tiết" class="fa fa-pencil"> </span>
                                                        </Link>
                                                        <Button id="btnDel" color="danger" size="sm" >
                                                            <i title="Xóa" className="fa fa-trash"></i>
                                                        </Button>
                                                    </td>
                                                    <td>CH0001</td>
                                                    <td>Yiorgos Avraamu</td>
                                                    <td>063154875</td>
                                                    <td>123 BTX</td>
                                                </tr>
                                            </tbody>
                                        </Table>
                                    </CardBody>
                                </Card>
                            </Col>
                        </Row>
                    </div>
                </div>
            </div>
        )
    }
}
export default Stores;


//<div className="form_create hidden" >
//    <StoresCreate />
//</div>
//    <div className="form_detail hidden" >
//        <StoresDetail />
//    </div>
