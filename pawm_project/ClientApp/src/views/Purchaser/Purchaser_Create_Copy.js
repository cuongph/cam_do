﻿import React, { Component } from 'react';
import {
    Row,
    Col,
    Card,
    CardHeader,
    CardBody,
    CardFooter,
    Button,
    FormGroup,
    Label,
    Input,
    Form
} from 'reactstrap';
import { Link } from 'react-router-dom';
import { TextMask, InputAdapter } from 'react-text-mask-hoc';
//React select
import Select from 'react-select';
import '../../../public/asset/scss/vendors/react-select/react-select.scss';
// React DateRangePicker
import 'react-dates/initialize';
import { DateRangePicker } from 'react-dates';
import 'react-dates/lib/css/_datepicker.css';

const options = [
    { value: 'AL', label: 'Alabama', disabled: true },
    { value: 'AK', label: 'Alaska' },
    { value: 'AS', label: 'American Samoa' },
    { value: 'AZ', label: 'Arizona' },
    { value: 'AR', label: 'Arkansas' },
    { value: 'CA', label: 'California' },
];
class CreatePurchaser extends Component {
    constructor(props) {
        super(props);
        this.saveChanges = this.saveChanges.bind(this);
        this.updateDimensions = this.updateDimensions.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleFormSubmit = this.handleFormSubmit.bind(this);

        this.state = {
            value: [],
            windowWidth: window.innerWidth,
            orientation: 'vertical',
            openDirection: 'down',

            username: '',
            password: '',
            items: []
        }
    };

    handleFormSubmit(e) {
        e.preventDefault();

        let items = [...this.state.items];

        items.push({ username: this.state.username, password: this.state.password });
        this.setState({
            items,
            username: '',
            password: ''
        });
    };
    handleInputChange(e) {
        let input = e.target;
        let name = e.target.name;
        let value = input.value;

        this.setState({
            [name]: value
        })
    };

    componentDidMount() {
        this.updateDimensions();
        window.addEventListener('resize', this.updateDimensions);

        (function () {
            var forms = document.getElementsByClassName('form-create-purchaser');
            // Loop over them and prevent submission
            Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        })();
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateDimensions);
    }

    saveChanges(value) {
        this.setState({ value: value });
    }

    updateDimensions() {
        const windowWidth = window.innerWidth;
        this.setState((state) => {
            return ({
                windowWidth: windowWidth,
                orientation: windowWidth < 620 ? 'vertical' : 'horizontal',
                openDirection: windowWidth < 620 ? 'up' : 'down'
            });
        });
    }
    //componentDidMount() {
    //    (function () {
    //        var forms = document.getElementsByClassName('form-create-purchaser');
    //        // Loop over them and prevent submission
    //        Array.prototype.filter.call(forms, function (form) {
    //            form.addEventListener('submit', function (event) {
    //                if (form.checkValidity() === false) {
    //                    event.preventDefault();
    //                    event.stopPropagation();
    //                }
    //                form.classList.add('was-validated');
    //            }, false);
    //        });
    //    })();
    //}
    render() {
        return (
            <div className="create-purchaser-page">
                <Form className="form-create-purchaser" noValidate>
                    <Card>
                        <CardHeader>
                            <h1>Thêm thợ</h1>
                        </CardHeader>
                        <CardBody>
                            <Row>
                                <Col sm="6" xs="12">
                                    <FormGroup>
                                        <Label htmlFor="text-input">Họ tên</Label>
                                        <Input type="text" id="text-input" name="text-input" placeholder="Họ tên" required />
                                        <div className="invalid-feedback">
                                            Vui lòng nhập họ tên.
                                </div>
                                    </FormGroup>
                                </Col>
                                <Col sm="6" xs="12">
                                    <FormGroup>
                                        <Label htmlFor="text-input">Email</Label>
                                        <Input type="text" id="text-input" name="text-input" placeholder="Email" />
                                    </FormGroup>
                                </Col>
                                <Col sm="6" xs="12">
                                    <FormGroup>
                                        <Label htmlFor="text-input">Số điện thoại</Label>
                                        <TextMask
                                            mask={[/[0]/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/]}
                                            Component={InputAdapter}
                                            className="form-control"
                                            placeholder="Số điện thoại"
                                            type="text" id="text-input" name="text-input"
                                            required
                                        />
                                        <div className="invalid-feedback">
                                            Vui lòng nhập số điện thoại.
                                    </div>
                                    </FormGroup>
                                </Col>
                                <Col sm="6" xs="12">
                                    <FormGroup>
                                        <Label>Giới tính</Label> <br />
                                        <FormGroup check inline>
                                            <Input className="form-check-input" type="radio" id="inline-radio1" name="inline-radios" value="option1" />
                                            <Label className="form-check-label" check htmlFor="inline-radio1">Nam</Label>
                                        </FormGroup>
                                        <FormGroup check inline style={{ marginLeft: '50px' }}>
                                            <Input className="form-check-input" type="radio" id="inline-radio2" name="inline-radios" value="option2" />
                                            <Label className="form-check-label" check htmlFor="inline-radio2">Nữ</Label>
                                        </FormGroup>
                                    </FormGroup>
                                </Col>
                                <Col sm="6" xs="12">
                                    <FormGroup>
                                        <Label htmlFor="text-input">Ngày sinh</Label>
                                        <TextMask
                                            mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
                                            Component={InputAdapter}
                                            className="form-control"
                                            placeholder="Ngày sinh"
                                            type="text" id="text-input" name="text-input"
                                        />
                                    </FormGroup>
                                </Col>
                                <Col sm="6" xs="12">
                                    <FormGroup>
                                        <Label htmlFor="text-input">Địa chỉ</Label>
                                        <Input type="textarea" name="textarea-input" id="textarea-input" rows="1" placeholder="Địa chỉ" />
                                    </FormGroup>
                                </Col>
                            </Row>
                                
                            
                        </CardBody>
                        <CardFooter>
                            <div className="button-right">
                                <Link to="/danh-sach-tho" className="btn btn-primary" >
                                    <span className="icon-list"> </span>Danh sách
                            </Link>
                                <Button className="button_focus" color="primary" type="submit">
                                    <span className="fa fa-save"> </span> Lưu
                            </Button>
                            </div>
                        </CardFooter>
                    </Card>
                </Form>
                <Card>
                    <CardHeader>
                        <h2>Thêm loại tài sản</h2>
                    </CardHeader>
                    <CardBody>
                        <Row>
                            <Col sm="12" xs="12">
                                <FormGroup>
                                    <Label htmlFor="text-input">Loại tài sản</Label>
                                    <Select
                                        name="form-field-name2"
                                        value={this.state.value}
                                        options={options}
                                        onChange={this.saveChanges}

                                    />
                                </FormGroup>
                            </Col>
                            <Col sm="12" xs="12">
                                <FormGroup>
                                    <div>
                                        <Label htmlFor="text-input">Thời gian</Label>
                                    </div>
                                    <DateRangePicker
                                        startDate={this.state.startDate}
                                        startDateId="startDate"
                                        endDate={this.state.endDate}
                                        endDateId="endDate"
                                        onDatesChange={({ startDate, endDate }) => this.setState({ startDate, endDate })}
                                        focusedInput={this.state.focusedInput}
                                        onFocusChange={focusedInput => this.setState({ focusedInput })}
                                        orientation={this.state.orientation}
                                        openDirection={this.state.openDirection}
                                    />
                                </FormGroup>
                            </Col>
                        </Row>
                    </CardBody>
                    <CardFooter>
                        <div className="button-right">
                            <Button className="button_focus" color="primary" type="button">
                                <span className="fa fa-save"> </span> Add
                                            </Button>
                        </div>
                    </CardFooter>

                    <div className="App">
                        <FormFS handleFormSubmit={this.handleFormSubmit} handleInputChange={this.handleInputChange} newUsername={this.state.username} newPassword={this.state.password} />
                        <Table items={this.state.items} />
                    </div>
                </Card>
            </div>
        )
    }
}
class Table extends Component {
    render() {
        const items = this.props.items;
        return (
            <div id="Table">
                <table>
                    <tbody>
                        <tr>
                            <th>Username</th>
                            <th>Password</th>
                        </tr>
                        {items.map(item => {
                            return (
                                <tr key={item.username}>
                                    <td>{item.username}</td>
                                    <td>{item.password}</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        );
    }
}

class FormFS extends Component {
    render() {
        return (
            <div id="FormFS">
                <h3>Add a new item to the table:</h3>
                <form onSubmit={this.props.handleFormSubmit}>
                    <label htmlFor="username">
                        Username:
          <input id="username" value={this.props.newUsername} type="text" name="username" onChange={this.props.handleInputChange} />
                    </label>
                    <label htmlFor="password">
                        Password:
          <input id="password" value={this.props.newPassword} type="password" name="password" onChange={this.props.handleInputChange} />
                    </label>
                    <button type="submit" value="Submit">Add Item</button>
                </form>
            </div>
        );
    }
}
export default CreatePurchaser;