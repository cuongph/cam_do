import React, { Component } from 'react';

class Footer extends Component {
    componentDidMount() {
        $("button[name='btnBackToTop']").attr("static", true)
        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $("button[name='btnBackToTop']").attr("static", false)
                $("button[name='btnBackToTop']").css({
                    'right': '10px',
                    '-webkit-animation': 'moveleftBToT 0.5s ease-in',
                    'animation': 'moveleftBToT 0.5s ease-in'
                })
            } else {
                if ($("button[name='btnBackToTop']").attr("static") === "false") {
                    $("button[name='btnBackToTop']").css({
                        'right': '-50px',
                        '-webkit-animation': 'moverightBToT 0.5s ease-out',
                        'animation': 'moverightBToT 0.5s ease-out'
                    })
                }
            }
        });
        $(document).on("click", "button[name='btnBackToTop']", function () {
            $("html, body").animate({
                scrollTop: 0
            }, "slow");
            return false;
        });
    }
    render() {
        return (
            <button type="button" className="btn back-to-top" name="btnBackToTop">
                <i className="icon-arrow-up no-mr"></i>
            </button>
        )
    }
}

export default Footer;
