﻿import * as helper from '../../modules/Helper'

export default {
    items: [
        {
            name: 'Thống kê',
            url: helper.getPathHost('dashboard', 'dashboard', 'url'),
            role: helper.getPathHost('dashboard', 'dashboard', 'role'),
            icon: 'icon-pie-chart'
        },
        {
            name: 'Khách hàng',
            icon: 'icon-people',
            role: helper.getPathHost('customer', null, 'role'),
            children: [
                {
                    name: 'Thêm khách hàng',
                    url: helper.getPathHost('customer', 'create-customer', 'url'),
                    role: helper.getPathHost('customer', 'create-customer', 'role'),
                    //icon: 'icon-user-follow'
                },
                {
                    name: 'Danh sách khách hàng',
                    url: helper.getPathHost('customer', 'list-customer', 'url'),
                    role: helper.getPathHost('customer', 'list-customer', 'role'),
                    //icon: 'icon-people'
                }
            ]
        },
        {
            name: 'Thanh lý',
            url: helper.getPathHost('liquidate', 'exprire-liquidate', 'url'),
            role: helper.getPathHost('liquidate', 'exprire-liquidate', 'role'),
            icon: 'icon-speedometer'
        },
        {
            name: 'Hóa đơn',
            url: helper.getPathHost('invoice', 'list-invoice', 'url'),
            role: helper.getPathHost('invoice', 'list-invoice', 'role'),
            icon: 'icon-book-open'
        },
        {
            name: 'Quản lý kho',
            url: helper.getPathHost('warehouse', 'warehouse-management', 'url'),
            role: helper.getPathHost('warehouse', 'warehouse-management', 'role'),
            icon: 'icon-home'
        },

        {
            name: 'Loại tài sản',
            icon: 'icon-rocket',
            role: helper.getPathHost('asset-type', null, 'role'),
            children: [
                {
                    name: 'Thêm loại tài sản',
                    url: helper.getPathHost('asset-type', 'create-asset-type', 'url'),
                    role: helper.getPathHost('asset-type', 'create-asset-type', 'role'),
                    //icon: 'icon-plus'
                },
                {
                    name: 'Danh sách loại tài sản',
                    url: helper.getPathHost('asset-type', 'list-asset-type', 'url'),
                    role: helper.getPathHost('asset-type', 'list-asset-type', 'role'),
                    //icon: 'icon-layers'
                }
            ]
        },
        {
            name: 'Cửa hàng',
            icon: 'icon-basket-loaded',
            role: helper.getPathHost('store', null, 'role'),
            children: [
                {
                    name: 'Thêm cửa hàng',
                    url: helper.getPathHost('store', 'create-store', 'url'),
                    role: helper.getPathHost('store', 'create-store', 'role'),
                    //icon: 'icon-plus'
                },
                {
                    name: 'Danh sách cửa hàng',
                    url: helper.getPathHost('store', 'list-store', 'url'),
                    role: helper.getPathHost('store', 'list-store', 'role'),
                    //icon: 'icon-home'
                }
            ]
        },
        {
            name: 'Nhân viên',
            icon: 'icon-user',
            role: helper.getPathHost('staff', null, 'role'),
            children: [
                {
                    name: 'Thêm nhân viên',
                    url: helper.getPathHost('staff', 'create-staff', 'url'),
                    role: helper.getPathHost('staff', 'create-staff', 'role'),
                    //icon: 'icon-user-follow'
                },
                {
                    name: 'Danh sách nhân viên',
                    url: helper.getPathHost('staff', 'list-staff', 'url'),
                    role: helper.getPathHost('staff', 'list-staff', 'role'),
                    //icon: 'icon-user'
                }
            ]
        },
        {
            name: 'Thợ',
            icon: 'icon-user',
            role: helper.getPathHost('purchaser', null, 'role'),
            children: [
                {
                    name: 'Thêm thợ',
                    url: helper.getPathHost('purchaser', 'create-purchaser', 'url'),
                    role: helper.getPathHost('purchaser', 'create-purchaser', 'role'),
                    //icon: 'icon-user-follow'
                },
                {
                    name: 'Danh sách thợ',
                    url: helper.getPathHost('purchaser', 'list-purchaser', 'url'),
                    role: helper.getPathHost('purchaser', 'list-purchaser', 'role'),
                    //icon: 'icon-user'
                },
                {
                    name: 'Thông tin thợ',
                    url: helper.getPathHost('purchaser', 'view-purchaser', 'url'),
                    role: helper.getPathHost('purchaser', 'view-purchaser', 'role'),
                    //icon: 'icon-user-follow'
                },
                {
                    name: 'Thợ yêu cầu',
                    url: helper.getPathHost('purchaser', 'list-purchaser-update-asset', 'url'),
                    role: helper.getPathHost('purchaser', 'list-purchaser-update-asset', 'role'),
                    //icon: 'icon-user-follow'
                },
                {
                    name: 'Thêm loại tài sản',
                    url: helper.getPathHost('purchaser', 'list-purchaser-new-asset', 'url'),
                    role: helper.getPathHost('purchaser', 'list-purchaser-new-asset', 'role'),
                    //icon: 'icon-user-follow'
                },
            ]
        },
    ]
};
