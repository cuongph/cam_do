// <binding BeforeBuild='sass' />
var gulp = require("gulp"),
    fs = require("fs"),
    less = require("gulp-less");
var sass = require("gulp-sass");

//gulp.task("less", function () {
//    return gulp.src('Styles/main.less')
//        .pipe(less())
//        .pipe(gulp.dest('wwwroot/css'));
//});

gulp.task("sass", function () {
    return gulp.src('ClientApp/public/asset/scss/style.scss')
        .pipe(sass())
        .pipe(gulp.dest('ClientApp/src'));
});
