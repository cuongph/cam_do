﻿import React, { Component } from 'react';
import {
    Row,
    Col,
    Card,
    CardHeader,
    CardBody,
    CardFooter,
    Button,
    Input,
    Tags,
    Table,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Form,
    FormGroup,
    Label,
    TabContent,
    TabPane,
    Nav,
    NavItem,
    NavLink
} from 'reactstrap';
import { TextMask, InputAdapter } from 'react-text-mask-hoc';
import classnames from 'classnames';
import { Link } from 'react-router-dom'
import * as helper from '../../modules/Helper'
import TagsInput from 'react-tagsinput'
import 'react-tagsinput/react-tagsinput.css'

class AssetManagement extends Component {
    constructor(props) {
        super(props);
        this.state = {
            primary: false,
            primary1: false,
            tags: [] ,
            activeTab: '1'
        };
        this.togglePrimary = this.togglePrimary.bind(this);
        this.togglePrimary1 = this.togglePrimary1.bind(this);
        this.handleChangeTagsInput = this.handleChangeTagsInput.bind(this);
    }
    handleChangeTagsInput(tags) {
        this.setState({ tags });
    }
    togglePrimary() {
        this.setState({
            primary: !this.state.primary
        });
    }
    togglePrimary1() {
        this.setState({
            primary1: !this.state.primary1
        });
    }
    render() {
        return (
            <div className="animated fadeIn">
                <div className="asset-page">
                    <Card>
                        <CardHeader>
                            <h1>Danh sách Loại tài sản</h1>
                        </CardHeader>
                        <CardBody>
                            <Row>
                                <Col xs="6">
                                    <Label>Mã loại tài sản</Label>
                                    <Input type="text" placeholder="Mã loại tài sản" />
                                </Col>
                                <Col xs="6">
                                    <Label>Tên loại tài sản</Label>
                                    <Input type="text" placeholder="Tên loại tài sản" data-role="tagsinput" />
                                </Col>
                            </Row>
                        </CardBody>
                        <CardFooter>
                            <div className="button-right">
                                <Link to="/them-loai-tai-san" to={helper.getPathHost('asset-type', 'create-asset-type', 'url')} className="btn btn-primary " >
                                    <span className="icon-plus"> </span> Thêm mới
                                    </Link>
                                <Button color="primary  button_focus">
                                    <span className="icon-magnifier"> </span>Tìm kiếm
                                </Button>
                                
                            </div>
                        </CardFooter>
                    </Card>
                    <Card>
                        
                        <CardBody>
                            <Table responsive className="table-striped">
                                <thead>
                                    <tr>
                                        <th width="100px" id="th-first"></th>
                                        <th>Mã loại TS</th>
                                        <th>Tên loại TS</th>
                                        <th>Ghi chú</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td id="control">
                                            
                                            <Button className="btn-primary btn-sm" onClick={this.togglePrimary1}>
                                                <span title="Chi tiết" class="fa fa-pencil"> </span>
                                            </Button>
                                            <Button id="btnDel" color="danger" size="sm" >
                                                <i title="Xóa" className="fa fa-trash"></i>
                                            </Button>
                                        </td>
                                        <td>TS01</td>
                                        <td>Điện thoại</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td id="control">
                                            <Button className="btn-primary btn-sm" onClick={this.togglePrimary1}>
                                                <span title="Chi tiết" class="fa fa-pencil"> </span>
                                            </Button>
                                            <Button id="btnDel" color="danger" size="sm" >
                                                <i title="Xóa" className="fa fa-trash"></i>
                                            </Button>
                                        </td>
                                        <td>TS01</td>
                                        <td>Điện thoại</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td id="control">
                                            <Button className="btn-primary btn-sm" onClick={this.togglePrimary1}>
                                                <span title="Chi tiết" class="fa fa-pencil"> </span>
                                            </Button>
                                            <Button id="btnDel" color="danger" size="sm" >
                                                <i title="Xóa" className="fa fa-trash"></i>
                                            </Button>
                                        </td>
                                        <td>TS01</td>
                                        <td>Điện thoại</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td id="control">
                                            <Button className="btn-primary btn-sm" onClick={this.togglePrimary1}>
                                                <span title="Chi tiết" class="fa fa-pencil"> </span>
                                            </Button>
                                            <Button id="btnDel" color="danger" size="sm" >
                                                <i title="Xóa" className="fa fa-trash"></i>
                                            </Button>
                                        </td>
                                        <td>TS01</td>
                                        <td>Điện thoại</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td id="control">
                                            <Button className="btn-primary btn-sm" onClick={this.togglePrimary1}>
                                                <span title="Chi tiết" class="fa fa-pencil"> </span>
                                            </Button>
                                            <Button id="btnDel" color="danger" size="sm" >
                                                <i title="Xóa" className="fa fa-trash"></i>
                                            </Button>
                                        </td>
                                        <td>TS01</td>
                                        <td>Điện thoại</td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </Table>

                            <Modal isOpen={this.state.primary1} toggle={this.togglePrimary1}
                                className={'modal-primary modal-lg ' + this.props.className}>
                                <ModalHeader toggle={this.togglePrimary1}>Chi tiết loại tài sản</ModalHeader>
                                <ModalBody id="popup_detail">
                                    <div className="form_popup">
                                        <Form className="form-asset-detail" noValidate>
                                            <Card>
                                                <CardBody>
                                                    <Row>
                                                        <Col sm="3" xs="12">
                                                            <FormGroup>
                                                                <Label >Mã loại tài sản</Label>
                                                                <Input type="text" placeholder="Mã loại tài sản" defaultValue="TS01" readOnly />
                                                            </FormGroup>
                                                        </Col>
                                                        <Col sm="3" xs="12">
                                                            <FormGroup>
                                                                <Label >Tên loại tài sản</Label>
                                                                <Input type="text" placeholder="Tên loại tài sản" defaultValue="Điện thoại" required />
                                                                <div className="invalid-feedback">
                                                                    Vui lòng nhập tên tài sản.
                                        </div>
                                                            </FormGroup>
                                                        </Col>
                                                        <Col xs="12">
                                                            <FormGroup>
                                                                <Label>Tên tài sản</Label>
                                                                <TagsInput inputProps={{ placeholder: "Tên tài sản" }} value={this.state.tags} onChange={this.handleChangeTagsInput} />
                                                            </FormGroup>
                                                        </Col>
                                                        <Col xs="12">
                                                            <FormGroup>
                                                                <Label>Ghi chú</Label>
                                                                <Input type="textarea" name="textarea-input" id="textarea-input" rows="2"
                                                                    placeholder="Ghi chú" />
                                                            </FormGroup>
                                                        </Col>
                                                    </Row>
                                                </CardBody>
                                            </Card>
                                        </Form>
                                    </div>
                                </ModalBody>
                                <ModalFooter className="card-footer">
                                    <div className="button-right">
                                        <Button className="button_focus" color="primary" type="button">
                                            <span className="fa fa-save"> </span> Lưu
                            </Button>
                                    </div>
                                </ModalFooter>
                            </Modal>

                        </CardBody>
                    </Card>
                </div>
                {/*popup thanh toan*/}
                <Modal isOpen={this.state.primary} toggle={this.togglePrimary}
                    className={'modal-primary ' + this.props.className}>
                    <ModalHeader toggle={this.togglePrimary}>Chi tiết hóa đơn</ModalHeader>
                    <ModalBody id="popup_detail">
                        <div className="form_popup">
                            <Form className="form-exchange-create" noValidate>
                                <Card>
                                    <CardBody>
                                        <Row>
                                            <Col sm="6" xs="12">
                                                <FormGroup>
                                                    <Label>Mã loại tài sản</Label>
                                                    <Input type="text" placeholder="TS01" required readOnly />
                                                    <div className="invalid-feedback">
                                                        Vui lòng nhập tên tài sản.
                                                    </div>
                                                </FormGroup>
                                            </Col>
                                            <Col sm="6" xs="12">
                                                <FormGroup>
                                                    <Label>Tên loại tài sản</Label>
                                                    <Input type="text" placeholder="Điện thoại" required />
                                                    <div className="invalid-feedback">
                                                        Vui lòng nhập tên tài sản.
                                                    </div>
                                                </FormGroup>
                                            </Col>
                                        </Row>
                                    </CardBody>
                                    <CardFooter>
                                        <div className="button-right">
                                            <Button className="button_focus" color="primary" type="submit">
                                                <span className="fa fa-save"> </span> Lưu
                                            </Button>
                                        </div>
                                    </CardFooter>
                                </Card>
                            </Form>
                        </div>
                    </ModalBody>
                </Modal>
                {/*end popup thanh toan*/}
            </div>
        )
    }
}
export default AssetManagement;
