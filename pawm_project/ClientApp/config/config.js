﻿import * as helper from '../src/modules/Helper'

module.exports = {
    urlPath: {
        'dashboard': {
            'role': 'admin',
            child: [
                {
                    'name': 'dashboard',
                    'url': '/thong-ke',
                    'role': 'admin'
                }
            ]
        },
        'invoice': {
            'role': 'user',
            child: [
                {
                    'name': 'list-invoice',
                    'url': '/danh-sach-hoa-don',
                    'role': 'customer'
                }
            ]
        },
        'warehouse': {
            'role': 'user',
            child: [
                {
                    'name': 'warehouse-management',
                    'url': '/quan-ly-kho',
                    'role': 'user'
                }
            ]
        },
        'liquidate': {
            'role': 'user',
            child: [
                {
                    'name': 'create-liquidate',
                    'url': '/giao-dich-het-han/them-phieu-thanh-ly',
                    'role': 'user'
                },
                {
                    'name': 'exprire-liquidate',
                    'url': '/giao-dich-het-han',
                    'role': 'user'
                }
            ]
        },
        'store': {
            'role': 'admin',
            child: [
                {
                    'name': 'detail-store',
                    'url': '/danh-sach-cua-hang/thong-tin-cua-hang',
                    'role': 'admin'
                },
                {
                    'name': 'list-store',
                    'url': '/danh-sach-cua-hang',
                    'role': 'admin'
                },
                {
                    'name': 'create-store',
                    'url': '/them-cua-hang',
                    'role': 'admin'
                }
            ]
        },
        'customer': {
            'role': 'user',
            child: [
                {
                    'name': 'detail-customer',
                    'url': '/danh-sach-khach-hang/thong-tin-khach-hang',
                    'role': 'user'
                },
                {
                    'name': 'list-customer',
                    'url': '/danh-sach-khach-hang',
                    'role': 'user'
                },
                {
                    'name': 'create-customer',
                    'url': '/them-khach-hang',
                    'role': 'user'
                },
                //{
                //    'name': 'create-exchange',
                //    'url': '/danh-sach-khach-hang/them-giao-dich',
                //    'role': 'user'
                //},
                //{
                //    'name': 'detail-exchange',
                //    'url': '/danh-sach-khach-hang/thong-tin-giao-dich',
                //    'role': 'user'
                //}
            ]
        },
        'asset-type': {
            'role': 'admin',
            child: [
                {
                    'name': 'detail-asset-type',
                    'url': '/danh-sach-loai-tai-san/thong-tin-loai-tai-san',
                    'role': 'admin'
                },
                {
                    'name': 'list-asset-type',
                    'url': '/danh-sach-loai-tai-san',
                    'role': 'admin'
                },
                {
                    'name': 'create-asset-type',
                    'url': '/them-loai-tai-san',
                    'role': 'admin'
                }
            ]
        },
        'staff': {
            'role': 'admin',
            child: [
                {
                    'name': 'detail-staff',
                    'url': '/danh-sach-nhan-vien/thong-tin-nhan-vien',
                    'role': 'admin'
                },
                {
                    'name': 'list-staff',
                    'url': '/danh-sach-nhan-vien',
                    'role': 'admin'
                },
                {
                    'name': 'create-staff',
                    'url': '/them-nhan-vien',
                    'role': 'admin'
                }
            ]
        },
        'public': {
            'role': 'any',
            child: [
                { 'name': 'profile-settings', 'url': '/thong-tin-ca-nhan' }, // luôn luôn để vị trí thứ 0 => default
                { 'name': 'login', 'url': '/dang-nhap' },
               
            ]
        },
        'purchaser': {
            'role': 'purchase',
            child: [
                {
                    'name': 'view-purchaser',
                    'url': '/thong-tin-tho',
                    'role': 'purchase'
                },
                {
                    'name': 'detail-purchaser',
                    'url': '/danh-sach-tho/thong-tin-tho',
                    'role': 'purchase'
                },
                {
                    'name': 'list-purchaser',
                    'url': '/danh-sach-tho',
                    'role': 'purchase'
                },
                {
                    'name': 'create-purchaser',
                    'url': '/them-tho',
                    'role': 'purchase'
                },
                {
                    'name': 'list-purchaser-new-asset',
                    'url': '/danh-sach-tai-san-tho-them-moi',
                    'role': 'purchase'
                },
                {
                    'name': 'list-purchaser-update-asset',
                    'url': '/danh-sach-tai-san-tho-cap-nhat',
                    'role': 'purchase'
                }
            ]
        }
    }
}