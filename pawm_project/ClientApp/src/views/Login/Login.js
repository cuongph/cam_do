﻿import React from 'react';
import { connect } from 'react-redux';
const Login = props => (
    <div className="body_login">
        <img src="img/left_bg.jpg" alt="Images" />
        <div className="container">
            
            <div className="body_login__right">
                <div className="col-md-12 body_login__right__form">
                    <div className="tab-content" id="myTabContent">
                        <img src="img/logo_head2.jpg" alt="Images" />
                        <h1>ĐĂNG NHẬP</h1>
                        <div className="">
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label htmlFor="exampleInputEmail" className="text-uppercase">Tên đăng nhập</label>
                                    <input type="text" className="form-control" placeholder="Tên Đăng Nhập *" defaultValue="" />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="exampleInputPassword1" className="text-uppercase">Mật khẩu</label>
                                    <input type="password" className="form-control" placeholder="Mật khẩu *" defaultValue="" />
                                </div>
                                <div className="form-check">
                                    <label className="form-check-label">
                                        <input type="checkbox" className="form-check-input" />
                                        <small>Nhớ đăng nhập</small>
                                    </label>
                                </div>
                                <div className="button_login"><button type="submit" className="btnLogin" > Đăng nhập</button></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
);
export default connect()(Login);