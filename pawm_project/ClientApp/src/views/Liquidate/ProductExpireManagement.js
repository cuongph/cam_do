﻿import React, { Component } from 'react';
import {
    Row,
    Col,
    Card,
    CardHeader,
    CardBody,
    CardFooter,
    Input,
    Table,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Button,
    Form,
    FormGroup,
    Label
} from 'reactstrap';
import { Link } from 'react-router-dom'
import { TextMask, InputAdapter } from 'react-text-mask-hoc';

class CustomerManagement extends Component {
    constructor(props) {
        super(props);
        this.state = {
            primary: false,
            activeTab: '1'
        };
        this.togglePrimary = this.togglePrimary.bind(this);
    }
    togglePrimary() {
        this.setState({
            primary: !this.state.primary
        });
    }

    render() {
        return (
            <div className="animated fadeIn">
                <div className="customer-page">
                    <Card>
                        <CardHeader>
                            <h1>Thanh lý</h1>
                        </CardHeader>
                        <CardBody>
                            <Row>
                                <Col sm="3" xs="12">
                                    <label>Cửa hàng</label>
                                    <Input type="select" className="mb-3">
                                        <option value="1">Cửa hàng 1</option>
                                        <option value="2">Cửa hàng 2</option>
                                    </Input>
                                </Col>
                                <Col sm="3" xs="12">
                                    <label>Loại tài sản</label>
                                    <Input type="select" className="mb-3">
                                        <option value="1">Điện thoại</option>
                                        <option value="2">Ô tô</option>
                                        <option value="3">Máy tính</option>
                                        <option value="4">Nhà</option>
                                    </Input>
                                </Col>
                                <Col sm="3" xs="12">
                                    <label>Tên tài sản</label>
                                    <Input type="text" placeholder="" className="mb-3" />
                                </Col>
                                <Col sm="3" xs="12">
                                    <FormGroup>
                                        <Label>Ngày hết hạn</Label>
                                        <TextMask
                                            mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
                                            Component={InputAdapter}
                                            className="form-control"
                                            placeholder=""
                                            type="text"
                                        />
                                    </FormGroup>
                                </Col>
                            </Row>
                        </CardBody>
                        <CardFooter>
                            <div className="button-right">
                                <Button color="primary" className="button_focus">
                                    <span className="icon-magnifier"> </span>Tìm kiếm
                                </Button>
                            </div>
                        </CardFooter>
                    </Card>
                    <Card>
                        
                        <CardBody>
                            <Button color="primary export_excel">
                                <span className="icon-cloud-download"> </span>Xuất excel
                            </Button>
                            <Table responsive className="table-striped">
                                <thead>
                                    <tr>
                                        <th id="th-first"></th>
                                        <th>Cửa hàng</th>
                                        <th>Loại TS</th>
                                        <th>Tên TS</th>
                                        <th>Tiền thanh lý</th>
                                        <th>Tên thợ</th>
                                        <th>Điện thoại</th>
                                        <th style={{ textAlign: "center" }}>Trạng thái</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <Button className="btn-primary btn-sm" onClick={this.togglePrimary}>
                                                <span title="Lập phiếu thanh lý" className="fa fa-shopping-cart"> </span>
                                            </Button>
                                        </td>
                                        <td>Cửa hàng 1</td>
                                        <td>Điện thoại</td>
                                        <td>Iphone 7</td>
                                        <td>15 000 000</td>
                                        <td>Nguyễn Quốc Hùng</td>
                                        <td>097 599 588</td>
                                        <td className="stt_icon" style={{ textAlign: "center" }}>
                                            <i className="icon-check icons"></i>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <Button className="btn-primary btn-sm" onClick={this.togglePrimary}>
                                                <span title="Lập phiếu thanh lý" className="fa fa-shopping-cart"> </span>
                                            </Button>
                                        </td>
                                        <td>Cửa hàng 1</td>
                                        <td>Điện thoại</td>
                                        <td>Iphone 7</td>
                                        <td>15 000 000</td>
                                        <td>Nguyễn Quốc Hùng</td>
                                        <td>097 599 588</td>
                                        <td className="stt_icon" style={{ textAlign: "center" }}>
                                            <i className="icon-close icons"></i>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <Button className="btn-primary btn-sm" onClick={this.togglePrimary}>
                                                <span title="Lập phiếu thanh lý" className="fa fa-shopping-cart"> </span>
                                            </Button>
                                        </td>
                                        <td>Cửa hàng 1</td>
                                        <td>Điện thoại</td>
                                        <td>Iphone 7</td>
                                        <td>15 000 000</td>
                                        <td>Nguyễn Quốc Hùng</td>
                                        <td>097 599 588</td>
                                        <td className="stt_icon" style={{ textAlign: "center" }}>
                                            <i className="icon-check icons"></i>
                                        </td>
                                    </tr>
                                </tbody>
                            </Table>
                        </CardBody>
                    </Card>
                </div>

                <Modal isOpen={this.state.primary} toggle={this.togglePrimary}
                    className={'modal-primary modal-lg ' + this.props.className}>
                    <ModalHeader toggle={this.togglePrimary}>Phiếu thanh lý</ModalHeader>
                    <ModalBody id="popup_detail">
                        <div className="form_popup tab_customer ">
                            <Form className="form-exchange-create" noValidate>
                                <Card>
                                    <CardBody>
                                        <Row>
                                            <Col sm="4" xs="12">
                                                <FormGroup>
                                                    <Label>Ngày</Label>
                                                    <TextMask
                                                        mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
                                                        Component={InputAdapter}
                                                        className="form-control"
                                                        placeholder=""
                                                        type="text"
                                                        required
                                                    />
                                                    <div className="invalid-feedback">
                                                        Vui lòng nhập ngày.
                        </div>
                                                </FormGroup>
                                            </Col>
                                            <Col sm="4" xs="12">
                                                <FormGroup>
                                                    <Label>Tên tài sản</Label>
                                                    <Input type="text" placeholder="Iphone 7plus" required readOnly />
                                                    <div className="invalid-feedback">
                                                        Vui lòng nhập tên tài sản.
                        </div>
                                                </FormGroup>
                                            </Col>
                                            <Col sm="4" xs="12">
                                                <FormGroup>
                                                    <Label>Giá</Label>
                                                    <Input type="text" placeholder="" required />
                                                    <div className="invalid-feedback">
                                                        Vui lòng nhập giá.
                        </div>
                                                </FormGroup>
                                            </Col>
                                            <Col sm="4" xs="12">
                                                <FormGroup>
                                                    <Label>Thợ</Label>
                                                    <Input type="select" required>
                                                        <option value="1">Nguyễn Hồng Sơn - 0975 299 244</option>
                                                        <option value="2">Nguyễn Hồng Sơn - 0903 255 002</option>
                                                        <option value="1">Nguyễn Hồng Sơn - 0975 299 244</option>
                                                        <option value="2">Nguyễn Hồng Sơn - 0903 255 002</option>
                                                    </Input>
                                                    <div className="invalid-feedback">
                                                        Vui lòng chọn cửa hàng.
                                    </div>
                                                </FormGroup>
                                            </Col>
                                            <Col sm="8" xs="12">
                                                <div className="img-box">
                                                    <label htmlFor="file-upload" className="custom-file-upload">
                                                        <i className="icon-cloud-upload" style={{ marginRight: '2px' }}></i> Chọn ảnh
                        </label>
                                                    <input id="file-upload" name='upload_cont_img' type="file" style={{ display: "none" }} multiple />
                                                    <img src="/img/image-default.png" alt="Images" />
                                                </div>
                                            </Col>
                                            <Col xs="12" className="mb-3">
                                                <FormGroup >
                                                    <Label>Ghi chú</Label>
                                                    <Input type="textarea" name="textarea-input" id="textarea-input" rows="2"
                                                        placeholder="Ghi chú" />
                                                </FormGroup>
                                            </Col>
                                        </Row>
                                    </CardBody>
                                    <CardFooter>
                                        <div className="button-right">
                                            <Button color="primary" type="button">
                                                <span className="fa fa-print"> </span> In phiếu
                                            </Button>
                                            <Button className="button_focus" color="primary" type="submit">
                                                <span className="fa fa-save"> </span> Lưu
                                            </Button>
                                        </div>
                                    </CardFooter>
                                </Card>
                            </Form>
                        </div>
                    </ModalBody>
                </Modal>
                {/*end popup chi tiet*/}
            </div>
        )
    }
}
export default CustomerManagement;
