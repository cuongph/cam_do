﻿import React, { Component } from 'react';
import { Link, Switch, Route, Redirect } from 'react-router-dom';
import * as Auth from '../modules/Auth';
import * as helper from '../modules/Helper'

var Authorization = (props) => {
    if (Auth.rolesMatched(props.role)) {
        return (<Route key={props.name} name={props.name} path={props.url} component={props.component} />);
    }
    //else {
    //    return (<Redirect key={props.name} name={props.name} from='/' to={props.redirect ? props.redirect : redirectTo[localStorage.getItem('role')]} />)
    //}
};
export default Authorization