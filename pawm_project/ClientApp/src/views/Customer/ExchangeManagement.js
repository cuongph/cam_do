﻿import React, { Component } from 'react';
import {
    Card,
    CardHeader,
    CardBody,
    Table,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Button,
    Form,
    Row,
    Col,
    CardFooter,
    FormGroup,
    Label,
    Input,
    TabContent,
    TabPane,
    Nav,
    NavItem,
    NavLink
} from 'reactstrap';
import { Link } from 'react-router-dom'
import { TextMask, InputAdapter } from 'react-text-mask-hoc';
import classnames from 'classnames';
//React select
import Select from 'react-select';
import '../../../public/asset/scss/vendors/react-select/react-select.scss';
const options = [
    { value: 'cmnd', label: 'Chứng minh nhân dân' },
    { value: 'cavet', label: 'Cavet xe' },
    { value: 'bh', label: 'Bảo hiểm xe' }
];

class ExchangeManagement extends Component {
    constructor(props) {
        super(props);
        this.state = {
            primary: false,
            primary1: false,
            activeTab: '1',
            value: [],
        };
        this.togglePrimary = this.togglePrimary.bind(this);
        this.togglePrimary1 = this.togglePrimary1.bind(this);
        this.toggle = this.toggle.bind(this);
        this.saveChanges = this.saveChanges.bind(this);
    }
    togglePrimary() {
        this.setState({
            primary: !this.state.primary
        });
    }
    togglePrimary1() {
        this.setState({
            primary1: !this.state.primary1
        });
    }
    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }
    saveChanges(value) {
        this.setState({ value });
    }

    componentDidMount() {
        $(document).on('change', '.img-box #file-upload', function () {
            //var i = $(this).prev('label').clone();
            //var fileName = $('#file-upload')[0].files[0].name;
            //$(this).prev('label').text(fileName);
            var files = $('.img-box #file-upload')[0].files;
            var html = ''
            for (var i = 0; i < files.length; i++) {
                //console.log("??", files[i].name)
                html += ` <img src="` + URL.createObjectURL(files[i]) + `" alt="Images" />`
            }
            $('#img_gd_scroll').empty()
            $('#img_gd_scroll').append(html)
            console.log("??", html)
            //$(".img-box img").attr('src', URL.createObjectURL(files));
        });
    }
    render() {
        return (
            <div>
                <div className="exchange-page">
                    <div id="ListGD">
                        <div className="button-right add_gd">
                            <Button className="btn btn-primary mb-3" onClick={this.togglePrimary}>
                                Thêm giao dịch
                            </Button>
                        </div>
                        <div className="inforname_customer">
                            <strong>CH10001</strong>
                            <span>Nguyễn Văn A</span>
                        </div>
                        <Card >
                            <CardBody className="padding_0" style={{ paddingTop: "0 !important" }}>
                                <Table responsive className="table-striped">
                                    <thead>
                                        <tr>
                                            <th id="th-first" style={{ width: "60px" }}></th>
                                            <th>Ngày cầm</th>
                                            <th>Loại TS</th>
                                            <th>Tên TS</th>
                                            <th>Số tiền cầm</th>
                                            <th>Lãi</th>
                                            <th>Số tiền TT</th>
                                            <th>Ngày hết hạn</th>
                                            <th>Ngày thanh lý</th>
                                            <th>Nhân viên</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <Button className="btn-primary btn-sm" onClick={this.togglePrimary1}>
                                                    <span title="Chi tiết" className="fa fa-pencil"> </span>
                                                </Button>
                                            </td>
                                            <td>20-01-2018</td>
                                            <td>Điện thoại</td>
                                            <td>Iphone 7</td>
                                            <td>12 000 000</td>
                                            <td>1%</td>
                                            <td>12,120,000</td>
                                            <td>20-02-2018</td>
                                            <td>10-03-2018</td>
                                            <td>Nguyễn Thái Bình</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <Button className="btn-primary btn-sm" onClick={this.togglePrimary1}>
                                                    <span title="Chi tiết" className="fa fa-pencil"> </span>
                                                </Button>
                                            </td>
                                            <td>20-01-2018</td>
                                            <td>Điện thoại</td>
                                            <td>Iphone 7</td>
                                            <td>12 000 000</td>
                                            <td>1%</td>
                                            <td>12,120,000</td>
                                            <td>20-02-2018</td>
                                            <td>10-03-2018</td>
                                            <td>Nguyễn Thái Bình</td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </CardBody>
                        </Card>
                    </div>
                </div>


                <Modal isOpen={this.state.primary} toggle={this.togglePrimary}
                    className={'modal-primary modal-lg ' + this.props.className}>
                    <ModalHeader toggle={this.togglePrimary}>Thêm giao dịch</ModalHeader>
                    <ModalBody id="popup_detail">
                        <div className="form_popup">
                            <Form className="form-exchange-create" noValidate>
                                <Card>
                                    <CardBody>
                                        <Row>
                                            <Col sm="4" xs="12" className="mb-3">
                                                <FormGroup>
                                                    <Label>Loại tài sản</Label>
                                                    <Input type="select" required className="mb-2">
                                                        <option value="1">Xe hơi</option>
                                                        <option value="2">Xe máy</option>
                                                        <option value="3">Điện thoại</option>
                                                        <option value="4">Máy tính</option>
                                                    </Input>
                                                </FormGroup>
                                            </Col>
                                            <Col sm="4" xs="12">
                                                <FormGroup>
                                                    <Label>Tên tài sản</Label>
                                                    <Input type="text" placeholder="Tên tài sản" required />
                                                    <div className="invalid-feedback">
                                                        Vui lòng nhập tên tài sản.
                        </div>
                                                </FormGroup>
                                            </Col>
                                            
                                            <Col sm="4" xs="12">
                                                <FormGroup>
                                                    <Label>Từ ngày</Label>
                                                    <TextMask
                                                        mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
                                                        Component={InputAdapter}
                                                        className="form-control"
                                                        placeholder="Từ ngày"
                                                        type="text"
                                                        required
                                                    />
                                                    <div className="invalid-feedback">
                                                        Vui lòng nhập ngày.
                        </div>
                                                </FormGroup>
                                            </Col>
                                            <Col sm="4" xs="12">
                                                <FormGroup>
                                                    <Label>Đến ngày</Label>
                                                    <TextMask
                                                        mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
                                                        Component={InputAdapter}
                                                        className="form-control"
                                                        placeholder="Đến ngày"
                                                        type="text"
                                                    />
                                                </FormGroup>
                                            </Col>
                                            <Col sm="4" xs="12" className="mb-3">
                                                <FormGroup>
                                                    <Label>Chọn thợ</Label>
                                                    <Input type="select" required className="mb-2">
                                                        <option value="1">Nguyễn Hùng Long - 0972 025 205</option>
                                                        <option value="1">Nguyễn Hùng Long - 0972 025 205</option>
                                                        <option value="1">Nguyễn Hùng Long - 0972 025 205</option>
                                                        <option value="1">Nguyễn Hùng Long - 0972 025 205</option>
                                                    </Input>
                                                </FormGroup>
                                            </Col>
                                            <Col sm="4" xs="12">
                                                <Col sm="6" xs="12" style={{ paddingLeft: "0" }}>
                                                    <FormGroup>
                                                        <Label>Số tiền thợ đề xuất</Label>
                                                        <Input type="text" placeholder="Số tiền thợ đề xuất" required id="HoldingAmount" />
                                                        <div className="invalid-feedback">
                                                            Vui lòng nhập số tiền thợ đề xuất.
                        </div>
                                                    </FormGroup>
                                                </Col>
                                                <Col sm="6" xs="12" style={{ paddingRight: "0" }}>
                                                    <FormGroup>
                                                        <Label>Số tiền cầm</Label>
                                                        <Input type="text" placeholder="Số tiền cầm" required id="HoldingAmount" />
                                                        <div className="invalid-feedback">
                                                            Vui lòng nhập số tiền cầm.
                        </div>
                                                    </FormGroup>
                                                </Col>
                                            </Col>
                                            <Col sm="4" xs="8">
                                                <Col sm="9" xs="8" className="padding_0">
                                                    <FormGroup>
                                                        <Label>Lãi</Label>
                                                        <Input type="select" required>
                                                            <option value="1">Theo ngày</option>
                                                            <option value="2">Theo tháng</option>
                                                        </Input>
                                                        <div className="invalid-feedback">
                                                            Vui lòng chọn lãi suất.
                        </div>
                                                    </FormGroup>
                                                </Col>
                                                <Col sm="3" xs="4">
                                                    <FormGroup>
                                                        <Label style={{ marginTop: "17px" }}></Label>
                                                        <Input type="text" placeholder="%" required id="InterestRate" />
                                                        <div className="invalid-feedback">
                                                            Không được bỏ trống.
                        </div>
                                                    </FormGroup>
                                                </Col>
                                            </Col>
                                            <Col sm="4" xs="12">
                                                <FormGroup>
                                                    <Label>Số tiền thanh toán</Label>
                                                    <Input type="text" placeholder="Phát sinh tự động" readOnly required />
                                                    <div className="invalid-feedback">
                                                        Vui lòng nhập số tiền thanh toán.
                        </div>
                                                </FormGroup>
                                            </Col>
                                            <Col sm="4" xs="12">
                                                <FormGroup>
                                                    <Label>Số tiền thanh lý</Label>
                                                    <Input type="text" placeholder="Số tiền thanh lý" required />
                                                    <div className="invalid-feedback">
                                                        Vui lòng nhập số tiền thanh lý.
                                    </div>
                                                </FormGroup>
                                            </Col>
                                            
                                            <Col sm="6" xs="12" className="mb-3">
                                                <FormGroup>
                                                    <Label>Giữ tài sản kèm theo</Label>
                                                    <Select
                                                        name="form-field-name2"
                                                        value={this.state.value}
                                                        options={options}
                                                        onChange={this.saveChanges}
                                                        multi
                                                    />
                                                </FormGroup>
                                            </Col>
                                            <Col sm="6" xs="12">
                                                <div className="img-box">
                                                    <label htmlFor="file-upload" className="custom-file-upload">
                                                        <i className="icon-cloud-upload" style={{ marginRight: '2px' }}></i> Chọn ảnh
                                                    </label>
                                                    <input id="file-upload" name='upload_cont_img' type="file" style={{ display: "none" }} multiple />
                                                    <div id="img_gd">
                                                        <div id="img_gd_scroll">
                                                            <img src="/img/image-default.png" alt="Images" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </Col>
                                            <Col xs="12" className="mb-3">
                                                <FormGroup >
                                                    <Label>Ghi chú</Label>
                                                    <Input type="textarea" name="textarea-input" id="textarea-input" rows="2"
                                                        placeholder="Ghi chú" />
                                                </FormGroup>
                                            </Col>
                                        </Row>
                                    </CardBody>
                                    <CardFooter>
                                        <div className="button-right">
                                            <Button color="primary" type="button">
                                                <span className="fa fa-print"> </span> In phiếu
                                            </Button>
                                            <Button className="button_focus" color="primary" type="submit">
                                                <span className="fa fa-save"> </span> Lưu
                                            </Button>
                                        </div>
                                    </CardFooter>
                                </Card>
                            </Form>
                        </div>
                    </ModalBody>
                </Modal>
                {/*end popup chi tiet*/}



                {/*popup thanh toan*/}
                <Modal isOpen={this.state.primary1} toggle={this.togglePrimary1}
                    className={'modal-primary modal-lg ' + this.props.className}>
                    <ModalBody id="popup_detail">
                        <ModalHeader className="button_close" toggle={this.togglePrimary1}></ModalHeader>
                        <div className="form_popup tab_customer">
                            <Nav tabs>
                                <NavItem>
                                    <NavLink
                                        className={classnames({ active: this.state.activeTab === '1' })}
                                        onClick={() => { this.toggle('1'); }}
                                    >
                                        Chi tiết giao dịch
                            </NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink
                                        className={classnames({ active: this.state.activeTab === '2' })}
                                        onClick={() => { this.toggle('2'); }}
                                    >
                                        Quá trình thanh toán
                            </NavLink>
                                </NavItem>
                            </Nav>
                            <TabContent activeTab={this.state.activeTab}>
                                <TabPane tabId="1">
                                            <Card>
                                                <CardBody>
                                                    <Row>
                                                        <Col sm="4" xs="12" className="mb-3">
                                                            <FormGroup>
                                                                <Label>Loại tài sản</Label>
                                                                <Input type="select" required className="mb-2">
                                                                    <option value="1">Xe hơi</option>
                                                                    <option value="2">Xe máy</option>
                                                                    <option value="3">Điện thoại</option>
                                                                    <option value="4">Máy tính</option>
                                                                </Input>
                                                            </FormGroup>
                                                        </Col>
                                                        <Col sm="4" xs="12">
                                                            <FormGroup>
                                                                <Label>Tên tài sản</Label>
                                                                <Input type="text" placeholder="Tên tài sản" required />
                                                                <div className="invalid-feedback">
                                                                    Vui lòng nhập tên tài sản.
                        </div>
                                                            </FormGroup>
                                                        </Col>

                                                        <Col sm="4" xs="12">
                                                            <FormGroup>
                                                                <Label>Từ ngày</Label>
                                                                <TextMask
                                                                    mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
                                                                    Component={InputAdapter}
                                                                    className="form-control"
                                                                    placeholder="Từ ngày"
                                                                    type="text"
                                                                    required
                                                                />
                                                                <div className="invalid-feedback">
                                                                    Vui lòng nhập ngày.
                        </div>
                                                            </FormGroup>
                                                        </Col>
                                                        <Col sm="4" xs="12">
                                                            <FormGroup>
                                                                <Label>Đến ngày</Label>
                                                                <TextMask
                                                                    mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
                                                                    Component={InputAdapter}
                                                                    className="form-control"
                                                                    placeholder="Đến ngày"
                                                                    type="text"
                                                                />
                                                            </FormGroup>
                                                        </Col>
                                                        <Col sm="4" xs="12" className="mb-3">
                                                            <FormGroup>
                                                                <Label>Chọn thợ</Label>
                                                                <Input type="select" required className="mb-2">
                                                                    <option value="1">Nguyễn Hùng Long - 0972 025 205</option>
                                                                    <option value="1">Nguyễn Hùng Long - 0972 025 205</option>
                                                                    <option value="1">Nguyễn Hùng Long - 0972 025 205</option>
                                                                    <option value="1">Nguyễn Hùng Long - 0972 025 205</option>
                                                                </Input>
                                                            </FormGroup>
                                                        </Col>
                                                        <Col sm="4" xs="12">
                                                            <Col sm="6" xs="12" style={{ paddingLeft: "0" }}>
                                                                <FormGroup>
                                                                    <Label>Số tiền thợ đề xuất</Label>
                                                                    <Input type="text" placeholder="Số tiền thợ đề xuất" required id="HoldingAmount" />
                                                                    <div className="invalid-feedback">
                                                                        Vui lòng nhập số tiền thợ đề xuất.
                        </div>
                                                                </FormGroup>
                                                            </Col>
                                                            <Col sm="6" xs="12" style={{ paddingRight: "0" }}>
                                                                <FormGroup>
                                                                    <Label>Số tiền cầm</Label>
                                                                    <Input type="text" placeholder="Số tiền cầm" required id="HoldingAmount" />
                                                                    <div className="invalid-feedback">
                                                                        Vui lòng nhập số tiền cầm.
                        </div>
                                                                </FormGroup>
                                                            </Col>
                                                        </Col>
                                                        <Col sm="4" xs="8">
                                                            <Col sm="9" xs="8" className="padding_0">
                                                                <FormGroup>
                                                                    <Label>Lãi</Label>
                                                                    <Input type="select" required>
                                                                        <option value="1">Theo ngày</option>
                                                                        <option value="2">Theo tháng</option>
                                                                    </Input>
                                                                    <div className="invalid-feedback">
                                                                        Vui lòng chọn lãi suất.
                        </div>
                                                                </FormGroup>
                                                            </Col>
                                                            <Col sm="3" xs="4">
                                                                <FormGroup>
                                                                    <Label style={{ marginTop: "17px" }}></Label>
                                                                    <Input type="text" placeholder="%" required id="InterestRate" />
                                                                    <div className="invalid-feedback">
                                                                        Không được bỏ trống.
                        </div>
                                                                </FormGroup>
                                                            </Col>
                                                        </Col>
                                                        <Col sm="4" xs="12">
                                                            <FormGroup>
                                                                <Label>Số tiền thanh toán</Label>
                                                                <Input type="text" placeholder="Phát sinh tự động" readOnly required />
                                                                <div className="invalid-feedback">
                                                                    Vui lòng nhập số tiền thanh toán.
                        </div>
                                                            </FormGroup>
                                                        </Col>
                                                        <Col sm="4" xs="12">
                                                            <FormGroup>
                                                                <Label>Số tiền thanh lý</Label>
                                                                <Input type="text" placeholder="Số tiền thanh lý" required />
                                                                <div className="invalid-feedback">
                                                                    Vui lòng nhập số tiền thanh lý.
                                    </div>
                                                            </FormGroup>
                                                        </Col>

                                                        <Col sm="6" xs="12" className="mb-3">
                                                            <FormGroup>
                                                                <Label>Giữ tài sản kèm theo</Label>
                                                                <Select
                                                                    name="form-field-name2"
                                                                    value={this.state.value}
                                                                    options={options}
                                                                    onChange={this.saveChanges}
                                                                    multi
                                                                />
                                                            </FormGroup>
                                                        </Col>
                                                        <Col sm="6" xs="12">
                                                            <div className="img-box">
                                                                <label htmlFor="file-upload" className="custom-file-upload">
                                                                    <i className="icon-cloud-upload" style={{ marginRight: '2px' }}></i> Chọn ảnh
                                                    </label>
                                                                <input id="file-upload" name='upload_cont_img' type="file" style={{ display: "none" }} multiple />
                                                                <div id="img_gd">
                                                                    <div id="img_gd_scroll">
                                                                        <img src="/img/image-default.png" alt="Images" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </Col>
                                                        <Col xs="12" className="mb-3">
                                                            <FormGroup >
                                                                <Label>Ghi chú</Label>
                                                                <Input type="textarea" name="textarea-input" id="textarea-input" rows="2"
                                                                    placeholder="Ghi chú" />
                                                            </FormGroup>
                                                        </Col>
                                                    </Row>
                                                </CardBody>
                                                <CardFooter>
                                                    <div className="button-right">
                                                        <Button color="primary" type="button">
                                                            <span className="fa fa-print"> </span> In phiếu
                                            </Button>
                                                        <Button className="button_focus" color="primary" type="submit">
                                                            <span className="fa fa-save"> </span> Lưu
                                            </Button>
                                                    </div>
                                                </CardFooter>
                                            </Card>
                                </TabPane>
                                <TabPane tabId="2">
                                    <Card >
                                        <CardBody>
                                            <Row>
                                                <Col sm="4" xs="12" className="mb-3">
                                                    <FormGroup>
                                                        <Label>Hình thức</Label>
                                                        <Input type="select" required className="mb-2">
                                                            <option value="1">Gia hạn</option>
                                                            <option value="2">Đóng lãi</option>
                                                            <option value="3">Trả bớt gốc</option>
                                                            <option value="4">Chuộc đồ</option>
                                                        </Input>
                                                    </FormGroup>
                                                </Col>
                                                <Col sm="4" xs="12">
                                                    <FormGroup>
                                                        <Label>Từ ngày</Label>
                                                        <TextMask
                                                            mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
                                                            Component={InputAdapter}
                                                            className="form-control"
                                                            placeholder="Từ ngày"
                                                            type="text"
                                                            required
                                                        />
                                                        <div className="invalid-feedback">
                                                            Vui lòng nhập ngày.
                                                            </div>
                                                    </FormGroup>
                                                </Col>
                                                <Col sm="4" xs="12">
                                                    <FormGroup>
                                                        <Label>Đến ngày</Label>
                                                        <TextMask
                                                            mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
                                                            Component={InputAdapter}
                                                            className="form-control"
                                                            placeholder="Đến ngày"
                                                            type="text"
                                                            required
                                                        />
                                                        <div className="invalid-feedback">
                                                            Vui lòng nhập ngày.
                                                            </div>
                                                    </FormGroup>
                                                </Col>
                                                <Col sm="4" style={{ padding: 0 }}>
                                                    <Col sm="8" xs="8">
                                                        <FormGroup>
                                                            <Label>Lãi</Label>
                                                            <Input type="select" required>
                                                                <option value="1">Theo ngày</option>
                                                                <option value="2">Theo tháng</option>
                                                            </Input>
                                                            <div className="invalid-feedback">
                                                                Vui lòng chọn lãi suất.
                                                            </div>
                                                        </FormGroup>
                                                    </Col>
                                                    <Col sm="4" xs="4">
                                                        <FormGroup>
                                                            <Label style={{ marginTop: "17px" }}></Label>
                                                            <Input type="text" placeholder="Lãi" required id="InterestRate" />
                                                        </FormGroup>
                                                    </Col>
                                                </Col>
                                                <Col sm="4" xs="12">
                                                    <FormGroup>
                                                        <Label>Tiền lãi phát sinh</Label>
                                                        <Input type="text" placeholder="Tiền lãi phát sinh" id="InterestArising" />
                                                    </FormGroup>
                                                </Col>
                                                <Col sm="4" xs="12">
                                                    <FormGroup>
                                                        <Label>Nhân viên</Label>
                                                        <Input type="text" placeholder="Nhân viên giao dịch" id="InterestArising" />
                                                    </FormGroup>
                                                </Col>
                                                <Col sm="12" xs="12" className="mb-3">
                                                    <FormGroup>
                                                        <Label>Ghi chú</Label>
                                                        <Input type="text" name="textarea-input" id="textarea-input" rows="2"
                                                            placeholder="Ghi chú" />
                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                            <CardFooter>
                                                <div className="button-right">
                                                    <Button color="primary" type="button">
                                                        <span className="fa fa-print"> </span> In phiếu
                                            </Button>
                                                    <Button className="button_focus" color="primary" type="submit">
                                                        <span className="fa fa-save"> </span> Lưu
                                            </Button>
                                                </div>
                                            </CardFooter>
                                            <h2>Lịch sử giao dịch</h2>
                                            <Table responsive className="table-striped">
                                                <thead>
                                                    <tr>
                                                        <th style={{ width: "50px" }}></th>
                                                        <th>Ngày</th>
                                                        <th>Hình thức</th>
                                                        <th>Lãi</th>
                                                        <th>Số tiền</th>
                                                        <th>Nhân viên</th>
                                                        <th>Ghi chú</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td style={{ width: "50px" }}>
                                                            <Button className="btn-primary btn-sm" >
                                                                <span title="Chi tiết" className="fa fa-pencil"> </span>
                                                            </Button>
                                                        </td>
                                                        <td>12/12/2018</td>
                                                        <td>Đóng lãi</td>
                                                        <td>1,5%</td>
                                                        <td>10,000,000</td>
                                                        <td>Trịnh Thị Kim Chi</td>
                                                        <td>Nhớ đóng lãi trước ngày...</td>
                                                    </tr>
                                                    <tr>
                                                        <td style={{ width: "50px" }}>
                                                            <Button className="btn-primary btn-sm" >
                                                                <span title="Chi tiết" className="fa fa-pencil"> </span>
                                                            </Button>
                                                        </td>
                                                        <td>12/12/2018</td>
                                                        <td>Đóng lãi</td>
                                                        <td>1,5%</td>
                                                        <td>10,000,000</td>
                                                        <td>Trịnh Thị Kim Chi</td>
                                                        <td>Nhớ đóng lãi trước ngày...</td>
                                                    </tr>
                                                    <tr>
                                                        <td style={{ width: "50px" }}>
                                                            <Button className="btn-primary btn-sm" >
                                                                <span title="Chi tiết" className="fa fa-pencil"> </span>
                                                            </Button>
                                                        </td>
                                                        <td>12/12/2018</td>
                                                        <td>Đóng lãi</td>
                                                        <td>1,5%</td>
                                                        <td>10,000,000</td>
                                                        <td>Trịnh Thị Kim Chi</td>
                                                        <td>Nhớ đóng lãi trước ngày...</td>
                                                    </tr>
                                                    <tr>
                                                        <td style={{ width: "50px" }}>
                                                            <Button className="btn-primary btn-sm" >
                                                                <span title="Chi tiết" className="fa fa-pencil"> </span>
                                                            </Button>
                                                        </td>
                                                        <td>12/12/2018</td>
                                                        <td>Đóng lãi</td>
                                                        <td>1,5%</td>
                                                        <td>10,000,000</td>
                                                        <td>Trịnh Thị Kim Chi</td>
                                                        <td>Nhớ đóng lãi trước ngày...</td>
                                                    </tr>
                                                    <tr>
                                                        <td style={{ width: "50px" }}>
                                                            <Button className="btn-primary btn-sm" >
                                                                <span title="Chi tiết" className="fa fa-pencil"> </span>
                                                            </Button>
                                                        </td>
                                                        <td>12/12/2018</td>
                                                        <td>Đóng lãi</td>
                                                        <td>1,5%</td>
                                                        <td>10,000,000</td>
                                                        <td>Trịnh Thị Kim Chi</td>
                                                        <td>Nhớ đóng lãi trước ngày...</td>
                                                    </tr>
                                                </tbody>
                                            </Table>
                                        </CardBody>
                                    </Card>
                                </TabPane>

                            </TabContent>
                        </div>
                    </ModalBody>
                </Modal>
                {/*end popup thanh toan*/}
            </div>
        )
    }
}
export default ExchangeManagement;
