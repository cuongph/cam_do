﻿import React, { Component } from 'react';
import {
    Row,
    Col,
    Card,
    CardHeader,
    CardBody,
    CardFooter,
    Button,
    FormGroup,
    Label,
    Input,
    Form
} from 'reactstrap';
import { Link } from 'react-router-dom'

class AssetDetail extends Component {
    componentDidMount() {
        (function () {
            var forms = document.getElementsByClassName('form-asset-detail');
            // Loop over them and prevent submission
            Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        })();
    }
    render() {
        return (
            <div className="asset-detail-page">
                <Form className="form-asset-detail" noValidate>
                    <Card>
                        <CardHeader>
                            <span>Thông tin loại tài sản</span>
                        </CardHeader>
                        <CardBody>
                            <Row>
                                <Col sm="6" xs="12">
                                    <FormGroup>
                                        <Label >Mã loại tài sản</Label>
                                        <Input type="text" placeholder="Mã loại tài sản" defaultValue="TS01" readOnly />
                                    </FormGroup>
                                </Col>
                                <Col sm="6" xs="12">
                                    <FormGroup>
                                        <Label >Tên loại tài sản</Label>
                                        <Input type="text" placeholder="Tên loại tài sản" defaultValue="Điện thoại" required />
                                        <div className="invalid-feedback">
                                            Vui lòng nhập tên tài sản.
                                        </div>
                                    </FormGroup>
                                </Col>
                                <Col xs="12">
                                    <FormGroup>
                                        <Label>Ghi chú</Label>
                                        <Input type="textarea" name="textarea-input" id="textarea-input" rows="2"
                                            placeholder="Ghi chú" />
                                    </FormGroup>
                                </Col>
                            </Row>
                        </CardBody>
                        <CardFooter>
                            <div className="button-right">
                                <Link to="/danh-sach-loai-tai-san" className="btn btn-primary" >
                                    <span className="icon-list"> </span>Danh sách
                            </Link>
                                <Button color="primary button_focus">
                                    <span className="fa fa-save"> </span> Lưu
                            </Button>
                            </div>
                        </CardFooter>
                    </Card>
                </Form>
            </div>
        )
    }
}
export default AssetDetail;
