﻿import React, { Component } from 'react';
import {
    Row,
    Col,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Card,
    CardHeader,
    CardBody,
    ButtonDropdown
} from 'reactstrap';
import $ from 'jquery';
import Highcharts from 'highcharts';
import { connect } from "react-redux";
import { actExample } from "../../actions";

class Dashboard extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.onRadioBtnClick = this.onRadioBtnClick.bind(this);

        this.state = {
            dropdownOpen: false,
            radioSelected: 2,

            seriesPieGroupStore: [{
                name: 'Gases',
                data: [
                    {
                        name: 'Cửa hàng 1',
                        y: 0.9,
                        color: '#3498db'
                    },
                    {
                        name: 'Cửa hàng 2',
                        y: 78.1,
                        color: '#9b59b6'
                    },
                    {
                        name: 'Cửa hàng 3',
                        y: 20.9,
                        color: '#2ecc71'
                    },
                    {
                        name: 'Cửa hàng 4',
                        y: 0.1,
                        color: '#f1c40f'
                    }
                ]
            }],
            seriesPieStore: [{
                name: 'Gases',
                data: [
                    {
                        name: 'Tổng thu',
                        y: 19,
                        color: '#3498db'
                    },
                    {
                        name: 'Tổng chi',
                        y: 60,
                        color: '#9b59b6'
                    },
                    {
                        name: 'Tổng lãi',
                        y: 21,
                        color: '#2ecc71'
                    }
                ]
            }]
        }
    }

    componentDidMount() {
        $(document).on("click", ".dropdown-menu .dropdown-item", function (e) {
            $('.dropdown-toggle').text($(this).text());
        });

        this.pieChartGroupStore();
        this.pieChartStore();
        this.columnChartIncomeGroupMonth();

        //this.props.getExample();
    }

    pieChartGroupStore() {
        Highcharts.chart({
            chart: {
                type: 'pie',
                renderTo: 'income-group-store'
            },
            title: {
                verticalAlign: 'middle',
                floating: true,
                text: '',
                style: {
                    fontSize: '10px'
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}: {point.percentage:.1f} %'
                    },
                    innerSize: '40%'
                }
            },
            series: this.state.seriesPieGroupStore
        });
    }

    pieChartStore() {
        Highcharts.chart({
            chart: {
                type: 'pie',
                renderTo: 'income-store'
            },
            title: {
                verticalAlign: 'middle',
                floating: true,
                text: '',
                style: {
                    fontSize: '10px'
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}: {point.percentage:.1f} %'
                    },
                    innerSize: '40%'
                }
            },
            series: this.state.seriesPieStore
        });
    }

    columnChartIncomeGroupMonth() {
        Highcharts.chart({
            chart: {
                type: 'column',
                renderTo: 'income-group-month'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: [
                    'Jan',
                    'Feb',
                    'Mar',
                    'Apr',
                    'May',
                    'Jun',
                    'Jul',
                    'Aug',
                    'Sep',
                    'Oct',
                    'Nov',
                    'Dec'
                ],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Doanh thu'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} vnđ</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Cửa hàng 1',
                data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]

            }, {
                name: 'Cửa hàng 2',
                data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3]

            }, {
                name: 'Cửa hàng 3',
                data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2]

            }, {
                name: 'Cửa hàng 4',
                data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1]

            }]
        });
    }

    toggle() {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen
        });
    }

    onRadioBtnClick(radioSelected) {
        this.setState({
            radioSelected: radioSelected
        });
    }

    render() {
        return (
            <div className="form-dashboard">
                <div className="animated fadeIn">
                    <div className="button-right">
                        <ButtonDropdown className="mb-2" isOpen={this.state.dropdownOpen} toggle={() => { this.toggle(0); }}>
                            <DropdownToggle caret color="primary">
                                Chọn Cửa Hàng
                                </DropdownToggle>
                            <DropdownMenu right>
                                <DropdownItem header>Cửa hàng</DropdownItem>
                                <DropdownItem>Tất cả cửa hàng</DropdownItem>
                                <DropdownItem>Cửa hàng 1</DropdownItem>
                                <DropdownItem>Cửa hàng 2</DropdownItem>
                                <DropdownItem>Cửa hàng 3</DropdownItem>
                                <DropdownItem>Cửa hàng 4</DropdownItem>
                            </DropdownMenu>
                        </ButtonDropdown>
                    </div>
                    <Row>
                        <Col xs="12" sm="6" lg="4">
                            <Card className="text-white bg-primary custom-card">
                                <CardBody className="pb-1">
                                    <h4 className="title">30</h4>
                                    <p className="count">Số lượng cửa hàng</p>
                                    <i className="icon-card fa fa-eye"></i>
                                </CardBody>
                            </Card>
                        </Col>
                        <Col xs="12" sm="6" lg="4">
                            <Card className="text-white bg-danger custom-card">
                                <CardBody className="pb-1">
                                    <h4 className="title">3 000</h4>
                                    <p className="count">Số lượng khách hàng</p>
                                    <i className="icon-card fa fa-user"></i>
                                </CardBody>
                            </Card>
                        </Col>

                        <Col xs="12" sm="6" lg="4">
                            <Card className="text-white bg-warning custom-card">
                                <CardBody className="pb-1">
                                    <h4 className="title">100 000 000</h4>
                                    <p className="count">Doanh thu</p>
                                    <i className="icon-card fa fa-money"></i>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs="12" sm="6">
                            <Card>
                                <CardHeader>
                                    <span>Cửa hàng có doanh thu cao nhất</span>
                                </CardHeader>
                                <CardBody>
                                    <div id="income-group-store">
                                    </div>
                                </CardBody>
                            </Card>
                        </Col>
                        <Col xs="12" sm="6">
                            <Card>
                                <CardHeader>
                                    <span> Doanh thu </span>
                                </CardHeader>
                                <CardBody>
                                    <div id="income-store">
                                    </div>
                                </CardBody>
                            </Card>
                        </Col>
                        <Col xs="12" sm="12">
                            <Card>
                                <CardHeader>
                                    <span>Doanh thu theo tháng</span>
                                </CardHeader>
                                <CardBody>
                                    <div id="income-group-month">
                                    </div>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </div>
            </div>
        )
    }
}

//export default Dashboard;
const mapStateToProps = (state) => {
    //console.log({ ...state });
    return { ...state.data }
};
Dashboard = connect(mapStateToProps, { ...actExample })(Dashboard);
export default Dashboard;