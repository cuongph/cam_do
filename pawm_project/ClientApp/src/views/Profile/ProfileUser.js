﻿import React, { Component } from 'react';
import {
    Form,
    Row,
    Col,
    Card,
    CardHeader,
    CardBody,
    CardFooter,
    Button,
    FormGroup,
    Label,
    Input
} from 'reactstrap';
import { TextMask, InputAdapter } from 'react-text-mask-hoc';

class ProfileUser extends Component {
    componentDidMount() {
        (function () {
            var forms = document.getElementsByClassName('form-liquidation-create');
            // Loop over them and prevent submission
            Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        })();

        (function () {
            var forms = document.getElementsByClassName('form-change-password');
            // Loop over them and prevent submission
            Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        })();
    }
    render() {
        return (
            <div className="create-liquidation-card-page">
                <Row>
                    <Col sm="8" xs="12">
                        <Card>
                            <Form className="form-liquidation-create" noValidate>
                                <CardHeader>
                                    <h1>Thông tin cá nhân</h1>
                                </CardHeader>
                                <CardBody>
                                    <Row>
                                        <Col sm="4" xs="12">
                                            <FormGroup>
                                                <Label htmlFor="text-input">Mã nhân viên</Label>
                                                <Input type="text" id="text-input" name="text-input" placeholder="Mã nhân viên" defaultValue="NV0001" required readOnly />
                                            </FormGroup>
                                        </Col>
                                        <Col sm="4" xs="12">
                                            <FormGroup>
                                                <Label htmlFor="text-input">Số điện thoại</Label>
                                                <TextMask
                                                    mask={[/[0]/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/]}
                                                    Component={InputAdapter}
                                                    className="form-control"
                                                    placeholder="Số điện thoại"
                                                    type="text" id="text-input" name="text-input"
                                                    value="0931548754"
                                                    required
                                                />
                                                <div className="invalid-feedback">
                                                    Vui lòng nhập số điện thoại.
                                                </div>
                                            </FormGroup>
                                        </Col>
                                        <Col sm="4" xs="12">
                                            <FormGroup>
                                                <Label htmlFor="text-input">Ngày sinh</Label>
                                                <TextMask
                                                    mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
                                                    Component={InputAdapter}
                                                    className="form-control"
                                                    placeholder="Ngày sinh"
                                                    type="text" id="text-input" name="text-input"
                                                    value="11/11/1996"
                                                />
                                            </FormGroup>
                                        </Col>
                                        <Col sm="6" xs="12">
                                            <FormGroup>
                                                <Label htmlFor="text-input">Họ tên</Label>
                                                <Input type="text" id="text-input" name="text-input" placeholder="Họ tên" defaultValue="Nguyễn Văn A" required />
                                                <div className="invalid-feedback">
                                                    Vui lòng nhập họ tên.
                                                </div>
                                            </FormGroup>
                                        </Col>
                                        <Col sm="6" xs="12">
                                            <FormGroup>
                                                <Label>Giới tính</Label> <br />
                                                <FormGroup check inline>
                                                    <Input className="form-check-input" type="radio" id="inline-radio1" name="inline-radios" value="option1" defaultChecked="true" />
                                                    <Label className="form-check-label" check htmlFor="inline-radio1">Nam</Label>
                                                </FormGroup>
                                                <FormGroup check inline style={{ marginLeft: '50px' }}>
                                                    <Input className="form-check-input" type="radio" id="inline-radio2" name="inline-radios" value="option2" />
                                                    <Label className="form-check-label" check htmlFor="inline-radio2">Nữ</Label>
                                                </FormGroup>
                                            </FormGroup>
                                        </Col>
                                        <Col sm="6" xs="12">
                                            <FormGroup>
                                                <Label htmlFor="text-input">Email</Label>
                                                <Input type="text" id="text-input" name="text-input" placeholder="Email" defaultValue="anv@gmail.com" />
                                            </FormGroup>
                                        </Col>
                                        <Col sm="6" xs="12">
                                            <FormGroup>
                                                <Label>Cửa hàng</Label>
                                                <Input type="text" placeholder="Cửa hàng" defaultValue="Cửa hàng 1" readOnly />
                                                <div className="invalid-feedback">
                                                    Vui lòng chọn cửa hàng.
                                                </div>
                                            </FormGroup>
                                        </Col>
                                        <Col sm="12" xs="12">
                                            <FormGroup>
                                                <Label htmlFor="text-input">Địa chỉ</Label>
                                                <Input type="textarea" rows="2" id="text-input" name="text-input" placeholder="Địa chỉ" defaultValue="123 BTX" />
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                </CardBody>
                                <CardFooter>
                                    <div className="button-right">
                                        <Button color="primary" type="submit">
                                            <span className="fa fa-save"> </span> Lưu
                                    </Button>
                                    </div>
                                </CardFooter>
                            </Form>
                        </Card>
                    </Col>
                    <Col sm="4" xs="12">
                        <Form className="form-change-password" noValidate>
                            <Card>
                                <CardHeader>
                                    <span>Đổi mật khẩu</span>
                                </CardHeader>
                                <CardBody>
                                    <Row>
                                        <Col xs="12">
                                            <FormGroup>
                                                <Label >Mật khẩu cũ</Label>
                                                <Input type="text" placeholder="Mật khẩu cũ" required />
                                                <div className="invalid-feedback">
                                                    Vui lòng nhập mật khẩu cũ.
                                                </div>
                                            </FormGroup>
                                        </Col>
                                        <Col xs="12">
                                            <FormGroup>
                                                <Label>Mật khẩu mới</Label>
                                                <Input type="text" placeholder="Mật khẩu mới" required />
                                                <div className="invalid-feedback">
                                                    Vui lòng nhập mật khẩu mới.
                                                </div>
                                            </FormGroup>
                                        </Col>
                                        <Col xs="12">
                                            <FormGroup>
                                                <Label >Nhập lại mật khẩu</Label>
                                                <Input type="text" placeholder="Nhập lại mật khẩu" required />
                                                <div className="invalid-feedback">
                                                    Vui lòng nhập lại mật khẩu mới.
                                                </div>
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                </CardBody>
                                <CardFooter>
                                    <div className="button-right">
                                        <Button color="primary" type="submit">
                                            <span className="fa fa-save"> </span> Lưu
                                    </Button>
                                    </div>
                                </CardFooter>
                            </Card>
                        </Form>
                    </Col>
                </Row>
            </div>
        )
    }
}
export default ProfileUser;
