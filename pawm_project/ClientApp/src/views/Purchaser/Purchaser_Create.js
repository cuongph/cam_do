﻿import React, { Component } from 'react';
import {
    Row,
    Col,
    Card,
    CardHeader,
    CardBody,
    CardFooter,
    Button,
    FormGroup,
    Label,
    Input,
    Form
} from 'reactstrap';
import { Link } from 'react-router-dom';
import { TextMask, InputAdapter } from 'react-text-mask-hoc';
import * as helper from '../../modules/Helper'
//React select
import Select from 'react-select';
import '../../../public/asset/scss/vendors/react-select/react-select.scss';
// React DateRangePicker
import 'react-dates/initialize';
import { DateRangePicker } from 'react-dates';
import 'react-dates/lib/css/_datepicker.css';

const options = [
    { value: 'AL', label: 'Alabama', disabled: true },
    { value: 'AK', label: 'Alaska' },
    { value: 'AS', label: 'American Samoa' },
    { value: 'AZ', label: 'Arizona' },
    { value: 'AR', label: 'Arkansas' },
    { value: 'CA', label: 'California' },
];
class CreatePurchaser extends Component {
    constructor(props) {
        super(props);

        this.state = {
        }
    };
    componentDidMount() {
        (function () {
            var forms = document.getElementsByClassName('form-create-purchaser');
            // Loop over them and prevent submission
            Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        })();
    }
    render() {
        return (
            <div className="create-purchaser-page">
                <Form className="form-create-purchaser" noValidate>
                    <Card>
                        <CardHeader>
                            <h1>Thêm thợ</h1>
                        </CardHeader>
                        <CardBody>
                            <Row>
                                <Col sm="3" xs="12">
                                    <FormGroup>
                                        <Label htmlFor="text-input">Mã thợ</Label>
                                        <Input type="text" id="text-input" name="text-input" placeholder="T0012" required readOnly />
                                        <div className="invalid-feedback">
                                            Vui lòng nhập họ tên.
                                </div>
                                    </FormGroup>
                                </Col>
                                <Col sm="3" xs="12">
                                    <FormGroup>
                                        <Label htmlFor="text-input">Họ tên</Label>
                                        <Input type="text" id="text-input" name="text-input" placeholder="Họ tên" required />
                                        <div className="invalid-feedback">
                                            Vui lòng nhập họ tên.
                                </div>
                                    </FormGroup>
                                </Col>
                                <Col sm="3" xs="12">
                                    <FormGroup>
                                        <Label htmlFor="text-input">Số điện thoại</Label>
                                        <TextMask
                                            mask={[/[0]/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/]}
                                            Component={InputAdapter}
                                            className="form-control"
                                            placeholder="Số điện thoại"
                                            type="text" id="text-input" name="text-input"
                                            required
                                        />
                                        <div className="invalid-feedback">
                                            Vui lòng nhập số điện thoại.
                                    </div>
                                    </FormGroup>
                                </Col>
                                <Col sm="3" xs="12">
                                    <FormGroup>
                                        <Label htmlFor="text-input">Email</Label>
                                        <Input type="text" id="text-input" name="text-input" placeholder="Email" />
                                    </FormGroup>
                                </Col>
                                <Col sm="3" xs="12">
                                    <FormGroup>
                                        <Label htmlFor="text-input">Ngày sinh</Label>
                                        <TextMask
                                            mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
                                            Component={InputAdapter}
                                            className="form-control"
                                            placeholder="Ngày sinh"
                                            type="text" id="text-input" name="text-input"
                                        />
                                    </FormGroup>
                                </Col>
                                <Col sm="3" xs="12">
                                    <FormGroup>
                                        <Label htmlFor="text-input">Địa chỉ</Label>
                                        <Input type="text" name="textarea-input" id="textarea-input" rows="1" placeholder="Địa chỉ" />
                                    </FormGroup>
                                </Col>
                                <Col sm="3" xs="12">
                                    <FormGroup>
                                        <Label>Giới tính</Label> <br />
                                        <FormGroup check inline>
                                            <Input className="form-check-input" type="radio" id="inline-radio1" name="inline-radios" value="option1" />
                                            <Label className="form-check-label" check htmlFor="inline-radio1">Nam</Label>
                                        </FormGroup>
                                        <FormGroup check inline style={{ marginLeft: '50px' }}>
                                            <Input className="form-check-input" type="radio" id="inline-radio2" name="inline-radios" value="option2" />
                                            <Label className="form-check-label" check htmlFor="inline-radio2">Nữ</Label>
                                        </FormGroup>
                                    </FormGroup>
                                </Col>
                                <Col sm="12" xs="12">
                                    <FormGroup>
                                        <Label htmlFor="text-input">Ghi chú</Label>
                                        <Input type="textarea" name="textarea-input" id="textarea-input" rows="1" placeholder="Thợ tốt" />
                                    </FormGroup>
                                </Col>
                            </Row>
                            
                        </CardBody>
                        <CardFooter>
                            <div className="button-right">
                                <Link to="/danh-sach-tho" className="btn btn-primary" >
                                    <span className="icon-list"> </span>Danh sách
                            </Link>
                                <Button className="button_focus" color="primary" type="submit">
                                    <span className="fa fa-save"> </span> Lưu
                            </Button>
                            </div>
                        </CardFooter>
                    </Card>
                </Form>
                
            </div>
        )
    }
}
export default CreatePurchaser;