﻿import React, { Component } from 'react';
//import PropTypes from 'prop-types';
import {
    Nav,
    NavbarBrand,
    NavbarToggler
} from 'reactstrap';
import HeaderDropdown from './HeaderDropdown';

class Header extends Component {

    sidebarToggle(e) {
        e.preventDefault();
        document.body.classList.toggle('sidebar-hidden');
    }

    sidebarMinimize(e) {
        e.preventDefault();
        document.body.classList.toggle('sidebar-minimized');
    }

    mobileSidebarToggle(e) {
        e.preventDefault();
        document.body.classList.toggle('sidebar-mobile-show');
    }

    asideToggle(e) {
        e.preventDefault();
        document.body.classList.toggle('aside-menu-hidden');
    }

    render() {
        return (
            <header className="app-header navbar">
                <NavbarToggler className="d-lg-none" onClick={this.mobileSidebarToggle}>
                    <span className="navbar-toggler-icon"><i className="icon-menu"></i></span>
                </NavbarToggler>
                <NavbarToggler className="d-md-down-none" onClick={this.sidebarToggle}>
                    <span className="navbar-toggler-icon"><i className="icon-menu"></i></span>
                </NavbarToggler>
                <Nav className="ml-auto" navbar>
                    <HeaderDropdown accnt />
                </Nav>
            </header>
        );
    }
}

export default Header;
