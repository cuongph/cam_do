﻿import React, { Component } from 'react';
import {
    Col,
    Button,
    Card,
    CardHeader,
    CardFooter,
    CardBody,
    Form,
    FormGroup,
    Label,
    Input,
    Row
} from 'reactstrap';
import { Link } from 'react-router-dom'
import { TextMask, InputAdapter } from 'react-text-mask-hoc';
class Stores_Detail extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = { collapse: true };
    }

    toggle() {
        this.setState({ collapse: !this.state.collapse });
    }
    componentDidMount() {
        (function () {
            var forms = document.getElementsByClassName('form-stores-update');
            // Loop over them and prevent submission
            Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        })();
    }
    render() {
        return (
            <Card>
                <Form className="form-stores-update" noValidate>
                    <CardHeader>
                        <h1>Thông tin cửa hàng</h1>
                    </CardHeader>
                    <CardBody>
                        <Row>
                            <Col xs="4">
                                <FormGroup>
                                    <Label>Mã cửa hàng</Label>
                                    <Input type="text" id="text-input" name="text-input" placeholder="" defaultValue="CH001" readOnly />
                                </FormGroup>
                            </Col>
                            <Col xs="4">
                                <FormGroup>
                                    <Label htmlFor="text-input">Tên cửa hàng</Label>
                                    <Input type="text" id="text-input" name="text-input" placeholder="Tên cửa hàng" defaultValue="Cửa hàng Bình Hưng" required />
                                    <div className="invalid-feedback">
                                        Vui lòng nhập tên cửa hàng.
                                </div>
                                </FormGroup>
                            </Col>
                            <Col xs="4">
                                <FormGroup>
                                    <Label htmlFor="text-input">Số điện thoại</Label>
                                    <TextMask
                                        mask={[/[0]/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/]}
                                        Component={InputAdapter}
                                        className="form-control"
                                        placeholder="Số điện thoại"
                                        type="text" id="text-input" name="text-input"
                                        value="0963187542"
                                        required
                                    />
                                    <div className="invalid-feedback">
                                        Vui lòng nhập số điện thoại.
                                </div>
                                </FormGroup>
                            </Col>
                            <Col xs="12">
                                <FormGroup>
                                    <Label htmlFor="text-input">Địa chỉ</Label>
                                    <Input type="textarea" rows="2" placeholder="Địa chỉ" defaultValue="đường số 6 Bình Hưng, Bình Chánh" required />
                                    <div className="invalid-feedback">
                                        Vui lòng nhập địa chỉ cửa hàng.
                                </div>
                                </FormGroup>
                            </Col>
                        </Row>
                    </CardBody>
                    <CardFooter>
                        <div className="button-right">
                            <Link to="/danh-sach-cua-hang" className="btn btn-primary" >
                                <span className="icon-list"> </span>Danh sách
                            </Link>
                            <Button color="primary" type="submit" className="button_focus">
                                <span className="fa fa-save"> </span> Lưu
                                    </Button>
                        </div>
                    </CardFooter>
                </Form>
            </Card>
        )
    }
}

export default Stores_Detail;