﻿import React, { Component } from 'react';
import {
    Row,
    Col,
    Card,
    CardHeader,
    CardBody,
    CardFooter,
    Button,
    Input,
    Table
} from 'reactstrap';
import { Link } from 'react-router-dom'
import $ from 'jquery'
import * as helper from '../../modules/Helper'

import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
//import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';

const data = [
    {
        'makh': 'KH-0001',
        'tenkh': 'Nguyễn Văn A',
        'dienthoai': '123456789',
        'diachi': '	64/5/2B1 Nguyễn Khoái, p2. Q4 Hồ Chí Minh',
        'langd': '2',
        'tongtien': '15,000,000'
    },
    {
        'makh': 'KH-0001',
        'tenkh': 'Nguyễn Văn A',
        'dienthoai': '123456789',
        'diachi': '	64/5/2B1 Nguyễn Khoái, p2. Q4 Hồ Chí Minh',
        'langd': '2',
        'tongtien': '15,000,000'
    },
    {
        'makh': 'KH-0001',
        'tenkh': 'Nguyễn Văn A',
        'dienthoai': '123456789',
        'diachi': '	64/5/2B1 Nguyễn Khoái, p2. Q4 Hồ Chí Minh',
        'langd': '2',
        'tongtien': '15,000,000'
    },
]
class CustomerManagement extends Component {
    constructor(props) {
        super(props);

        this.table = data;
        this.options = {
            sortIndicator: true,
            hideSizePerPage: true,
            //sizePerPage: 25,
            paginationSize: 3,
            hidePageListOnlyOnePage: false,
            clearSearch: false,
            alwaysShowAllBtns: true,
            withFirstAndLast: false,
            //prePage: '<<',
            //nextPage: '>>',
            paginationShowsTotal: true,
            noDataText: 'Không có dữ liệu'
        }

        this.cellButton = this.cellButton.bind(this)

    }

    componentDidMount() {
        $(document).on("keypress", "#Phone", function (evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57)) {
                return false;
            }
            return true;
        })
    }

    onClickProductSelected(cell, row, rowIndex) {
        console.log('Product #', rowIndex);
    }

    cellButton(cell, row, enumObject, rowIndex) {
        return (
            //<button
            //    type="button"
            //    onClick={() =>
            //        this.onClickProductSelected(cell, row, rowIndex)}
            //>
            //    Click me {rowIndex}
            //</button>

            <Link to={helper.getPathHost('customer', 'detail-customer', 'url')} className="btn btn-primary btn-sm" >
                <span className="icon-plus no-mr"> </span>
            </Link>
        )
    }
    render() {
        return (
            <div className="animated fadeIn">
                <div className="customer-page form_customer">
                    <Card>
                        <CardHeader>
                            <h1>Danh sách khách hàng</h1>
                        </CardHeader>
                        <CardBody>
                            <Row>
                                <Col sm="4" xs="12">
                                    <label>Mã cửa hàng</label>
                                    <Input type="text" placeholder="Mã khách hàng" />
                                </Col>
                                <Col sm="4" xs="12">
                                    <label>Tên khách hàng</label>
                                    <Input type="text" placeholder="Tên khách hàng" />
                                </Col>
                                <Col sm="4" xs="12">
                                    <label>Số điện thoại</label>
                                    <Input type="text" placeholder="Số điện thoại" id="Phone" />
                                </Col>
                            </Row>
                        </CardBody>
                        <CardFooter>
                            <div className="button-right">
                                <Link to={helper.getPathHost('customer', 'create-customer', 'url')} className="btn btn-primary" >
                                    <span className="icon-plus"> </span> Thêm mới
                                    </Link>
                                <Button color="primary button_focus">
                                    <span className="icon-magnifier"> </span>Tìm kiếm
                                </Button>
                            </div>
                        </CardFooter>
                    </Card>
                    <Card>
                        <CardBody className="tablet_store ">
                            <BootstrapTable data={this.table} version="4" bordered={false} striped hover pagination search options={this.options} >
                                <TableHeaderColumn dataField='button' dataFormat={this.cellButton} width='60px' dataAlign='center'/>
                                <TableHeaderColumn isKey dataField="makh" dataSort width='90px'  >Mã KH</TableHeaderColumn>
                                <TableHeaderColumn dataField="tenkh" dataSort>Tên KH</TableHeaderColumn>
                                <TableHeaderColumn dataField="dienthoai" dataSort>Điện thoại</TableHeaderColumn>
                                <TableHeaderColumn dataField="diachi" dataSort width='300px' >Địa chỉ</TableHeaderColumn>
                                <TableHeaderColumn dataField="langd" dataSort dataAlign='center'>Lần GD</TableHeaderColumn>
                                <TableHeaderColumn dataField="tongtien" dataSort>Tổng tiền</TableHeaderColumn>
                            </BootstrapTable>
                        </CardBody>
                    </Card>
                </div>
            </div>
        )
    }
}
export default CustomerManagement;
