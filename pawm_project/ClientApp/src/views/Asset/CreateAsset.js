﻿import React, { Component } from 'react';
import {
    Row,
    Col,
    Card,
    CardHeader,
    CardBody,
    CardFooter,
    Button,
    FormGroup,
    Label,
    Input,
    Form
} from 'reactstrap';
import { Link } from 'react-router-dom'
import TagsInput from 'react-tagsinput'
import 'react-tagsinput/react-tagsinput.css'

class CreateAsset extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tags: []
        };
        this.handleChangeTagsInput = this.handleChangeTagsInput.bind(this);
    }
    handleChangeTagsInput(tags) {
        this.setState({ tags });
    }
    componentDidMount() {
        (function () {
            var forms = document.getElementsByClassName('form-create-asset');
            // Loop over them and prevent submission
            Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        })();
    }
    render() {
        return (
            <div className="create-asset-page">
                <Form className="form-create-asset" noValidate>
                    <Card>
                        <CardHeader>
                            <h1>Thêm loại tài sản</h1>
                        </CardHeader>
                        <CardBody>
                            <Row>
                                <Col xs="3">
                                    <Label>Mã loại tài sản</Label>
                                    <Input type="text" placeholder="TS05" readOnly />
                                </Col>
                                <Col xs="3">
                                    <Label>Tên loại tài sản</Label>
                                    <Input type="text" placeholder="Tên loại tài sản" />
                                </Col>
                                <Col xs="12">
                                    <FormGroup>
                                        <Label>Tên tài sản</Label>
                                        <TagsInput inputProps={{ placeholder: "Tên tài sản" }} value={this.state.tags} onChange={this.handleChangeTagsInput} />
                                    </FormGroup>
                                </Col>
                                <Col xs="12">
                                    <FormGroup>
                                        <Label>Ghi chú</Label>
                                        <Input type="textarea" name="textarea-input" id="textarea-input" rows="2"
                                            placeholder="Ghi chú" />
                                    </FormGroup>
                                </Col>
                            </Row>
                        </CardBody>
                        <CardFooter>
                            <div className="button-right">
                                <Link to="/danh-sach-loai-tai-san" className="btn btn-primary" >
                                    <span className="icon-list"> </span>Danh sách
                            </Link>
                                <Button className="button_focus" color="primary" type="submit">
                                    <span className="fa fa-save"> </span> Lưu
                            </Button>
                            </div>
                        </CardFooter>
                    </Card>
                </Form>
            </div>
        )
    }
}
export default CreateAsset;
